#tag Module
Protected Module IconsModule
	#tag Method, Flags = &h0
		Function Ico(Name As String, Debug As Boolean = False) As Picture
		  If IconList.HasKey(Name) then
		    Return IconList.Value(Name)
		  elseif Debug then
		    #if DebugBuild
		      break
		    #endif
		  End If
		  
		  Return Nil
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function loadMaskedPicture(image as picture) As picture
		  if Image = nil then Return nil
		  
		  If image.Width <> Image.Height*2 then Return image
		  
		  dim newpic as Picture = New Picture(image.Width/2, Image.Height, 32)
		  NewPic.Graphics.DrawPicture Image, 0,0
		  NewPic.Mask.Graphics.DrawPicture Image, 0, 0, NewPic.Width, NewPic.Height, Image.Width/2, 0
		  
		  Return newpic
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Open(FolderPath As String = "")
		  IconList = new Dictionary
		  //1
		  
		  Dim f As new FolderItem
		  #if DebugBuild and TargetWin32
		    f = f.Parent
		  #endif
		  
		  #if TargetLinux = false then
		    f = f.Child("Resources").Child("Icons")
		  #else
		    //alviero per linux
		    f = GetFolderItem("Resources")
		    if f <> nil then
		      f = f.parent
		      #if DebugBuild
		        f = GetFolderItem(f.parent.absolutePath + "Resources")
		      #else
		        f = GetFolderItem(f.absolutePath + "Resources")
		      #endif
		      f =  getfolderItem(f.absolutePath + "Icons")
		    end if
		  #endif
		  
		  
		  If FolderPath <> "" then
		    f = GetFolderItem(FolderPath)
		  End If
		  
		  
		  dim g As FolderItem
		  
		  IconList = New Dictionary
		  Dim p As Picture
		  
		  If f is nil or not f.Exists then
		    MsgBox("Unable to open resources")
		    Return
		  End If
		  For i as Integer = 1 to f.Count
		    g = f.TrueItem(i)
		    'MsgBox g.Name
		    If g.Length < Abs(500000) then
		      p = Picture.Open(g)
		      If p <> Nil then
		        If p.Width >= p.Height * 2 then
		          IconList.Value(ReplaceExtension(g.Name, "")) = IconsModule.loadMaskedPicture(p)
		        else
		          IconList.Value(ReplaceExtension(g.Name, "")) = p
		        End If
		      End If
		    End If
		  Next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ReplaceExtension(sourceString as string, replacementString as string = "") As String
		  Dim outString as string
		  
		  outString=sourceString.Replace(right(sourceString, 6-right(sourceString, 5).InStr(".")), replacementString)
		  
		  Return outString
		End Function
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected IconList As Dictionary
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
