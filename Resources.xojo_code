#tag Module
Protected Module Resources
	#tag Property, Flags = &h0
		ButtonXML As String
	#tag EndProperty

	#tag Property, Flags = &h0
		MenuXML As String
	#tag EndProperty

	#tag Property, Flags = &h0
		OptionXML As String
	#tag EndProperty

	#tag Property, Flags = &h0
		RibbonXML As String
	#tag EndProperty

	#tag Property, Flags = &h0
		SectionXML As String
	#tag EndProperty

	#tag Property, Flags = &h0
		TabXML As String
	#tag EndProperty

	#tag Property, Flags = &h0
		xButton As RibbonButton
	#tag EndProperty

	#tag Property, Flags = &h0
		xMenu As RibbonMenu
	#tag EndProperty

	#tag Property, Flags = &h0
		xOption As RibbonMenuOption
	#tag EndProperty

	#tag Property, Flags = &h0
		xSection As RibbonSection
	#tag EndProperty

	#tag Property, Flags = &h0
		xTab As RibbonTab
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="ButtonXML"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MenuXML"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OptionXML"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RibbonXML"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SectionXML"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabXML"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
