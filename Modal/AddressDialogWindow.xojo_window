#tag Window
Begin Window AddressDialogWindow
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   9
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   469
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "ที่อยู่อาศัย"
   Visible         =   True
   Width           =   576
   Begin Label Label1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "ที่อยู่"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   52
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin Label Label2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "จังหวัด"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   132
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin Label Label4
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   294
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "อำเภอ/เขต"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   132
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin Label Label12
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "หมายเหตุ"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   292
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin TextArea taRemarks
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   True
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   100
      HelpTag         =   ""
      HideSelection   =   True
      Index           =   -2147483648
      Italic          =   False
      Left            =   20
      LimitText       =   0
      LineHeight      =   0.0
      LineSpacing     =   1.0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Multiline       =   True
      ReadOnly        =   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollbarVertical=   True
      Styled          =   True
      TabIndex        =   22
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "ข้อมูลทดสอบ"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   317
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   535
   End
   Begin PushButton btnCancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "ยกเลิก"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   476
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   23
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   429
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin PushButton btnAdd
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "บันทึก"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   384
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   24
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   429
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin Label Label5
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   33
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "ตำบล/แขวง"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   164
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin Label Label6
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   294
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   39
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "รหัสไปรษณีย์"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   164
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin Label Label7
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   40
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "โทรศัพท์บ้าน"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   196
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin Label Label8
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   294
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   41
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "โทรศัพท์มือถือ"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   197
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin Label Label9
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   42
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "อีเมลล์"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   228
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin Label Label10
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   43
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "ลักษณะที่อยู่อาศัย"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   260
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin PopupMenu popProvince
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   132
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   44
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   131
      Underline       =   False
      Visible         =   True
      Width           =   150
   End
   Begin PopupMenu popDistrict
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   405
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   45
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   132
      Underline       =   False
      Visible         =   True
      Width           =   150
   End
   Begin PopupMenu popSubDistrict
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   132
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   46
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   163
      Underline       =   False
      Visible         =   True
      Width           =   150
   End
   Begin PopupMenu popZipcode
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   406
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   47
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   163
      Underline       =   False
      Visible         =   True
      Width           =   150
   End
   Begin TextField fldHomePhone
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      Italic          =   False
      Left            =   132
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   48
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "024554636"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   195
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   150
   End
   Begin TextField fldMobilePhone
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      Italic          =   False
      Left            =   406
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   49
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "0909090909"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   196
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   150
   End
   Begin TextField fldEmail
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      Italic          =   False
      Left            =   132
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   50
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "po@go.com"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   227
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   424
   End
   Begin PopupMenu popTypeAddress
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "บ้าน\nคอนโด"
      Italic          =   False
      Left            =   132
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   51
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   259
      Underline       =   False
      Visible         =   True
      Width           =   150
   End
   Begin PopupMenu popAddressDetails
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "อยู่คนเดียว\nอยู่กับครอบครัว\nอยู่กับญาติ\n"
      Italic          =   False
      Left            =   406
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   52
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   259
      Underline       =   False
      Visible         =   True
      Width           =   150
   End
   Begin TextField taAddress
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   68
      HelpTag         =   ""
      Index           =   -2147483648
      Italic          =   False
      Left            =   132
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   53
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "442"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   51
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   424
   End
   Begin Label Label13
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   54
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "ประเภทที่อยู่"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   20
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin PopupMenu popType
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "ที่อยู่ปัจจุบัน\nที่อยู่ตามทะเบียนบ้าน"
      Italic          =   False
      Left            =   132
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   55
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   19
      Underline       =   False
      Visible         =   True
      Width           =   424
   End
   Begin Label Label11
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   294
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   56
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "ลักษณะการอยู่อาศัย"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   260
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   110
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  me.SetComponent
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Function GetIDXFromAddressDetails(text as String) As Integer
		  For i As Integer = 0 To Me.popAddressDetails.ListCount-1
		    If Me.popAddressDetails.RowTag(i) = Text Then
		      Return i
		    End
		  Next
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetIDXFromDistrict(text as String) As Integer
		  For i As Integer = 0 To Me.popDistrict.ListCount-1
		    If Me.popDistrict.RowTag(i) = Text Then
		      Return i
		    End
		  Next
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetIDXFromProvince(text as String) As Integer
		  For i As Integer = 0 To Me.popProvince.ListCount-1
		    If Me.popProvince.RowTag(i) = Text Then
		      Return i
		    End
		  Next
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetIDXFromSubDistrict(text as String) As Integer
		  For i As Integer = 0 To Me.popSubDistrict.ListCount-1
		    If Me.popSubDistrict.RowTag(i) = Text Then
		      Return i
		    End
		  Next
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetIDXFromType(text as String) As Integer
		  For i As Integer = 0 To Me.popType.ListCount-1
		    If Me.popType.RowTag(i) = Text Then
		      Return i
		    End
		  Next
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetIDXFromTypeAddress(text as String) As Integer
		  For i As Integer = 0 To Me.popTypeAddress.ListCount-1
		    If Me.popTypeAddress.RowTag(i) = Text Then
		      Return i
		    End
		  Next
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetIDXFromZipcode(text as String) As Integer
		  For i As Integer = 0 To Me.popZipcode.ListCount-1
		    If Me.popZipcode.RowTag(i) = Text Then
		      Return i
		    End
		  Next
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetValue() As JSONItem
		  Dim addressJson as new JSONItem
		  if self.CurrentID = "" then
		    addressJson.Value("order")  = ""
		  else
		    addressJson.Value("order")  = self.CurrentID
		  end
		  
		  addressJson.Value("id")  = ""
		  addressJson.Value("addressType") = me.popType.RowTag(me.popType.ListIndex)
		  addressJson.Value("addressTypeName") = me.popType.Text
		  addressJson.Value("houseNo") = me.taAddress.Text
		  addressJson.Value("province") = me.popProvince.RowTag(me.popProvince.ListIndex)
		  addressJson.Value("provinceName") = me.popProvince.Text
		  addressJson.Value("district") = me.popDistrict.RowTag(me.popDistrict.ListIndex)
		  addressJson.Value("districtName") = me.popDistrict.Text
		  addressJson.Value("subdistrict") = me.popSubDistrict.RowTag(me.popSubDistrict.ListIndex)
		  addressJson.Value("subdistrictName") = me.popSubDistrict.Text
		  addressJson.Value("zipcode") = me.popZipcode.RowTag(me.popZipcode.ListIndex)
		  addressJson.Value("zipcodeName") = me.popZipcode.Text
		  addressJson.Value("homePhone") = me.fldHomePhone.Text
		  addressJson.Value("mobilePhone") = me.fldMobilePhone.Text
		  addressJson.Value("email") = me.fldEmail.Text
		  addressJson.Value("addressDetails") = me.popAddressDetails.RowTag(me.popAddressDetails.ListIndex)
		  addressJson.Value("addressDetailsName") = me.popAddressDetails.Text
		  addressJson.Value("addressDetails1")  = me.popTypeAddress.RowTag(me.popTypeAddress.ListIndex)
		  addressJson.Value("addressDetails1Name") = me.popTypeAddress.Text
		  addressJson.Value("remark") = me.taRemarks.Text
		  
		  Return addressJson
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetComponent()
		  me.SetPopupProvince
		  'me.SetPopupDistrict
		  'me.SetPopupSubDistrict
		  'me.SetPopupPostalCode
		  me.SetPopupTypeAddress
		  me.SetPopupAddressDetail
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetPopupAddressDetail()
		  'Dim cert as new Repo.WS.Certificate
		  'Dim js as String
		  'Dim jsData as new JSONItem
		  'Try
		  'js = cert.ListAll
		  'jsData = New JSONItem(js)
		  'If jsData.Value("status") = True Then
		  'Dim objData As JSONItem = jsData.Value("data")
		  'me.popCertificate.AddRow("กรุณาเลือกข้อมูล")
		  'me.popCertificate.RowTag(0) = ""
		  'For i As Integer = 0 To objData.Count-1
		  'Dim iItem As JSONItem = objData.Child(i)
		  'me.popCertificate.AddRow(iItem.Value("certificateNameTh"))
		  'me.popCertificate.RowTag(i+1) = iItem.Value("id")
		  'Next
		  'End
		  'Catch err As KeyNotFoundException
		  'End
		  '
		  'me.popCertificate.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetPopupDistrict(provinceId as String)
		  Dim dInstance As New Repo.WS.District
		  Dim js As String
		  Dim jsData As JSONItem
		  
		  js = dInstance.ListByID(provinceId)
		  jsData = New JSONItem(js)
		  popDistrict.DeleteAllRows
		  If jsData.Value("status") = True Then
		    Dim objData As JSONItem = jsData.Value("data")
		    For i As Integer = 0 To objData.Count - 1 ' loop over data
		      Dim iItem As JSONItem = objData.Child(i)
		      popDistrict.AddRow(iItem.Value("districtName"))
		      popDistrict.RowTag(i) = iItem.Value("id")
		    Next
		    popDistrict.ListIndex = 0
		    popDistrict.Enabled = true
		  End
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetPopupProvince()
		  Dim pvInstance As New Repo.WS.Province
		  Dim js As String
		  Dim jsData As JSONItem
		  js = pvInstance.ListAll
		  jsData = New JSONItem(js)
		  popProvince.DeleteAllRows
		  If jsData.Value("status") = True Then
		    Dim objData As JSONItem = jsData.Value("data")
		    popProvince.AddRow("กรุณาเลือกจังหวัด")
		    popProvince.ListIndex = 0
		    For i As Integer = 0 To objData.Count - 1
		      Dim iItem As JSONItem = objData.Child(i)
		      popProvince.AddRow(iItem.Value("provinceName"))
		      popProvince.RowTag(i + 1) = iItem.Value("id")
		    Next
		  End
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetPopupSubDistrict(districtID as String)
		  Dim sdInstance As New Repo.WS.SubDistrict
		  Dim js As String
		  Dim jsData As JSONItem
		  
		  js = sdInstance.ListByID(districtID)
		  jsData = New JSONItem(js)
		  popSubDistrict.DeleteAllRows
		  If jsData.Value("status") = True Then
		    Dim objData As JSONItem = jsData.Value("data")
		    For i As Integer = 0 To objData.Count - 1 ' loop over data
		      Dim iItem As JSONItem = objData.Child(i)
		      popSubDistrict.AddRow(iItem.Value("subDistrictName"))
		      popSubDistrict.RowTag(i) = iItem.Value("id")
		    Next
		    popSubDistrict.ListIndex = 0
		    popSubDistrict.Enabled = true
		  End
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetPopupTypeAddress()
		  'Dim cert as new Repo.WS.Certificate
		  'Dim js as String
		  'Dim jsData as new JSONItem
		  'Try
		  'js = cert.ListAll
		  'jsData = New JSONItem(js)
		  'If jsData.Value("status") = True Then
		  'Dim objData As JSONItem = jsData.Value("data")
		  'me.popCertificate.AddRow("กรุณาเลือกข้อมูล")
		  'me.popCertificate.RowTag(0) = ""
		  'For i As Integer = 0 To objData.Count-1
		  'Dim iItem As JSONItem = objData.Child(i)
		  'me.popCertificate.AddRow(iItem.Value("certificateNameTh"))
		  'me.popCertificate.RowTag(i+1) = iItem.Value("id")
		  'Next
		  'End
		  'Catch err As KeyNotFoundException
		  'End
		  '
		  'me.popCertificate.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetPopupZipcode(subID as String)
		  Dim zipIns As New Repo.WS.ZipCode
		  Dim js As String
		  Dim jsData As JSONItem
		  
		  js = zipIns.ListByID(subID)
		  jsData = New JSONItem(js)
		  popZipCode.DeleteAllRows
		  If jsData.Value("status") = True Then
		    Dim objData As JSONItem = jsData.Value("data")
		    if objData.Count > 0 Then
		      For i As Integer = 0 To objData.Count - 1 ' loop over data
		        Dim iItem As JSONItem = objData.Child(i)
		        popZipCode.AddRow(iItem.Value("zipcode"))
		        popZipCode.RowTag(i) = iItem.Value("id")
		      Next
		    else
		      popZipCode.AddRow("")
		      popZipCode.RowTag(0) = ""
		    end if
		    popZipCode.ListIndex = 0
		    popZipCode.Enabled = true
		  End
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetValue(data as JSONItem)
		  self.CurrentID = data.Value("order")
		  
		  me.popType.ListIndex = me.GetIDXFromType(data.Value("addressType").StringValue)
		  me.taAddress.Text = data.Value("houseNo")
		  me.popProvince.ListIndex = me.GetIDXFromProvince(data.Value("province").StringValue)
		  me.popDistrict.ListIndex = me.GetIDXFromDistrict(data.Value("district").StringValue)
		  me.popSubDistrict.ListIndex = me.GetIDXFromSubDistrict(data.Value("province").StringValue)
		  me.popZipcode.ListIndex = me.GetIDXFromZipcode(data.Value("zipcode").StringValue)
		  me.fldHomePhone.text = data.Value("homePhone").StringValue
		  me.fldMobilePhone.Text = data.Value("mobilePhone").StringValue
		  me.fldEmail.Text = data.Value("email").StringValue
		  me.popTypeAddress.ListIndex = me.GetIDXFromTypeAddress(data.Value("addressDetails").StringValue)
		  me.popAddressDetails.ListIndex = me.GetIDXFromAddressDetails(data.Value("addressDetails1").StringValue)
		  me.taRemarks.Text = data.Value("remark")
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		CurrentAddress As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		CurrentID As String
	#tag EndProperty


#tag EndWindowCode

#tag Events btnCancel
	#tag Event
		Sub Action()
		  Self.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnAdd
	#tag Event
		Sub Action()
		  Self.CurrentAddress = Self.GetValue
		  Self.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events popProvince
	#tag Event
		Sub Change()
		  Dim provinceId As String
		  if me.ListIndex > 0 Then
		    provinceId = me.RowTag(me.ListIndex)
		    SetPopupDistrict(provinceId)
		  End
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events popDistrict
	#tag Event
		Sub Change()
		  Dim districtID As String
		  if me.ListIndex > -1 Then
		    districtID = me.RowTag(me.ListIndex)
		    SetPopupSubDistrict(districtID)
		  End
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events popSubDistrict
	#tag Event
		Sub Change()
		  Dim subID As String
		  if me.ListIndex >= 0 Then
		    subID = me.RowTag(me.ListIndex)
		    SetPopupZipcode(subID)
		  End
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events popType
	#tag Event
		Sub Change()
		  Dim provinceId As String
		  if me.ListIndex > 0 Then
		    provinceId = me.RowTag(me.ListIndex)
		    SetPopupDistrict(provinceId)
		  End
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Group="OS X (Carbon)"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CurrentID"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Frame"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Frame"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Group="OS X (Carbon)"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Size"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Size"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Menus"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Size"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Size"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Frame"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
