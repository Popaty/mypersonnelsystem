#tag Window
Begin ContainerControl ctnUploadFile
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   548
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   True
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   904
   Begin Listbox FileList
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   4
      ColumnsResizable=   False
      ColumnWidths    =   "5%,45%,40%,10%"
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   False
      GridLinesHorizontal=   2
      GridLinesVertical=   2
      HasHeading      =   True
      HeadingIndex    =   -1
      Height          =   397
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "ลำดับ	หัวข้อเอกสาร	ไฟล์แนบ	สถานะ"
      Italic          =   False
      Left            =   20
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   86
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   864
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin PushButton PushButton2
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   True
      Caption         =   "อัพโหลด"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   797
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   495
      Underline       =   False
      Visible         =   True
      Width           =   87
   End
   Begin BevelButton BevelButton1
      AcceptFocus     =   True
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "เพิ่มเอกสาร"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   776
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   52
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   108
   End
   Begin BevelButton BevelButton2
      AcceptFocus     =   True
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "ลบเอกสาร"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   654
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   52
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   110
   End
   Begin Label Label1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   5
      TabPanelIndex   =   0
      Text            =   "สามารถอัพโหลดเอกสารได้ โดยเลือกหัวข้อที่ต้องการ"
      TextAlign       =   1
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   20
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   864
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  self.Deleted = new JSONItem
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Function CheckFileUpload(fileName as String, filePath as String, documentName as String) As Boolean
		  'MsgBox fileName+" "+filePAth+" "+documentName
		  
		  if self.ValidateFileType(fileName) then
		    if self.FileList.ListCount > 0 then
		      //Another Record
		      Dim getNameDocumentinList(-1) as String
		      for i as integer = 0 to self.FileList.ListCount-1
		        getNameDocumentinList.Append(self.FileList.Cell(i,1))
		      next
		      
		      Dim checkDocument as Boolean = False
		      for i as integer = 0 to getNameDocumentinList.Ubound
		        if documentName = getNameDocumentinList(i) then
		          checkDocument = True
		        end
		      next
		      
		      if checkDocument then
		        MsgBox ("เอกสารนี้ได้ถูกอัพโหลดไปแล้ว กรุณาเลือกหัวข้อเอกสารใหม่")
		      else
		        self.SetFileUploadToList(fileName,filePath,documentName)
		      end
		    else
		      //First Record
		      self.SetFileUploadToList(fileName,filePath,documentName)
		    end
		  else
		    MsgBox "อัพโหลดไฟล์รองรับนามสกุล PDF, JPG, PNG เท่านั้น"
		  end
		  
		  Return true
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetExtensionFile(file as String) As String
		  dim pos as integer=0
		  dim last  as boolean = false
		  dim extension as string =""
		  dim oldfile as string=file
		  
		  while not last
		    pos =InStr(file,".")
		    if pos > 0 then
		      oldfile = file
		      file = mid(file,pos+1)
		    else
		      last = true
		    end if
		  wend
		  
		  pos =InStr(oldfile,".")
		  
		  if pos >0 then
		    extension = mid(oldfile,pos+1,len(oldfile))
		  else
		    extension = ""
		  end if
		  
		  return extension
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetValue(category as String) As JSONItem
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetFileUploadStatus()
		  for i as integer = 0 to self.FileList.ListCount -1
		    self.FileList.Cell(i,3) = "อัพโหลดแล้ว"
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetFileUploadToList(fileName as String, filePath as String, documentName as String)
		  self.FileList.AddRow
		  self.FileList.CellType(self.FileList.LastIndex,0) = self.FileList.TypeCheckbox
		  self.FileList.Cell(self.FileList.LastIndex,1) = documentName
		  self.FileList.Cell(self.FileList.LastIndex,2) = fileName
		  self.FileList.CellTag(self.FileList.LastIndex,2) = filePath
		  self.FileList.Cell(self.FileList.LastIndex,3) = "ยังไม่ได้อัพโหลด"
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ValidateFileType(fileName as String) As Boolean
		  Dim extension As String
		  Dim result As Boolean
		  extension = Self.GetExtensionFile(fileName)
		  If extension = "pdf" Or extension = "jpg" Or extension = "png" Then
		    result = true
		  Else
		    result = False
		  End
		  return result
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		CategoryName As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Deleted As JsonItem
	#tag EndProperty


#tag EndWindowCode

#tag Events FileList
	#tag Event
		Sub Open()
		  Me.DefaultRowHeight = 25
		  'Me.AcceptFileDrop(AllFiles.Any)
		End Sub
	#tag EndEvent
	#tag Event
		Sub DropObject(obj As DragItem, action As Integer)
		  'If obj.FolderItemAvailable Then
		  ''Me.AddRow(obj.FolderItem.Name)
		  'Me.Cell(me.ListIndex,1) = obj.FolderItem.Name
		  'Me.CellTag(Me.ListIndex,1) = obj.FolderItem.NativePath
		  'End If
		End Sub
	#tag EndEvent
	#tag Event
		Sub DoubleClick()
		  If Me.ListIndex >= 0 Then
		    If Me.CellTag(Me.ListIndex,2) <> "" Then
		      Dim filePath As String = Me.CellTag(Me.ListIndex,2)
		      Dim file As New FolderItem(filePath, FolderItem.PathTypeNative)
		      If file <> Nil And file.Exists Then
		        file.Launch
		      End If
		    Else
		      MsgBox "กรุณาอัพโหลดไฟล์"
		    End
		  End If
		End Sub
	#tag EndEvent
	#tag Event
		Function CellTextPaint(g As Graphics, row As Integer, column As Integer, x as Integer, y as Integer) As Boolean
		  if column = 3 then
		    if me.Cell(row,column) = "ยังไม่ได้อัพโหลด" then
		      g.ForeColor = &cFF0000
		    end
		    if me.Cell(row,column) = "อัพโหลดแล้ว" then
		      g.ForeColor = &c0EAE21
		    end
		  end
		End Function
	#tag EndEvent
#tag EndEvents
#tag Events PushButton2
	#tag Event
		Sub Action()
		  Dim jsonUpload as new JSONItem
		  
		  for i as integer = 0 to self.FileList.ListCount-1
		    Dim jsonUploadItem as new JSONItem
		    if self.FileList.Cell(i,1)<>"" then
		      Dim filePath As String = self.FileList.CellTag(i,2)
		      Dim document As New FolderItem(filePath, FolderItem.PathTypeNative)
		      
		      Dim f as new FolderItem
		      f = f.Child("upload")
		      Dim s as String
		      select case self.FileList.Cell(i,1)
		      case "สำเนาปริญญาบัตร ปริญญาตรี"
		        dim d as new UUIDMBS
		        s = "bachelorDegreeCertificate"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("BachelorDegreeCertificate") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      case "สำเนาปริญญาบัตร ปริญญาโท"
		        dim d as new UUIDMBS
		        s = "masterDegreeCertificate"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("MasterDegreeCertificate") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      case "สำเนาปริญญาบัตร ปริญญาเอก"
		        dim d as new UUIDMBS
		        s = "doctorDegreeCertificate"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("DoctorDegreeCertificate") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      case "ใบแสดงผลการศึกษาระดับ ปริญญาตรี"
		        dim d as new UUIDMBS
		        s = "bachelorDegreeTranscript"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("BachelorDegreeTranscript") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      case "ใบแสดงผลการศึกษาระดับ ปริญญาโท"
		        dim d as new UUIDMBS
		        s = "masterDegreeTranscript"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("MasterDegreeTranscript") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      case "ใบแสดงผลการศึกษาระดับ ปริญญาเอก"
		        dim d as new UUIDMBS
		        s = "doctorDegreeTranscript"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("DoctorDegreeTranscript") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      case "สำเนาบัตรประชาชน"
		        dim d as new UUIDMBS
		        s = "identificationCard"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("IdentificationCard") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      case "สำเนาทะเบียนบ้าน"
		        dim d as new UUIDMBS
		        s = "homeRegistration"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("HomeRegistration") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      case "ใบรับรองแพทย์"
		        dim d as new UUIDMBS
		        s = "medicalCertificate"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("MedicalCertificate") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      case "หลักฐานทางการทหาร (ถ้ามี)"
		        dim d as new UUIDMBS
		        s = "militaryServiceRecord"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("MilitaryServiceRecord") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      case "ใบสำคัญการเปลี่ยนชื่อ, นามสกุล, ฯลฯ"
		        dim d as new UUIDMBS
		        s = "changingNotification"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("ChangingNotification") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      case "หนังสือรับรองจากอาจารย์ที่ปรึกษา 1"
		        dim d as new UUIDMBS
		        s = "recommendationLetter1st"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("RecommendationLetter1st") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      case "หนังสือรับรองจากอาจารย์ที่ปรึกษา 2"
		        dim d as new UUIDMBS
		        s = "recommendationLetter2nd"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("RecommendationLetter2nd") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      case "ใบอนุญาตประกอบวิชาชีพ เวชกรรม"
		        dim d as new UUIDMBS
		        s = "licenseToPracticeMedicine"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("LicenseToPracticeMedicine") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      case "หลักฐานผ่านทดสอบ ความสามารถภาษาอังกฤษ"
		        dim d as new UUIDMBS
		        s = "englishTestScore"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("EnglishTestScore") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      case "วุฒิบัตร/หนังสืออนุมัติบัตร เฉพาะทาง 1"
		        dim d as new UUIDMBS
		        s = "diplomaInField1st"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("DiplomaInField1st") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      case "วุฒิบัตร/หนังสืออนุมัติบัตร เฉพาะทาง 2"
		        dim d as new UUIDMBS
		        s = "diplomaInField2nd"+d.randomUUID.ValueFormattedString+"."+self.GetExtensionFile(document.Name)
		        jsonUploadItem.Value("DiplomaInField2nd") = s
		        f = f.Child(s)
		        document.CopyFileTo(f)
		      end
		      
		    end
		    jsonUpload.Append(jsonUploadItem)
		  next
		  
		  PersonnelWindow.cFormPersonnel.CurrentFileUpload = jsonUpload
		  Self.SetFileUploadStatus
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BevelButton1
	#tag Event
		Sub Action()
		  Dim ulm as new UploadDialogWindow
		  ulm.ShowModal
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BevelButton2
	#tag Event
		Sub Action()
		  Dim deleted(-1) as String
		  
		  for i as integer = 0 to self.FileList.ListCount-1
		    if self.FileList.CellCheck(i,0) = true then
		      deleted.Append(Str(i))
		    end
		  next
		  if deleted.Ubound >= 0 then
		    for i as integer = deleted.Ubound to 0 Step -1
		      self.FileList.RemoveRow(Val(deleted(i)))
		    next
		  else
		    MsgBox "กรุณาเลือกข้อมูลที่ต้องการจะลบ"
		  end
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CategoryName"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
