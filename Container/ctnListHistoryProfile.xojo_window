#tag Window
Begin ContainerControl ctnListHistoryProfile
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   548
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   True
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   904
   Begin BevelButton btnDelete
      AcceptFocus     =   True
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "ลบ"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   824
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   2
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   20
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   60
   End
   Begin BevelButton btnUpdate
      AcceptFocus     =   True
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "แก้ไข"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   765
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   2
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   20
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   60
   End
   Begin BevelButton btnAdd
      AcceptFocus     =   True
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "เพิ่ม"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   706
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   2
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   20
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   60
   End
   Begin Listbox lstHistoryProfile
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   0
      ColumnsResizable=   False
      ColumnWidths    =   ""
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   False
      GridLinesHorizontal=   2
      GridLinesVertical=   2
      HasHeading      =   True
      HeadingIndex    =   -1
      Height          =   474
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   True
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   54
      Underline       =   False
      UseFocusRing    =   False
      Visible         =   True
      Width           =   864
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin Label lblTopic
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   5
      TabPanelIndex   =   0
      Text            =   ""
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   20
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   289
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  self.Deleted = new JSONItem
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Function GetValue(category as String) As JSONItem
		  select case category
		  case "education"
		    Dim educationJson As New JSONItem
		    Dim educationNew As New JSONItem
		    Dim educationUpdate As New JSONItem
		    Dim educationDelete As New JSONItem
		    educationDelete = self.Deleted
		    'MsgBox educationDelete.ToString
		    
		    for i as integer = 0 to self.lstHistoryProfile.ListCount-1
		      Dim educationItem As New JSONItem
		      Dim mainCert as Boolean = False
		      Dim highCert as Boolean = False
		      if Str(self.lstHistoryProfile.CellState(i,1)) = "1" then
		        mainCert = True
		      end
		      if  Str(self.lstHistoryProfile.CellState(i,2)) = "1" then
		        highCert = True
		      end
		      educationItem.value("useDefault") = mainCert
		      educationItem.value("high") = highCert
		      educationItem.value("certificate") = self.lstHistoryProfile.CellTag(i,3)
		      educationItem.value("institution") = self.lstHistoryProfile.CellTag(i,4)
		      educationItem.value("faculty") = self.lstHistoryProfile.CellTag(i,5)
		      educationItem.value("major") = self.lstHistoryProfile.CellTag(i,6)
		      educationItem.value("gpa") = self.lstHistoryProfile.Cell(i,7)
		      educationItem.value("startYear") = self.lstHistoryProfile.Cell(i,8)
		      educationItem.value("endYear") = self.lstHistoryProfile.Cell(i,9)
		      educationItem.value("country") = self.lstHistoryProfile.Cell(i,10)
		      educationItem.value("remark") = self.lstHistoryProfile.Cell(i,11)
		      educationItem.value("honor") = "1"
		      
		      if self.lstHistoryProfile.CellTag(i,0) <> "" then
		        educationItem.value("id") = self.lstHistoryProfile.CellTag(i,0)
		        educationUpdate.Append(educationItem)
		      else
		        educationNew.Append(educationItem)
		      end
		    next
		    
		    if educationNew.Count > 0 then
		      educationJson.Value("new") = educationNew
		    end
		    if educationUpdate.Count > 0 then
		      educationJson.Value("update") = educationUpdate
		    end
		    if educationDelete.Count > 0 then
		      educationJson.Value("delete") = educationDelete
		    end
		    
		    'educationArray.Append(educationItem)
		    'educationJson.Value("new") = educationArray
		    'data.Value("education") = educationJson.ToString
		    return educationJson
		  case "relative"
		    Dim relativeJson As New JSONItem
		    for i as integer = 0 to self.lstHistoryProfile.ListCount-1
		      Dim relativeItem As New JSONItem
		      relativeItem.Value("nameTitleName") = self.lstHistoryProfile.Cell(i,1)
		      relativeItem.Value("nameTitle") = self.lstHistoryProfile.CellTag(i,1)
		      relativeItem.Value("firstName") = self.lstHistoryProfile.Cell(i,2)
		      relativeItem.Value("lastName") = self.lstHistoryProfile.Cell(i,3)
		      relativeItem.Value("gender") = self.lstHistoryProfile.Cell(i,4)
		      relativeItem.Value("relationship") = self.lstHistoryProfile.Cell(i,5)
		      relativeItem.Value("identificationNo") = self.lstHistoryProfile.Cell(i,6)
		      relativeItem.Value("job") = self.lstHistoryProfile.Cell(i,7)
		      relativeItem.Value("DOB") = self.lstHistoryProfile.Cell(i,8)
		      relativeItem.Value("status") = self.lstHistoryProfile.Cell(i,9)
		      relativeItem.Value("deathDate") = self.lstHistoryProfile.Cell(i,10)
		      relativeItem.Value("phoneNo") = self.lstHistoryProfile.Cell(i,11)
		      relativeItem.Value("remark") = self.lstHistoryProfile.Cell(i,12)
		      relativeJson.Append(relativeItem)
		    next
		    Return relativeJson
		  case "address"
		    Dim addressJson As New JSONItem
		    for i as integer = 0 to self.lstHistoryProfile.ListCount-1
		      Dim addressItem As New JSONItem
		      addressItem.Value("addressType") = self.lstHistoryProfile.CellTag(i,1)
		      addressItem.Value("addressTypeName") = self.lstHistoryProfile.Cell(i,1)
		      addressItem.Value("houseNo") = self.lstHistoryProfile.Cell(i,2)
		      addressItem.Value("province") =  self.lstHistoryProfile.CellTag(i,3)
		      addressItem.Value("provinceName") = self.lstHistoryProfile.Cell(i,3)
		      addressItem.Value("district") =  self.lstHistoryProfile.CellTag(i,4)
		      addressItem.Value("districtName") = self.lstHistoryProfile.Cell(i,4)
		      addressItem.Value("subdistrict") = self.lstHistoryProfile.CellTag(i,5)
		      addressItem.Value("subdistrictName") = self.lstHistoryProfile.Cell(i,5)
		      addressItem.Value("zipcode") = self.lstHistoryProfile.CellTag(i,6)
		      addressItem.Value("zipcodeName") = self.lstHistoryProfile.Cell(i,6)
		      addressItem.Value("homePhone") = self.lstHistoryProfile.Cell(i,7)
		      addressItem.Value("mobilePhone") =  self.lstHistoryProfile.Cell(i,8)
		      addressItem.Value("email") =  self.lstHistoryProfile.Cell(i,9)
		      addressItem.Value("addressDetails") = self.lstHistoryProfile.CellTag(i,10)
		      addressItem.Value("addressDetailsName") = self.lstHistoryProfile.Cell(i,10)
		      addressItem.Value("addressDetails1")  = self.lstHistoryProfile.CellTag(i,11)
		      addressItem.Value("addressDetails1Name") = self.lstHistoryProfile.Cell(i,11)
		      addressItem.Value("remark") = self.lstHistoryProfile.Cell(i,12)
		      addressJson.Append(addressItem)
		    next
		    Return addressJson
		  case "bank"
		    Dim bankJson As New JSONItem
		    Dim bankNew As New JSONItem
		    Dim bankUpdate As New JSONItem
		    Dim bankDelete As New JSONItem
		    bankDelete = self.Deleted
		    
		    for i as integer = 0 to self.lstHistoryProfile.ListCount-1
		      Dim bankItem As New JSONItem
		      'bankItem.Value("bank")  = self.lstHistoryProfile.CellTag(i,1)
		      bankItem.Value("bankId")  = self.lstHistoryProfile.CellTag(i,1)
		      bankItem.Value("bankName")  = self.lstHistoryProfile.Cell(i,1)
		      bankItem.Value("branch")  = self.lstHistoryProfile.CellTag(i,2)
		      bankItem.Value("bankBranchNo")  = self.lstHistoryProfile.Cell(i,2)
		      bankItem.Value("accountNo")  =  self.lstHistoryProfile.Cell(i,3)
		      bankItem.Value("accountName")  = self.lstHistoryProfile.Cell(i,4)
		      bankItem.Value("remark")  = self.lstHistoryProfile.Cell(i,5)
		      if self.lstHistoryProfile.CellTag(i,0) <> "" then
		        bankItem.value("id") = self.lstHistoryProfile.CellTag(i,0)
		        bankUpdate.Append(bankItem)
		      else
		        bankNew.Append(bankItem)
		      end
		    next
		    
		    if bankNew.Count > 0 then
		      bankJson.Value("new") = bankNew
		    end
		    if bankUpdate.Count > 0 then
		      bankJson.Value("update") = bankUpdate
		    end
		    if bankDelete.Count > 0 then
		      bankJson.Value("delete") = bankDelete
		    end
		    return bankJson
		    
		  case "income"
		    Dim incomeJson As New JSONItem
		    Dim incomeNew As New JSONItem
		    Dim incomeUpdate As New JSONItem
		    Dim incomeDelete As New JSONItem
		    incomeDelete = self.Deleted
		    
		    for i as integer = 0 to self.lstHistoryProfile.ListCount-1
		      Dim incomeItem As New JSONItem
		      
		      'incomeItem.Value("income")  = self.lstHistoryProfile.CellTag(i,1)
		      'incomeItem.Value("incomeDeductTypeId")  = self.lstHistoryProfile.CellTag(i,1)
		      'incomeItem.Value("incomeName") = self.lstHistoryProfile.Cell(i,1)
		      'incomeItem.Value("groupIncome") = self.lstHistoryProfile.Cell(i,2)
		      'incomeItem.Value("annulty") = self.lstHistoryProfile.CellTag(i,3)
		      'incomeItem.Value("annultyName") = self.lstHistoryProfile.Cell(i,3)
		      'incomeItem.Value("typeIncome") = self.lstHistoryProfile.Cell(i,4)
		      'incomeItem.Value("effectiveDate") = self.lstHistoryProfile.Cell(i,5) 
		      'incomeItem.Value("startDate") = self.lstHistoryProfile.Cell(i,5) 
		      'incomeItem.Value("endDate") = self.lstHistoryProfile.Cell(i,5) 
		      'incomeItem.Value("amount") = self.lstHistoryProfile.Cell(i,6)
		      'incomeItem.Value("remark") = self.lstHistoryProfile.Cell(i,7)
		      
		      incomeItem.Value("personnelIncomeDeductId") = self.lstHistoryProfile.CellTag(i,0)
		      incomeItem.Value("incomeDeductType") = self.lstHistoryProfile.CellTag(i,1)
		      incomeItem.Value("incomeDeductTypeName") =self.lstHistoryProfile.Cell(i,2)
		      incomeItem.Value("amount") = self.lstHistoryProfile.Cell(i,3)
		      incomeItem.Value("annuityName") = self.lstHistoryProfile.Cell(i,4)
		      incomeItem.Value("annuity") = self.lstHistoryProfile.CellTag(i,4)
		      incomeItem.Value("startDate") = self.lstHistoryProfile.Cell(i,5)
		      incomeItem.Value("endDate") = self.lstHistoryProfile.Cell(i,6)
		      incomeItem.Value("remark") = self.lstHistoryProfile.Cell(i,7)
		      
		      if self.lstHistoryProfile.CellTag(i,0) <> "" then
		        incomeItem.value("id") = self.lstHistoryProfile.CellTag(i,0)
		        incomeUpdate.Append(incomeItem)
		      else
		        incomeNew.Append(incomeItem)
		      end
		    next
		    
		    if incomeNew.Count > 0 then
		      incomeJson.Value("new") = incomeNew
		    end
		    if incomeUpdate.Count > 0 then
		      incomeJson.Value("update") = incomeUpdate
		    end
		    if incomeDelete.Count > 0 then
		      incomeJson.Value("delete") = incomeDelete
		    end
		    return incomeJson
		    
		  case "deduct"
		    Dim deductJson As New JSONItem
		    Dim deductNew As New JSONItem
		    Dim deductUpdate As New JSONItem
		    Dim deductDelete As New JSONItem
		    deductDelete = self.Deleted
		    
		    for i as integer = 0 to self.lstHistoryProfile.ListCount-1
		      Dim deductItem As New JSONItem
		      
		      'deductItem.Value("deduct") = self.lstHistoryProfile.CellTag(i,1)
		      'deductItem.Value("incomeDeductTypeId") = self.lstHistoryProfile.CellTag(i,1)
		      'deductItem.Value("deductName") = self.lstHistoryProfile.Cell(i,1)
		      'deductItem.Value("annulty") = self.lstHistoryProfile.CellTag(i,2)
		      'deductItem.Value("annultyName") = self.lstHistoryProfile.Cell(i,2)
		      'deductItem.Value("effectiveDate") = self.lstHistoryProfile.CellTag(i,3)
		      'deductItem.Value("startDate") = self.lstHistoryProfile.CellTag(i,4)
		      'deductItem.Value("endDate") = self.lstHistoryProfile.CellTag(i,4)
		      'deductItem.Value("principle") = self.lstHistoryProfile.CellTag(i,5)
		      'deductItem.Value("deductAccumulate") = self.lstHistoryProfile.CellTag(i,6)
		      'deductItem.Value("interest") = self.lstHistoryProfile.CellTag(i,7)
		      'deductItem.Value("interestAccumulate") = self.lstHistoryProfile.CellTag(i,8)
		      'deductItem.Value("deductPerAnnulty") = self.lstHistoryProfile.CellTag(i,9)
		      'deductItem.Value("interestPerAnnulty") = self.lstHistoryProfile.CellTag(i,10)
		      'deductItem.Value("remark") = self.lstHistoryProfile.CellTag(i,11)
		      'deductItem.Value("amount") = self.lstHistoryProfile.CellTag(i,5)
		      
		      deductItem.Value("personnelIncomeDeductId") = self.lstHistoryProfile.CellTag(i,0)
		      deductItem.Value("incomeDeductType") = self.lstHistoryProfile.CellTag(i,1)
		      deductItem.Value("incomeDeductTypeName") =self.lstHistoryProfile.Cell(i,2)
		      deductItem.Value("amount") = self.lstHistoryProfile.Cell(i,3)
		      deductItem.Value("annuityName") = self.lstHistoryProfile.Cell(i,4)
		      deductItem.Value("annuity") = self.lstHistoryProfile.CellTag(i,4)
		      deductItem.Value("startDate") = self.lstHistoryProfile.Cell(i,5)
		      deductItem.Value("endDate") = self.lstHistoryProfile.Cell(i,6)
		      deductItem.Value("remark") = self.lstHistoryProfile.Cell(i,7)
		      
		      if self.lstHistoryProfile.CellTag(i,0) <> "" then
		        deductItem.value("id") = self.lstHistoryProfile.CellTag(i,0)
		        deductUpdate.Append(deductItem)
		      else
		        deductNew.Append(deductItem)
		      end
		    next
		    
		    if deductNew.Count > 0 then
		      deductJson.Value("new") = deductNew
		    end
		    if deductUpdate.Count > 0 then
		      deductJson.Value("update") = deductUpdate
		    end
		    if deductDelete.Count > 0 then
		      deductJson.Value("delete") = deductDelete
		    end
		    return deductJson
		    
		  case "experience"
		    Dim experienceJson As New JSONItem
		    for i as integer = 0 to self.lstHistoryProfile.ListCount-1
		      Dim experienceItem As New JSONItem
		      
		      experienceItem.Value("company")  = self.lstHistoryProfile.Cell(i,1) 
		      experienceItem.Value("position")  = self.lstHistoryProfile.Cell(i,2)
		      experienceItem.Value("salary")  = self.lstHistoryProfile.Cell(i,3)
		      experienceItem.Value("effectiveDate") = self.lstHistoryProfile.Cell(i,4)
		      experienceItem.Value("endDate")  = self.lstHistoryProfile.Cell(i,5)
		      experienceItem.Value("jobType") = self.lstHistoryProfile.Cell(i,6)
		      experienceItem.Value("jobLevel") = self.lstHistoryProfile.Cell(i,7)
		      experienceItem.Value("jobDuration") = self.lstHistoryProfile.Cell(i,8)
		      experienceItem.Value("leaveReason") = self.lstHistoryProfile.Cell(i,9)
		      experienceItem.Value("remark") = self.lstHistoryProfile.Cell(i,10)
		      
		      experienceJson.Append(experienceItem)
		    next
		    Return experienceJson
		  case "fund"
		    Dim fundJson As New JSONItem
		    Dim fundNew As New JSONItem
		    Dim fundUpdate As New JSONItem
		    Dim fundDelete As New JSONItem
		    fundDelete = self.Deleted
		    
		    for i as integer = 0 to self.lstHistoryProfile.ListCount-1
		      Dim fundItem As New JSONItem
		      
		      fundItem.Value("fund") = self.lstHistoryProfile.CellTag(i,1)
		      fundItem.Value("fundName") = self.lstHistoryProfile.Cell(i,1)
		      fundItem.Value("identificationNo") = self.lstHistoryProfile.Cell(i,2)
		      fundItem.Value("memberDate") = self.lstHistoryProfile.Cell(i,3)
		      fundItem.Value("leaveDate") = self.lstHistoryProfile.Cell(i,4)
		      fundItem.Value("empAccumulate") = self.lstHistoryProfile.CellTag(i,5)
		      fundItem.Value("empAccumulateName") = self.lstHistoryProfile.Cell(i,5)
		      fundItem.Value("compAccumulate") = self.lstHistoryProfile.CellTag(i,6)
		      fundItem.Value("compAccumulateName") = self.lstHistoryProfile.Cell(i,6)
		      fundItem.Value("policy") = self.lstHistoryProfile.CellTag(i,7)
		      fundItem.Value("policyName") = self.lstHistoryProfile.Cell(i,7)
		      fundItem.Value("remark") = self.lstHistoryProfile.Cell(i,8)
		      
		      if self.lstHistoryProfile.CellTag(i,0) <> "" then
		        fundItem.value("id") = self.lstHistoryProfile.CellTag(i,0)
		        fundUpdate.Append(fundItem)
		      else
		        fundNew.Append(fundItem)
		      end
		    next
		    
		    if fundNew.Count > 0 then
		      fundJson.Value("new") = fundNew
		    end
		    if fundUpdate.Count > 0 then
		      fundJson.Value("update") = fundUpdate
		    end
		    if fundDelete.Count > 0 then
		      fundJson.Value("delete") = fundDelete
		    end
		    return fundJson
		    
		  case "leave"
		    Dim leaveJson As New JSONItem
		    Dim leaveNew As New JSONItem
		    Dim leaveUpdate As New JSONItem
		    Dim leaveDelete As New JSONItem
		    leaveDelete = self.Deleted
		    
		    for i as integer = 0 to self.lstHistoryProfile.ListCount-1
		      Dim leaveItem As New JSONItem
		      
		      leaveItem.Value("leaveType") = self.lstHistoryProfile.CellTag(i,1)
		      leaveItem.Value("leaveTypeName") =  self.lstHistoryProfile.Cell(i,1)
		      leaveItem.Value("startDate") = self.lstHistoryProfile.Cell(i,2)
		      leaveItem.Value("endDate") = self.lstHistoryProfile.Cell(i,3)
		      leaveItem.Value("accumulateLeaveDay") = self.lstHistoryProfile.Cell(i,4)
		      leaveItem.Value("limitLeaveDay") = self.lstHistoryProfile.Cell(i,5)
		      leaveItem.Value("totalLeaveDay") = self.lstHistoryProfile.Cell(i,6)
		      leaveItem.Value("remaintLeaveDay") = self.lstHistoryProfile.Cell(i,7)
		      leaveItem.Value("remark") = self.lstHistoryProfile.Cell(i,8)
		      
		      
		      if self.lstHistoryProfile.CellTag(i,0) <> "" then
		        leaveItem.value("id") = self.lstHistoryProfile.CellTag(i,0)
		        leaveUpdate.Append(leaveItem)
		      else
		        leaveNew.Append(leaveItem)
		      end
		    next
		    
		    if leaveNew.Count > 0 then
		      leaveJson.Value("new") = leaveNew
		    end
		    if leaveUpdate.Count > 0 then
		      leaveJson.Value("update") = leaveUpdate
		    end
		    if leaveDelete.Count > 0 then
		      leaveJson.Value("delete") = leaveDelete
		    end
		    return leaveJson
		    
		  case "name-change"
		    Dim ncJson As New JSONItem
		    Dim ncNew As New JSONItem
		    Dim ncUpdate As New JSONItem
		    Dim ncDelete As New JSONItem
		    ncDelete = self.Deleted
		    
		    for i as integer = 0 to self.lstHistoryProfile.ListCount-1
		      Dim ncItem As New JSONItem
		      
		      ncItem.Value("title") = self.lstHistoryProfile.Celltag(i,1)
		      ncItem.Value("titleName") = self.lstHistoryProfile.Cell(i,1)
		      ncItem.Value("firstNameTh") = self.lstHistoryProfile.Cell(i,2)
		      ncItem.Value("lastNameTh") = self.lstHistoryProfile.Cell(i,3)
		      ncItem.Value("firstNameEng") = self.lstHistoryProfile.Cell(i,4)
		      ncItem.Value("lastNameEng") = self.lstHistoryProfile.Cell(i,5)
		      ncItem.Value("nickName") = self.lstHistoryProfile.Cell(i,6)
		      ncItem.Value("soundDex") = self.lstHistoryProfile.Cell(i,7)
		      ncItem.Value("changeNameDate") = self.lstHistoryProfile.Cell(i,8)
		      ncItem.Value("reason") = self.lstHistoryProfile.Cell(i,9)
		      ncItem.Value("remark") = self.lstHistoryProfile.Cell(i,10)
		      
		      if self.lstHistoryProfile.CellTag(i,0) <> "" then
		        ncItem.value("id") = self.lstHistoryProfile.CellTag(i,0)
		        ncUpdate.Append(ncItem)
		      else
		        ncNew.Append(ncItem)
		      end
		    next
		    
		    if ncNew.Count > 0 then
		      ncJson.Value("new") = ncNew
		    end
		    if ncUpdate.Count > 0 then
		      ncJson.Value("update") = ncUpdate
		    end
		    if ncDelete.Count > 0 then
		      ncJson.Value("delete") = ncDelete
		    end
		    return ncJson
		    
		  case "skill"
		    Dim skillJson As New JSONItem
		    for i as integer = 0 to self.lstHistoryProfile.ListCount-1
		      Dim skillItem As New JSONItem
		      
		      skillItem.Value("skill") = self.lstHistoryProfile.CellTag(i,1)
		      skillItem.Value("skillName") = self.lstHistoryProfile.Cell(i,1)
		      skillItem.Value("skillDetails") = self.lstHistoryProfile.CellTag(i,2)
		      skillItem.Value("skillDetailsName") = self.lstHistoryProfile.Cell(i,2)
		      skillItem.Value("skillLevel") = self.lstHistoryProfile.CellTag(i,3)
		      skillItem.Value("skillLevelName") = self.lstHistoryProfile.Cell(i,3)
		      skillItem.Value("remark") = self.lstHistoryProfile.Cell(i,4)
		      
		      skillJson.Append(skillItem)
		    next
		    Return skillJson
		  else
		    System.DebugLog("category ที่ส่งมามีปัญหาไม่ตรงกับ case")
		  end
		  
		  
		  Exception err as KeyNotFoundException
		  Exception err as JSONException
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetAddress()
		  Dim addressHeading() as String = Array("ลำดับ", "ประเภทที่อยู่","บ้านเลขที่","จังหวัด","อำเภอ/เขต","ตำบล/แขวง","รหัสไปรษณีย์","โทรศัพท์บ้าน" _
		  ,"โทรศัพท์มือถือ","อีเมลล์","ลักษณะที่อยู่อาศัย","ลักษณะที่อยู่อาศัย1","หมายเหตุ")
		  
		  me.lstHistoryProfile.ColumnCount = addressHeading.Ubound+1
		  me.lstHistoryProfile.ColumnWidths = "5%,30%,30%,30%,30%,30%,30%,30%,30%,30%,30%,30%,30%" 
		  me.lstHistoryProfile.HasHeading = True
		  
		  for i as Integer = 0 to addressHeading.Ubound
		    me.lstHistoryProfile.Heading(i) = addressHeading(i)
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetBankAccount()
		  Dim bankAccountHeading() as String = Array("ลำดับ", "ธนาคาร","รหัสสาขา","เลขที่บัญชี","ชื่อบัญชี","หมายเหตุ")
		  
		  me.lstHistoryProfile.ColumnCount = bankAccountHeading.Ubound+1
		  me.lstHistoryProfile.HasHeading = True
		  
		  for i as Integer = 0 to bankAccountHeading.Ubound
		    me.lstHistoryProfile.Heading(i) = bankAccountHeading(i)
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SetDatatoJson() As JSONItem
		  Dim category as String
		  Dim jsonGetData as new JSONItem
		  category = self.CategoryName
		  select case category
		  case "skill"
		    jsonGetData.Value("order") = self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex)
		    jsonGetData.Value("skill") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1)
		    jsonGetData.Value("skillDetails") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,2)
		    jsonGetData.Value("skillLevel") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,3)
		    jsonGetData.Value("remark") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4)
		  case "name-change"
		    jsonGetData.Value("order") = self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex)
		    jsonGetData.Value("title") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1)
		    jsonGetData.Value("firstNameTh") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2)
		    jsonGetData.Value("lastNameTh") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,3)
		    jsonGetData.Value("firstNameEng") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4)
		    jsonGetData.Value("lastNameEng") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5)
		    jsonGetData.Value("nickName") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,6)
		    jsonGetData.Value("soundDex") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7)
		    jsonGetData.Value("changeNameDate") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,9)
		    jsonGetData.Value("reason") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8)
		    jsonGetData.Value("remark") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,11)
		  case "leave"
		    jsonGetData.Value("order") = self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex)
		    jsonGetData.Value("leaveType") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1)
		    jsonGetData.Value("startDate") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2)
		    jsonGetData.Value("endDate") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,3)
		    jsonGetData.Value("accumulateLeaveDay") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4)
		    jsonGetData.Value("limitLeaveDay") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5)
		    jsonGetData.Value("totalLeaveDay") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,6)
		    jsonGetData.Value("remaintLeaveDay") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7)
		    jsonGetData.Value("remark") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8)
		  case "fund"
		    jsonGetData.Value("order") = self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex)
		    jsonGetData.Value("fund") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1)
		    jsonGetData.Value("identificationNo") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2)
		    jsonGetData.Value("memberDate") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,3)
		    jsonGetData.Value("leaveDate") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4)
		    jsonGetData.Value("empAccumulate") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,5)
		    jsonGetData.Value("compAccumulate") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,6)
		    jsonGetData.Value("policy") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,7)
		    jsonGetData.Value("remark") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8)
		  case "experience"
		    jsonGetData.Value("order") = self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex)
		    jsonGetData.Value("company") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1)
		    jsonGetData.Value("position") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2)
		    jsonGetData.Value("salary") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,3)
		    jsonGetData.Value("effectiveDate") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4)
		    jsonGetData.Value("endDate") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5)
		    jsonGetData.Value("jobType") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,6)
		    jsonGetData.Value("jobLevel") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7)
		    jsonGetData.Value("jobDuration") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8)
		    jsonGetData.Value("leaveReason") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,9)
		    jsonGetData.Value("remark") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,10)
		  case "event"
		    jsonGetData.Value("order") = self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex)
		    jsonGetData.Value("topic") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1)
		    jsonGetData.Value("createdDate") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2)
		    jsonGetData.Value("income") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,3)
		    jsonGetData.Value("effectiveDate") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4)
		    jsonGetData.Value("endDate") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5)
		    jsonGetData.Value("department") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,6)
		    jsonGetData.Value("position") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7)
		    jsonGetData.Value("level") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8)
		    jsonGetData.Value("score") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,9)
		    jsonGetData.Value("remark") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,10)
		  case "deduct"
		    jsonGetData.Value("order") = self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex)
		    jsonGetData.Value("deduct") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1)
		    jsonGetData.Value("annulty") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2)
		    jsonGetData.Value("effectiveDate") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,3)
		    jsonGetData.Value("endDate") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4)
		    jsonGetData.Value("principle") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5)
		    jsonGetData.Value("deductAccumulate") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,6)
		    jsonGetData.Value("interest") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7)
		    jsonGetData.Value("interestAccumulate") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8)
		    jsonGetData.Value("deductPerAnnulty") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,9)
		    jsonGetData.Value("interestPerAnnulty") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,10)
		    jsonGetData.Value("remark") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,11)
		  case "income"
		    jsonGetData.Value("order") = self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex)
		    jsonGetData.Value("income") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1)
		    jsonGetData.Value("groupIncome") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2)
		    jsonGetData.Value("annulty") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,3)
		    jsonGetData.Value("typeIncome") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4)
		    jsonGetData.Value("effectiveDate") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5)
		    jsonGetData.Value("amount") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,6)
		    jsonGetData.Value("remark") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7)
		  case "bank"
		    jsonGetData.Value("order") = self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex)
		    jsonGetData.Value("bank") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1)
		    jsonGetData.Value("branch") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,2)
		    jsonGetData.Value("accountNo") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,3)
		    jsonGetData.Value("accountName") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4)
		    jsonGetData.Value("remark") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5)
		  case "address"
		    jsonGetData.Value("order") = self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex)
		    jsonGetData.Value("addressType") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1)
		    jsonGetData.Value("houseNo") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2)
		    jsonGetData.Value("province") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,3)
		    jsonGetData.Value("district") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,4)
		    jsonGetData.Value("subdistrict") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,5)
		    jsonGetData.Value("zipcode") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,6)
		    jsonGetData.Value("homePhone") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7)
		    jsonGetData.Value("mobilePhone") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8)
		    jsonGetData.Value("email") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,9)
		    jsonGetData.Value("addressDetails") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,10)
		    jsonGetData.Value("addressDetails1") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,11)
		    jsonGetData.Value("remark") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,12)
		  case "relative"
		    jsonGetData.Value("order") = self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex)
		    jsonGetData.Value("nameTitle") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1)
		    jsonGetData.Value("firstName") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2)
		    jsonGetData.Value("lastName") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,3)
		    jsonGetData.Value("gender") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4)
		    jsonGetData.Value("relationship") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5)
		    jsonGetData.Value("identificationNo") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,6)
		    jsonGetData.Value("job") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7)
		    jsonGetData.Value("DOB") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8)
		    jsonGetData.Value("status") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,9)
		    jsonGetData.Value("deathDate") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,10)
		    jsonGetData.Value("phoneNo") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,11)
		    jsonGetData.Value("remark") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,12)
		    
		  case "education"
		    'jsonGetData.Value("id")  = ""
		    jsonGetData.Value("order")  = self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex)
		    jsonGetData.Value("mainCert") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,1)
		    jsonGetData.Value("highCert") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2)
		    jsonGetData.Value("certificate") = self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,3)
		    jsonGetData.Value("institution") =  self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,4)
		    jsonGetData.Value("faculty") =  self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,5)
		    jsonGetData.Value("major") =  self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,6)
		    jsonGetData.Value("gpa") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7)
		    jsonGetData.Value("startYear") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8)
		    jsonGetData.Value("endYear") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,9)
		    jsonGetData.Value("country") = self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,10)
		    jsonGetData.Value("remark") =self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,11)
		  end
		  
		  Return jsonGetData
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetDatatoList(myData as JSONItem)
		  Dim category as String
		  category = self.CategoryName
		  'MsgBox category
		  select case category
		  case "skill"
		    if myData.Value("order").StringValue = "" then
		      self.lstHistoryProfile.AddRow()
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,0) = self.lstHistoryProfile.ListCount.ToText
		      self.lstHistoryProfile.CellType(self.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,1) = myData.Value("skillName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,1) = myData.Value("skill")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,2) = myData.Value("skillDetailsName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,2) = myData.Value("skillDetails")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,3) = myData.Value("skillLevelName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,3) = myData.Value("skillLevel")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,4) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.LastIndex) = self.lstHistoryProfile.LastIndex
		    else
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,1) = myData.Value("skillName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1) = myData.Value("skill")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2) = myData.Value("skillDetailsName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,2) = myData.Value("skillDetails")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,3) = myData.Value("skillLevelName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,3) = myData.Value("skillLevel")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex) = self.lstHistoryProfile.LastIndex
		    end
		  case "name-change"
		    if myData.Value("order").StringValue = "" then
		      self.lstHistoryProfile.AddRow()
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,0) = self.lstHistoryProfile.ListCount.ToText
		      self.lstHistoryProfile.CellType(self.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,1) = myData.Value("titleName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,1) = myData.Value("title")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,2) = myData.Value("firstNameTh")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,3) = myData.Value("lastNameTh")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,4) = myData.Value("firstNameEng")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,5) = myData.Value("lastNameEng")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,6) = myData.Value("nickName")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,7) = myData.Value("soundDex")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,9) = myData.Value("changeNameDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,8) = myData.Value("reason")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,11) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.LastIndex) = self.lstHistoryProfile.LastIndex
		    else
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,1) = myData.Value("titleName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1) = myData.Value("title")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2) = myData.Value("firstNameTh")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,3) = myData.Value("lastNameTh")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4) = myData.Value("firstNameEng")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5) = myData.Value("lastNameEng")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,6) = myData.Value("nickName")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7) = myData.Value("soundDex")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,9) = myData.Value("changeNameDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8) = myData.Value("reason")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,11) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex) = self.lstHistoryProfile.LastIndex
		    end
		  case "leave"
		    if myData.Value("order").StringValue = "" then
		      self.lstHistoryProfile.AddRow()
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,0) = self.lstHistoryProfile.ListCount.ToText
		      self.lstHistoryProfile.CellType(self.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,1) = myData.Value("leaveTypeName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,1) = myData.Value("leaveType")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,2) = myData.Value("startDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,3) = myData.Value("endDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,4) = myData.Value("accumulateLeaveDay")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,5) = myData.Value("limitLeaveDay")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,6) = myData.Value("totalLeaveDay")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,7) = myData.Value("remaintLeaveDay")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,8) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.LastIndex) = self.lstHistoryProfile.LastIndex
		    else
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,1) = myData.Value("leaveTypeName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1) = myData.Value("leaveType")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2) = myData.Value("startDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,3) = myData.Value("endDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4) = myData.Value("accumulateLeaveDay")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5) = myData.Value("limitLeaveDay")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,6) = myData.Value("totalLeaveDay")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7) = myData.Value("remaintLeaveDay")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex) = self.lstHistoryProfile.LastIndex
		    end
		  case "fund"
		    if myData.Value("order").StringValue = "" then
		      self.lstHistoryProfile.AddRow()
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,0) = self.lstHistoryProfile.ListCount.ToText
		      self.lstHistoryProfile.CellType(self.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,1) = myData.Value("fundName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,1) = myData.Value("fund")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,2) = myData.Value("identificationNo")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,3) = myData.Value("memberDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,4) = myData.Value("leaveDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,5) = myData.Value("empAccumulateName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,5) = myData.Value("empAccumulate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,6) = myData.Value("compAccumulateName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,6) = myData.Value("compAccumulate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,7) = myData.Value("policyName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,7) = myData.Value("policy")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,8) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.LastIndex) = self.lstHistoryProfile.LastIndex
		    else
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,1) = myData.Value("fundName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1) = myData.Value("fund")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2) = myData.Value("identificationNo")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,3) = myData.Value("memberDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4) = myData.Value("leaveDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5) = myData.Value("empAccumulateName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,5) = myData.Value("empAccumulate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,6) = myData.Value("compAccumulateName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,6) = myData.Value("compAccumulate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7) = myData.Value("policyName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,7) = myData.Value("policy")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex) = self.lstHistoryProfile.LastIndex
		    end
		  case "experience"
		    if myData.Value("order").StringValue = "" then
		      self.lstHistoryProfile.AddRow()
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,0) = self.lstHistoryProfile.ListCount.ToText
		      self.lstHistoryProfile.CellType(self.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,1) = myData.Value("company")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,2) = myData.Value("position")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,3) = myData.Value("salary")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,4) = myData.Value("effectiveDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,5) = myData.Value("endDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,6) = myData.Value("jobType")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,7) = myData.Value("jobLevel")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,8) = myData.Value("jobDuration")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,9) = myData.Value("leaveReason")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,10) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.LastIndex) = self.lstHistoryProfile.LastIndex
		    else
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,1) = myData.Value("company")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2) = myData.Value("position")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,3) = myData.Value("salary")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4) = myData.Value("effectiveDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5) = myData.Value("endDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,6) = myData.Value("jobType")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7) = myData.Value("jobLevel")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8) = myData.Value("jobDuration")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,9) = myData.Value("leaveReason")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,10) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex) = self.lstHistoryProfile.LastIndex
		    end
		  case "event"
		    if myData.Value("order").StringValue = "" then
		      self.lstHistoryProfile.AddRow()
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,0) = self.lstHistoryProfile.ListCount.ToText
		      self.lstHistoryProfile.CellType(self.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,1) = myData.Value("title")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,2) = myData.Value("createdDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,3) = myData.Value("eventDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,4) = myData.Value("detail")
		      'self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,5) = myData.Value("endDate")
		      'self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,6) = myData.Value("department")
		      'self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,7) = myData.Value("position")
		      'self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,8) = myData.Value("level")
		      'self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,9) = myData.Value("score")
		      'self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,10) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.LastIndex) = self.lstHistoryProfile.LastIndex
		    else
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,1) = myData.Value("title")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2) = myData.Value("createdDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,3) = myData.Value("eventDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4) = myData.Value("detail")
		      'self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5) = myData.Value("endDate")
		      'self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,6) = myData.Value("department")
		      'self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7) = myData.Value("position")
		      'self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8) = myData.Value("level")
		      'self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,9) = myData.Value("score")
		      'self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,10) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex) = self.lstHistoryProfile.LastIndex
		    end
		  case "deduct"
		    if myData.Value("order").StringValue = "" then
		      self.lstHistoryProfile.AddRow
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,0) = self.lstHistoryProfile.ListCount.ToText
		      self.lstHistoryProfile.CellType(self.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,0) = myData.Value("id")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,1) = myData.Value("deduct")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,2) = myData.Value("deductName")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,3) = myData.Value("amount")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,4) = myData.Value("annultyName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,4) = myData.Value("annulty")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,5) = myData.Value("startDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,6) = myData.Value("endDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,7) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.LastIndex) = self.lstHistoryProfile.LastIndex
		    else
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,1) = myData.Value("deduct")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,2) = myData.Value("deductName")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,3) = myData.Value("amount")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,4) = myData.Value("annultyName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,4) = myData.Value("annulty")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,5) = myData.Value("startDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,6) = myData.Value("endDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,7) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.LastIndex) = self.lstHistoryProfile.LastIndex
		    end
		  case "income"
		    if myData.Value("order").StringValue = "" then
		      self.lstHistoryProfile.AddRow
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,0) = self.lstHistoryProfile.ListCount.ToText
		      self.lstHistoryProfile.CellType(self.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,0) = myData.Value("id")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,1) = myData.Value("income")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,2) = myData.Value("incomeName")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,3) = myData.Value("amount")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,4) = myData.Value("annultyName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,4) = myData.Value("annulty")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,5) = myData.Value("startDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,6) = myData.Value("endDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,7) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.LastIndex) = self.lstHistoryProfile.LastIndex
		      
		    else
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,1) = myData.Value("income")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,2) = myData.Value("incomeName")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,3) = myData.Value("amount")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,4) = myData.Value("annuityName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,4) = myData.Value("annulty")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,5) = myData.Value("startDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,6) = myData.Value("endDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,7) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.LastIndex) = self.lstHistoryProfile.LastIndex
		    end
		  case "bank"
		    if myData.Value("order").StringValue = "" then
		      self.lstHistoryProfile.AddRow()
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,0) = self.lstHistoryProfile.ListCount.ToText
		      self.lstHistoryProfile.CellType(self.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,1) = myData.Value("bankName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,1) = myData.Value("bank")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,2) = myData.Value("branchName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,2) = myData.Value("branch")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,3) = myData.Value("accountNo")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,4) = myData.Value("accountName")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,5) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.LastIndex) = self.lstHistoryProfile.LastIndex
		    else
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,1) = myData.Value("bankName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1) = myData.Value("bank")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2) = myData.Value("branchName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,2) = myData.Value("branch")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,3) = myData.Value("accountNo")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4) = myData.Value("accountName")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex) = self.lstHistoryProfile.LastIndex
		    end
		  case "address"
		    if myData.Value("order").StringValue = "" then
		      self.lstHistoryProfile.AddRow()
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,0) = self.lstHistoryProfile.ListCount.ToText
		      self.lstHistoryProfile.CellType(self.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,1) = myData.Value("addressTypeName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,1) = myData.Value("addressType")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,2) = myData.Value("houseNo")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,3) = myData.Value("provinceName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,3) = myData.Value("province")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,4) = myData.Value("districtName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,4) = myData.Value("district")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,5) = myData.Value("subdistrictName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,5) = myData.Value("subdistrict")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,6) = myData.Value("zipcodeName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,6) = myData.Value("zipcode")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,7) = myData.Value("homePhone")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,8) = myData.Value("mobilePhone")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,9) = myData.Value("email")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,10) = myData.Value("addressDetailsName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,10) = myData.Value("addressDetails")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,11) = myData.Value("addressDetails1Name")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,11) = myData.Value("addressDetails1")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,12) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.LastIndex) = self.lstHistoryProfile.LastIndex
		    else
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,1) = myData.Value("addressTypeName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1) = myData.Value("addressType")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2) = myData.Value("houseNo")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,3) = myData.Value("provinceName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,3) = myData.Value("province")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4) = myData.Value("districtName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,4) = myData.Value("district")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5) = myData.Value("subdistrictName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,5) = myData.Value("subdistrict")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,6) = myData.Value("zipcodeName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,6) = myData.Value("zipcode")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7) = myData.Value("homePhone")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8) = myData.Value("mobilePhone")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,9) = myData.Value("email")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,10) = myData.Value("addressDetailsName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,10) = myData.Value("addressDetails")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,11) = myData.Value("addressDetails1Name")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,11) = myData.Value("addressDetails1")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,12) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex) = self.lstHistoryProfile.LastIndex
		    end
		  case "relative"
		    if myData.Value("order").StringValue = "" then
		      self.lstHistoryProfile.AddRow()
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,0) = self.lstHistoryProfile.ListCount.ToText
		      self.lstHistoryProfile.CellType(self.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,1) = myData.Value("nameTitleName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,1) = myData.Value("nameTitle")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,2) = myData.Value("firstName")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,3) = myData.Value("lastName")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,4) = myData.Value("gender")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,5) = myData.Value("relationship")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,6) = myData.Value("identificationNo")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,7) = myData.Value("job")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,8) = myData.Value("DOB")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,9) = myData.Value("status")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,10) = myData.Value("deathDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,11) = myData.Value("phoneNo")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,12) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.LastIndex) = self.lstHistoryProfile.LastIndex
		    else
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,1) = myData.Value("nameTitleName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,1) = myData.Value("nameTitle")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,2) = myData.Value("firstName")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,3) = myData.Value("lastName")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4) = myData.Value("gender")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5) = myData.Value("relationship")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,6) = myData.Value("identificationNo")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7) = myData.Value("job")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8) = myData.Value("DOB")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,9) = myData.Value("status")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,10) = myData.Value("deathDate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,11) = myData.Value("phoneNo")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,12) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex) = self.lstHistoryProfile.LastIndex
		    end
		    
		  case "education"
		    'MsgBox myData.Value("order").StringValue
		    if myData.Value("order").StringValue = "" then
		      self.lstHistoryProfile.AddRow()
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,0) = self.lstHistoryProfile.ListCount.ToText
		      self.lstHistoryProfile.CellType(self.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,0) = myData.Value("id")
		      self.lstHistoryProfile.CellType(self.lstHistoryProfile.LastIndex,1) = Listbox.TypeCheckbox
		      if myData.Value("mainCert") then
		        self.lstHistoryProfile.CellState(self.lstHistoryProfile.LastIndex,1) = CheckBox.CheckedStates.Checked
		      end
		      self.lstHistoryProfile.CellType(self.lstHistoryProfile.LastIndex,2) = Listbox.TypeCheckbox
		      if myData.Value("highCert") then
		        self.lstHistoryProfile.CellState(self.lstHistoryProfile.LastIndex,2) = CheckBox.CheckedStates.Checked
		      end
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,3) = myData.Value("certificateName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,3) = myData.Value("certificate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,4) = myData.Value("institutionName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,4) = myData.Value("institution")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,5) = myData.Value("facultyName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,5) = myData.Value("faculty")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,6) = myData.Value("majorName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.LastIndex,6) = myData.Value("major")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,7) = myData.Value("gpa")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,8) = myData.Value("startYear")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,9) = myData.Value("endYear")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,10) = myData.Value("country")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.LastIndex,11) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.LastIndex) = self.lstHistoryProfile.LastIndex
		    else
		      self.lstHistoryProfile.ListIndex = myData.Value("order").IntegerValue
		      'self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,0) = myData.Value("id")
		      self.lstHistoryProfile.CellType(self.lstHistoryProfile.ListIndex,1) = Listbox.TypeCheckbox
		      if myData.Value("mainCert") then
		        self.lstHistoryProfile.CellState(self.lstHistoryProfile.LastIndex,1) = CheckBox.CheckedStates.Checked
		      end
		      self.lstHistoryProfile.CellType(self.lstHistoryProfile.ListIndex,2) = Listbox.TypeCheckbox
		      if myData.Value("highCert") then
		        self.lstHistoryProfile.CellState(self.lstHistoryProfile.LastIndex,2) = CheckBox.CheckedStates.Checked
		      end
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,3) = myData.Value("certificateName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,3) = myData.Value("certificate")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,4) = myData.Value("institutionName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,4) = myData.Value("institution")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,5) = myData.Value("facultyName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,5) = myData.Value("faculty")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,6) = myData.Value("majorName")
		      self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,6) = myData.Value("major")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,7) = myData.Value("gpa")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,8) = myData.Value("startYear")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,9) = myData.Value("endYear")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,10) = myData.Value("country")
		      self.lstHistoryProfile.Cell(self.lstHistoryProfile.ListIndex,11) = myData.Value("remark")
		      self.lstHistoryProfile.RowTag(self.lstHistoryProfile.ListIndex) = self.lstHistoryProfile.LastIndex
		    end
		    
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetDeduct()
		  'Dim deductHeading() As String = Array("ลำดับ","รหัสเงินหัก", "งวดการจ่ายเงิน","วันที่มีผล","วันที่หยุดหัก","จำนวนเงินต้น","จำนวนเงินหักสะสม","จำนวนดอกเบี้ย" _
		  ',"จำนวนดอกเบี้ยสะสม","จำนวนเงินหักต่องวด","จำนวนดอกเบี้ยต่องวด","หมายเหตุ")
		  '
		  'Me.lstHistoryProfile.ColumnCount = deductHeading.Ubound+1
		  'Me.lstHistoryProfile.HasHeading = True
		  'Me.lstHistoryProfile.ColumnWidths = "5%,30%,30%,30%,30%,30%,30%,30%,30%,30%,30%,30%" 
		  '
		  'For i As Integer = 0 To deductHeading.Ubound
		  'Me.lstHistoryProfile.Heading(i) = deductHeading(i)
		  'Next
		  
		  
		  Dim deductHeading() As String = Array("ลำดับ","รหัสเงินหัก", "ชื่อรายหัก","จำนวนเงิน","ประเภทงวด","วันที่เริ่มต้น","วันที่สิ้นสุด","หมายเหตุ")
		  
		  Me.lstHistoryProfile.ColumnCount = deductHeading.Ubound+1
		  Me.lstHistoryProfile.HasHeading = True
		  Me.lstHistoryProfile.ColumnWidths = "5%,30%,30%,30%,30%,30%,30%,30%" 
		  
		  For i As Integer = 0 To deductHeading.Ubound
		    Me.lstHistoryProfile.Heading(i) = deductHeading(i)
		  Next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetDisableButton()
		  me.btnAdd.Enabled = False
		  me.btnDelete.Enabled = False
		  me.btnUpdate.Enabled = False
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetDiscipline()
		  Dim disciplineHeading() as String = Array("ลำดับ", "เลขที่เอกสาร","ชื่อ-นามสกุล","วันที่กระทำผิด","บทลงโทษ","หมายเหตุ")
		  
		  me.lstHistoryProfile.ColumnCount = disciplineHeading.Ubound+1
		  me.lstHistoryProfile.HasHeading = True
		  
		  for i as Integer = 0 to disciplineHeading.Ubound
		    me.lstHistoryProfile.Heading(i) = disciplineHeading(i)
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetEducation()
		  Dim educationHeading() as String = Array("ลำดับ","วุฒิหลัก","วุฒิสูงสุด","วุฒิการศึกษา","สถานศึกษา","คณะภาควิชา","สาขา","เกรดเฉลี่ย"," ปีที่เข้าการศึกษา","ปีที่จบการศึกษา", _
		  "ประเทศ","หมายเหตุ")
		  
		  me.lstHistoryProfile.ColumnCount = educationHeading.Ubound+1
		  me.lstHistoryProfile.ColumnWidths = "5%,6%,6%,30%,30%,30%,30%,10%,10%,10%,10%,30%" 
		  me.lstHistoryProfile.HasHeading = True
		  
		  for i as Integer = 0 to educationHeading.Ubound
		    me.lstHistoryProfile.Heading(i) = educationHeading(i)
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetEvent()
		  'Dim eventHeading() as String = Array("ลำดับ", "วันที่บันทึก","วันที่มีผลระหว่าง","แผนก","ตำแหน่ง","ลำดับขั้น","ระดับคะแนน","เรื่อง","หมายเหตุ")
		  Dim eventHeading() as String = Array("ลำดับ", "วันที่บันทึก","วันที่มีผลระหว่าง","เรื่อง","หมายเหตุ")
		  
		  me.lstHistoryProfile.ColumnCount = eventHeading.Ubound+1
		  me.lstHistoryProfile.HasHeading = True
		  
		  for i as Integer = 0 to eventHeading.Ubound
		    me.lstHistoryProfile.Heading(i) = eventHeading(i)
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetExperience()
		  Dim experienceHeading() As String = Array("ลำดับ", "หน่วยงาน","ตำแหน่ง","ระหว่างวันที่","เงินเดือน","เหตุที่ออก", _
		  "ระดับความชำนาญ","ประเภทงานที่ทำ","รวมเวลาทำงาน","ชื่อบริษัท","หมายเหตุ")
		  
		  Me.lstHistoryProfile.ColumnCount = experienceHeading.Ubound+1
		  Me.lstHistoryProfile.ColumnWidths = "5%,30%,30%,30%,30%,30%,30%,30%,30%,30%,30%" 
		  Me.lstHistoryProfile.HasHeading = True
		  
		  For i As Integer = 0 To experienceHeading.Ubound
		    Me.lstHistoryProfile.Heading(i) = experienceHeading(i)
		  Next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetFund()
		  Dim fundHeading() as String = Array("ลำดับ", "รหัสกองทุน","เลขที่บัตร","วันที่เป็นสมาชิก","หมายเหตุ")
		  
		  me.lstHistoryProfile.ColumnCount = fundHeading.Ubound+1
		  me.lstHistoryProfile.HasHeading = True
		  
		  for i as Integer = 0 to fundHeading.Ubound
		    me.lstHistoryProfile.Heading(i) = fundHeading(i)
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetHistoryPayment()
		  Dim hpHeading() as String = Array("ลำดับ", "เดือนปีที่จ่าย","งวดที่จ่าย","ชนิดเงิน","รหัสเงิน","ยอดเงิน1" _
		  ,"แผนก","หน่วยงาน","ตำแหน่ง","ประเภทพนักงาน","จำนวนวันที่ทำงาน","วันที่บันทึกล่าสุด","หมายเหตุ")
		  
		  me.lstHistoryProfile.ColumnCount = hpHeading.Ubound+1
		  me.lstHistoryProfile.HasHeading = True
		  
		  for i as Integer = 0 to hpHeading.Ubound
		    me.lstHistoryProfile.Heading(i) = hpHeading(i)
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetHistoryPosition()
		  Dim hposHeading() as String = Array("ลำดับ", "เลขที่เอกสาร","มีผลระหว่างวันที่","ตำแหน่ง","เงินได้","เหตุที่ออก" _
		  ,"แผนก","หน่วยงาน","ขั้น","หมายเหตุ")
		  
		  me.lstHistoryProfile.ColumnCount = hposHeading.Ubound+1
		  me.lstHistoryProfile.HasHeading = True
		  
		  for i as Integer = 0 to hposHeading.Ubound
		    me.lstHistoryProfile.Heading(i) = hposHeading(i)
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetIncome()
		  'Dim incomeHeading() As String = Array("ลำดับ","รหัสเงินได้", "กลุ่มเงินได้","งวดที่จ่าย","ชนิดเงินได้","มีผลตั้งแต่วันที่","จำนวนเงินได้","หมายเหตุ")
		  '
		  'Me.lstHistoryProfile.ColumnCount = incomeHeading.Ubound+1
		  'Me.lstHistoryProfile.HasHeading = True
		  'Me.lstHistoryProfile.ColumnWidths = "5%,30%,30%,30%,30%,30%,30%,30%" 
		  '
		  'For i As Integer = 0 To incomeHeading.Ubound
		  'Me.lstHistoryProfile.Heading(i) = incomeHeading(i)
		  'Next
		  
		  Dim incomeHeading() As String = Array("ลำดับ","รหัสเงินได้", "ชื่อรายได้","จำนวนเงิน","ประเภทงวด","วันที่เริ่มต้น","วันที่สิ้นสุด","หมายเหตุ")
		  
		  Me.lstHistoryProfile.ColumnCount = incomeHeading.Ubound+1
		  Me.lstHistoryProfile.HasHeading = True
		  Me.lstHistoryProfile.ColumnWidths = "5%,30%,30%,30%,30%,30%,30%,30%" 
		  
		  For i As Integer = 0 To incomeHeading.Ubound
		    Me.lstHistoryProfile.Heading(i) = incomeHeading(i)
		  Next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetInsignia()
		  Dim insigniaHeading() as String = Array("ลำดับ", "เครื่องราชอิสริยาภรณ์","วันที่ได้รับ","วันที่คืน","สถานะถือครอง","หมายเหตุ")
		  
		  me.lstHistoryProfile.ColumnCount = insigniaHeading.Ubound+1
		  me.lstHistoryProfile.HasHeading = True
		  
		  for i as Integer = 0 to insigniaHeading.Ubound
		    me.lstHistoryProfile.Heading(i) = insigniaHeading(i)
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetInvisibleButton()
		  me.btnAdd.Visible = False
		  me.btnDelete.Visible = False
		  me.btnUpdate.Visible = False
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetLeave()
		  Dim leaveHeading() as String = Array("ลำดับ", "ประเภทการขอลา","วันที่เริ่มต้นลา","วันที่สิ้นสุดวันลา", _
		  "สวัสดิการวันลายกมา","สวัสดิการวันลา","สวัสดิการคงเหลือทั้งสิ้น","หมายเหตุ")
		  
		  me.lstHistoryProfile.ColumnCount = leaveHeading.Ubound+1
		  me.lstHistoryProfile.HasHeading = True
		  
		  for i as Integer = 0 to leaveHeading.Ubound
		    me.lstHistoryProfile.Heading(i) = leaveHeading(i)
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetNameChange()
		  Dim ncHeading() As String = Array("ลำดับ", "คำนำหน้า","ชื่อ","นามสกุล","ชื่อภาษาอังกฤษ", _
		  "นามสกุลภาษาอังกฤษ","ชื่อเล่น","คำพ้องเสียง","เหตุผลที่เปลี่ยนชื่อ","วันที่เปลี่ยน","เลขที่เอกสาร","หมายเหตุ")
		  
		  Me.lstHistoryProfile.ColumnCount = ncHeading.Ubound+1
		  Me.lstHistoryProfile.ColumnWidths = "5%,30%,30%,30%,30%,30%,30%,30%,30%,30%,30%,30%" 
		  Me.lstHistoryProfile.HasHeading = True
		  
		  For i As Integer = 0 To ncHeading.Ubound
		    Me.lstHistoryProfile.Heading(i) = ncHeading(i)
		  Next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetRelative()
		  Dim relativeHeading() as String = Array("ลำดับ", "คำนำหน้า","ชื่อ","นามสกุล","เพศ","ความสัมพันธ์","เลขบัตรประชาชน" _
		  ,"งานที่ทำ","วันเกิด","สถานะการมีชีวิต","วันที่เสียชีวิต","เบอร์ติดต่อ","หมายเหตุ")
		  
		  me.lstHistoryProfile.ColumnCount = relativeHeading.Ubound+1
		  me.lstHistoryProfile.ColumnWidths = "5%,30%,30%,30%,30%,30%,30%,30%,30%,30%,30%,30%,30%" 
		  me.lstHistoryProfile.HasHeading = True
		  
		  for i as Integer = 0 to relativeHeading.Ubound
		    me.lstHistoryProfile.Heading(i) = relativeHeading(i)
		  next
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetSkill()
		  Dim skillHeading() as String = Array("ลำดับ", "ทักษะด้าน","ความชำนาญ","ระดับทักษะ","หมายเหตุ")
		  
		  me.lstHistoryProfile.ColumnCount = skillHeading.Ubound+1
		  Me.lstHistoryProfile.ColumnWidths = "5%,30%,30%,30%,30%" 
		  me.lstHistoryProfile.HasHeading = True
		  
		  for i as Integer = 0 to skillHeading.Ubound
		    me.lstHistoryProfile.Heading(i) = skillHeading(i)
		  next
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		CategoryName As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Deleted As JsonItem
	#tag EndProperty


#tag EndWindowCode

#tag Events btnDelete
	#tag Event
		Sub Action()
		  Dim d as New MessageDialog
		  Dim b as MessageDialogButton
		  d.icon=MessageDialog.GraphicCaution
		  
		  if self.lstHistoryProfile.ListIndex <> -1 Then
		    d.ActionButton.Caption="ลบ"
		    d.CancelButton.Visible=True
		    d.CancelButton.Caption="ยกเลิก"
		    d.AlternateActionButton.Caption="Don't Save"
		    d.Message="ท่านต้องการจะลบข้อมูลนี้ ใช่หรือไม่"
		    d.Explanation="ถ้าลบแล้วจะไม่สามารถ นำข้อมูลกลับมาได้อีก"
		    b=d.ShowModal
		    Select Case b  
		    Case d.ActionButton
		      if self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,0) <> "" then
		        self.Deleted.Append(self.lstHistoryProfile.CellTag(self.lstHistoryProfile.ListIndex,0))
		      end
		      Self.lstHistoryProfile.RemoveRow(self.lstHistoryProfile.ListIndex)
		    Case d.CancelButton
		    End select
		  else
		    d.Message="กรุณาเลือกข้อมูลในตารางด้านล่างก่อน เพื่อทำการลบ"
		    d.Explanation="ถ้าไม่มีข้อมูล, กรุณาสร้างชุดข้อมูลใหม่"
		    b=d.ShowModal
		  end
		  
		  'MsgBox self.Deleted.ToString
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnUpdate
	#tag Event
		Sub Action()
		  if self.lstHistoryProfile.ListIndex <> -1 Then
		    If Self.CategoryName <> "" Then
		      Select case self.CategoryName
		      case "education"
		        Dim edDlg as new EducationDialogWindow
		        'MsgBox Self.SetDatatoJson.ToString
		        edDlg.SetValue(Self.SetDatatoJson)
		        edDlg.btnAdd.Caption = "แก้ไข"
		        edDlg.ShowModal
		        If edDlg.CurrentEducation <> Nil Then
		          Self.SetDatatoList(edDlg.CurrentEducation)
		        End
		      case "relative"
		        Dim rtDlg as new RelativeDialogWindow
		        rtDlg.SetValue(Self.SetDatatoJson)
		        rtDlg.btnAdd.Caption = "แก้ไข"
		        rtDlg.ShowModal
		        If rtDlg.CurrentRelative <> Nil Then
		          Self.SetDatatoList(rtDlg.CurrentRelative)
		        End
		      case "address"
		        Dim addDlg as new AddressDialogWindow
		        addDlg.SetValue(Self.SetDatatoJson)
		        addDlg.btnAdd.Caption = "แก้ไข"
		        addDlg.ShowModal
		        If addDlg.CurrentAddress <> Nil Then
		          Self.SetDatatoList(addDlg.CurrentAddress)
		        End
		      case "bank"
		        Dim bkDlg as new BankDialogWindow
		        bkDlg.SetValue(Self.SetDatatoJson)
		        bkDlg.btnAdd.Caption = "แก้ไข"
		        bkDlg.ShowModal
		        If bkDlg.CurrentBankAccount <> Nil Then
		          Self.SetDatatoList(bkDlg.CurrentBankAccount)
		        End
		      case "income"
		        Dim incDlg as new IncomeDialogWindow
		        incDlg.SetValue(Self.SetDatatoJson)
		        incDlg.btnAdd.Caption = "แก้ไข"
		        incDlg.ShowModal
		        If incDlg.CurrentIncome <> Nil Then
		          Self.SetDatatoList(incDlg.CurrentIncome)
		        End
		      case "deduct"
		        Dim dcDlg as new DeductDialogWindow
		        dcDlg.SetValue(Self.SetDatatoJson)
		        dcDlg.btnAdd.Caption = "แก้ไข"
		        dcDlg.ShowModal
		        If dcDlg.CurrentDeduct <> Nil Then
		          Self.SetDatatoList(dcDlg.CurrentDeduct)
		        End
		      case "event"
		        Dim evDlg as new EventDialogWindow
		        evDlg.SetValue(Self.SetDatatoJson)
		        evDlg.btnAdd.Caption = "แก้ไข"
		        evDlg.ShowModal
		        If evDlg.CurrentEvent <> Nil Then
		          Self.SetDatatoList(evDlg.CurrentEvent)
		        End
		      case "experience"
		        Dim expDlg as new ExperienceDialogWindow
		        expDlg.SetValue(Self.SetDatatoJson)
		        expDlg.btnAdd.Caption = "แก้ไข"
		        expDlg.ShowModal
		        If expDlg.CurrentExperience <> Nil Then
		          Self.SetDatatoList(expDlg.CurrentExperience)
		        End
		      case "fund"
		        Dim fdDlg as new FundDialogWindow
		        fdDlg.SetValue(Self.SetDatatoJson)
		        fdDlg.btnAdd.Caption = "แก้ไข"
		        fdDlg.ShowModal
		        If fdDlg.CurrentFund <> Nil Then
		          Self.SetDatatoList(fdDlg.CurrentFund)
		        End
		      case "leave"
		        Dim lvDlg as new LeaveDialogWindow
		        lvDlg.SetValue(Self.SetDatatoJson)
		        lvDlg.btnAdd.Caption = "แก้ไข"
		        lvDlg.ShowModal
		        If lvDlg.CurrentLeave <> Nil Then
		          Self.SetDatatoList(lvDlg.CurrentLeave)
		        End
		      case "name-change"
		        Dim ncDlg as new NameChangeDialogWindow
		        ncDlg.SetValue(Self.SetDatatoJson)
		        ncDlg.btnAdd.Caption = "แก้ไข"
		        ncDlg.ShowModal
		        If ncDlg.CurrentNameChange <> Nil Then
		          Self.SetDatatoList(ncDlg.CurrentNameChange)
		        End
		      case "skill"
		        Dim skDlg as new SkillDialogWindow
		        skDlg.SetValue(Self.SetDatatoJson)
		        skDlg.btnAdd.Caption = "แก้ไข"
		        skDlg.ShowModal
		        If skDlg.CurrentSkill <> Nil Then
		          Self.SetDatatoList(skDlg.CurrentSkill)
		        End
		      end
		    end
		  end
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnAdd
	#tag Event
		Sub Action()
		  'MsgBox self.CategoryName
		  Select Case Self.CategoryName
		    
		  Case "education"
		    Dim edDlg As New EducationDialogWindow
		    edDlg.ShowModal
		    If edDlg.CurrentEducation <> Nil Then
		      Self.SetDatatoList(edDlg.CurrentEducation)
		    End
		    
		  Case "relative"
		    Dim rtDlg As New RelativeDialogWindow
		    rtDlg.ShowModal
		    If rtDlg.CurrentRelative <> Nil Then
		      Self.SetDatatoList(rtDlg.CurrentRelative)
		    End
		    
		  Case "event"
		    Dim evDlg As New EventDialogWindow
		    evDlg.ShowModal
		    If evDlg.CurrentEvent <> Nil Then
		      Self.SetDatatoList(evDlg.CurrentEvent)
		    End
		    
		  Case "experience"
		    Dim expDlg As New ExperienceDialogWindow
		    expDlg.ShowModal
		    If expDlg.CurrentExperience <> Nil Then
		      Self.SetDatatoList(expDlg.CurrentExperience)
		    End
		    
		  Case "fund"
		    Dim fdDlg As New FundDialogWindow
		    fdDlg.ShowModal
		    If fdDlg.CurrentFund <> Nil Then
		      Self.SetDatatoList(fdDlg.CurrentFund)
		    End
		    
		  Case "leave"
		    Dim lvDlg As New LeaveDialogWindow
		    lvDlg.ShowModal
		    If lvDlg.CurrentLeave <> Nil Then
		      Self.SetDatatoList(lvDlg.CurrentLeave)
		    End
		    
		  Case "name-change"
		    Dim ncDlg As New NameChangeDialogWindow
		    ncDlg.ShowModal
		    If ncDlg.CurrentNameChange <> Nil Then
		      Self.SetDatatoList(ncDlg.CurrentNameChange)
		    End
		    
		  Case "skill"
		    Dim skDlg As New SkillDialogWindow
		    skDlg.ShowModal
		    If skDlg.CurrentSkill <> Nil Then
		      Self.SetDatatoList(skDlg.CurrentSkill)
		    End
		    
		  Case "income"
		    Dim incDlg As New IncomeDialogWindow
		    incDlg.ShowModal
		    If incDlg.CurrentIncome <> Nil Then
		      Self.SetDatatoList(incDlg.CurrentIncome)
		    End
		    
		  Case "deduct"
		    Dim ducDlg As New DeductDialogWindow
		    ducDlg.ShowModal
		    If ducDlg.CurrentDeduct <> Nil Then
		      Self.SetDatatoList(ducDlg.CurrentDeduct)
		    End
		    
		  Case "address"
		    Dim addDlg As New AddressDialogWindow
		    addDlg.ShowModal
		    If addDlg.CurrentAddress <> Nil Then
		      Self.SetDatatoList(addDlg.CurrentAddress)
		    End
		    
		  Case "bank"
		    Dim bkDlg As New BankDialogWindow
		    bkDlg.ShowModal
		    If bkDlg.CurrentBankAccount <> Nil Then
		      Self.SetDatatoList(bkDlg.CurrentBankAccount)
		    End
		    
		    
		    
		  End
		  
		  
		  
		  'Dim ed as new EducationDialogWindow
		  'ed.Top = Screen(0).Height / 2
		  'ed.Left = Screen(0).Width / 2
		  'ed.ShowModal
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events lstHistoryProfile
	#tag Event
		Sub Open()
		  'Me.DefaultRowHeight = 25
		  Me.ScrollBarHorizontal = True
		End Sub
	#tag EndEvent
	#tag Event
		Function HeaderPressed(column as Integer) As Boolean
		  me.ColumnSortDirection(column) = me.SortAscending
		  me.SortedColumn = column
		  me.Sort
		End Function
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CategoryName"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
