#tag Window
Begin ContainerControl ctnRibbonMenuPersonnel
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   130
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   1280
   Begin RibbonCanvas Ribbon
      AcceptFocus     =   True
      AcceptTabs      =   True
      Animate         =   False
      AutoDeactivate  =   True
      AutoHide        =   False
      BackColor       =   &c00FFFFFF
      Backdrop        =   0
      BestHeight      =   0
      BorderColor     =   &c00000000
      DoubleBuffer    =   False
      Enabled         =   True
      EraseBackground =   False
      Freeze          =   False
      FullRefresh     =   False
      GearIcon        =   0
      Height          =   130
      HelpTag         =   ""
      Hidden          =   False
      HideButtonLeft  =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LeftOffset      =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      RedrawValue     =   0
      Scope           =   0
      ShowGearIcon    =   False
      ShowHideButton  =   False
      ShowRefreshTime =   False
      ShowTabs        =   False
      ShowTooltips    =   False
      Style           =   0
      TabCount        =   0
      TabIndex        =   1
      TabInnerOffset  =   0
      TabMidOffset    =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Transparent     =   True
      UseFocusRing    =   False
      Value           =   0
      Visible         =   True
      Width           =   1280
   End
End
#tag EndWindow

#tag WindowCode
#tag EndWindowCode

#tag Events Ribbon
	#tag Event
		Sub Open()
		  me.Value = 0
		  me.LeftOffset = 0 '24
		  
		  me.HideButtonLeft = 99999
		  
		  'me.ShowRefreshTime = True
		  me.Animate = True
		  
		  
		  Dim M As RibbonMenu
		  Dim O As RibbonMenuOption
		  Dim T As RibbonTab
		  Dim S As RibbonSection
		  Dim B As RibbonButton
		  
		  
		  //Main
		  M = New RibbonMenu(Ico("OpenFile-small"))
		  M.IconName = "OpenFile-small"
		  
		  me.Menu = M
		  M.Visible = True
		  
		  O = New RibbonMenuOption("OpenXML", "XML/Project", Ico("OpenFile"), False, True, True, "Help\nThis HelpTag has lines breaks.\nThis is a line break")
		  O.IconName = "OpenFile"
		  M.Options.Append O
		  O = New RibbonMenuOption("OpenPic", "&Picture", Ico("OpenFile"), True, True, True, "Helptag", "Test Header")
		  O.IconName = "OpenFile"
		  M.Options.Append O
		  O.SubOptions.Append New RibbonMenuOption("Test1", "Test1", Ico("OpenFile"), False, True, True, "", "This is some text", True)
		  O.SubOptions.Append New RibbonMenuOption("Test2", "Test2", Nil, False, True, True, "", "This is also some text", True)
		  M.Options.Append New RibbonMenuOption() //Separator
		  O = New RibbonMenuOption("Generate", "G&enerate",  Ico("BuildApp"), False, True)
		  O.IconName = "BuildApp"
		  M.Options.Append O
		  O = New RibbonMenuOption("ReloadPic", "Reload Picture", Ico("RefreshSearch"))
		  O.IconName = "RefreshSearch"
		  M.Options.Append O
		  
		  
		  //Tab 1
		  T = New RibbonTab("Home", "&Home")
		  me.Tabs.Append T
		  
		  S = New RibbonSection("Open", "Open", 0, False, 3)
		  T.Sections.Append S
		  
		  B = New RibbonButton("Home", "Home", Ico("OpenFile"), Ico("OpenFile-small"), 0, False, False, True, True, "Help\nThis HelpTag displays a picture. The picture is optional.\nThis is a line break", Ico("help1"))
		  B.IconName = "OpenFile"
		  B.SmallIconName = "OpenFile-small"
		  B.HelpPictureName  = "help1"
		  S.Buttons.Append B
		  B = New RibbonButton("OpenXML", "XML/Project", Ico("OpenFile"), Ico("OpenFile-small"), 0, False, False, True, True, "Help\nThis HelpTag displays a picture. The picture is optional.\nThis is a line break", Ico("help1"))
		  B.IconName = "OpenFile"
		  B.SmallIconName = "OpenFile-small"
		  B.HelpPictureName  = "help1"
		  S.Buttons.Append B
		  B = New RibbonButton("OpenPic", "Picture", Ico("OpenFile"), Ico("OpenFile-small"), 0, False, False, True, True, "Help\nThis HelpTag doesn't have any picture.")
		  B.IconName = "OpenFile"
		  B.SmallIconName = "OpenFile-small"
		  S.Buttons.Append B
		  B = New RibbonButton("ReloadPic", "Reload Picture", Ico("RefreshSearch"), Ico("RefreshSearch-small"))
		  B.IconName = "RefreshSearch"
		  B.SmallIconName = "RefreshSearch-small"
		  S.Buttons.Append B
		  
		  S = New RibbonSection("Save", "Save", 0, True, 2)
		  T.Sections.Append S
		  
		  B = New RibbonButton("Save", "Save", Ico("SaveFile"), Ico("SaveFile-small"), 0, False, True)
		  B.IconName = "SaveFile"
		  B.SmallIconName = "SaveFile-small"
		  S.Buttons.Append B
		  B = New RibbonButton("SaveAs", "Save As...", Ico("SaveFileAs"), Ico("SaveFileAs-small"))
		  B.IconName = "SaveFileAs"
		  B.SmallIconName = "SaveFileAs-small"
		  S.Buttons.Append B
		  B = New RibbonButton("Generate", "Generate", Ico("BuildApp"), Ico("BuildApp-small"))
		  B.IconName = "BuildApp"
		  B.SmallIconName = "BuildApp-small"
		  S.Buttons.Append B
		  
		  S = New RibbonSection("Modification", "Modification", 0, False, 1)
		  T.Sections.Append S
		  
		  B = New RibbonButton("Find", "Find & Replace", Ico("FindIcon"), Nil)
		  B.IconName = "FindIcon"
		  S.Buttons.Append B
		  'B = New RibbonButton("Quick Find" , "", Nil, Nil, SearchField.Width + 10)
		  'B.EmbeddedControl = New RibbonEmbeddedControl(SearchField, -1, -1)
		  B.WhiteSpace = True
		  S.Buttons.Append B
		  
		  
		  //MyCustom SectionBar
		  S = New RibbonSection("เครื่องมือ", "เครื่องมือ", 0, True, 2)
		  T.Sections.Append S
		  
		  B = New RibbonButton("Create", "สร้าง", Ico("SaveFile"), Ico("SaveFile-small"), 0, False, True)
		  B.IconName = "SaveFile"
		  B.SmallIconName = "SaveFile-small"
		  S.Buttons.Append B
		  B = New RibbonButton("Edit", "แก้ไข", Ico("SaveFileAs"), Ico("SaveFileAs-small"))
		  B.IconName = "SaveFileAs"
		  B.SmallIconName = "SaveFileAs-small"
		  S.Buttons.Append B
		  B = New RibbonButton("Delete", "ลบ", Ico("BuildApp"), Ico("BuildApp-small"))
		  B.IconName = "BuildApp"
		  B.SmallIconName = "BuildApp-small"
		  S.Buttons.Append B
		  
		  //MyCustom SectionBar2
		  S = New RibbonSection("เครื่องมือของฟอร์ม", "เครื่องมือ", 0, True, 2)
		  T.Sections.Append S
		  
		  B = New RibbonButton("SaveForm", "บันทึก", Ico("SaveFile"), Ico("SaveFile-small"), 0, False, True)
		  B.IconName = "SaveFile"
		  B.SmallIconName = "SaveFile-small"
		  S.Buttons.Append B
		  B = New RibbonButton("CancelForm", "ยกเลิก", Ico("SaveFileAs"), Ico("SaveFileAs-small"))
		  B.IconName = "SaveFileAs"
		  B.SmallIconName = "SaveFileAs-small"
		  S.Buttons.Append B
		  
		  S = New RibbonSection("เครื่องมือของฟอร์ม", "เครื่องมือ", 0, True, 2)
		  T.Sections.Append S
		  B = New RibbonButton("ImportPerson", "นำเข้าข้อมูลบุคคล", Ico("SaveFileAs"), Ico("SaveFileAs-small"))
		  B.IconName = "SaveFileAs"
		  B.SmallIconName = "SaveFileAs-small"
		  S.Buttons.Append B
		  
		  
		  
		  
		  //Tab 2
		  T = New RibbonTab("XML", "&XML")
		  'T.BackColor = &cD5BAE5
		  'T.HasBackColor = True
		  me.Tabs.Append T
		  
		  S = New RibbonSection("Edit", "Edit", 0, False, 1)
		  T.Sections.Append S
		  
		  B = New RibbonButton("Undo", "Undo", Ico("Undo"), Ico("Undo-small"))
		  B.IconName = "Undo"
		  B.SmallIconName = "Undo-small"
		  S.Buttons.Append B
		  B = New RibbonButton("Redo", "Redo", Ico("Redo"), Ico("Redo-small"))
		  B.IconName = "Redo"
		  B.SmallIconName = "Redo-small"
		  S.Buttons.Append B
		  B = New RibbonButton("Comment", "Comment", Nil, Nil, 0, True)
		  S.Buttons.Append B
		  B = New RibbonButton("Indent", "Indent Selection", Ico("Indent-small"), Nil, 0, True)
		  B.IconName = "Indent-small"
		  S.Buttons.Append B
		  B = New RibbonButton("Convert", "Convert to v6 (beta)", Nil, Nil, 0, True)
		  S.Buttons.Append B
		  
		  S = New RibbonSection("View", "View", 0, True, 2)
		  T.Sections.Append S
		  
		  B = New RibbonButton("PicSelect", "Select in picture...", Ico("select"), Nil)
		  B.IconName = "select"
		  S.Buttons.Append B
		  B = New RibbonButton("Bookmarks", "Bookmarks", Ico("Bookmark"), Ico("Bookmark-small"))
		  B.IconName = "Bookmark"
		  B.SmallIconName = "Bookmark-small"
		  S.Buttons.Append B
		  
		  
		  
		  //Tab 3
		  T = New RibbonTab("Help / Options", "Help / &Options")
		  me.Tabs.Append T
		  
		  S = New RibbonSection("Options", "Options", 0, True, 2)
		  T.Sections.Append S
		  
		  B = New RibbonButton("Settings", "Settings", Ico("settings"), Ico("Settings-small"))
		  B.IconName = "settings"
		  B.SmallIconName = "Settings-small"
		  S.Buttons.Append B
		  B = New RibbonButton("Translate", "Translate", Ico("translate"), Ico("Translate-small"))
		  B.IconName = "translate"
		  B.SmallIconName = "Translate-small"
		  S.Buttons.Append B
		  
		  S = New RibbonSection("?", "Help", 0, False, 1)
		  T.Sections.Append S
		  
		  B = New RibbonButton("Help", "User Guide", Ico("LanguageReference"), Ico("LanguageReference-small"), 0, False, False, False)
		  B.IconName = "LanguageReference"
		  B.SmallIconName = "LanguageReference-small"
		  S.Buttons.Append B
		  S.Buttons.Append New RibbonButton() //Separator
		  'B = New RibbonButton("Update", "Check for Update", Nil, Nil, 70)
		  'B.EmbeddedControl = New RibbonEmbeddedControl(testbox, -1, -1)
		  'B.WhiteSpace = True
		  'S.Buttons.Append B
		  B = New RibbonButton("About", "About", Ico("About"), Ico("About-small"))
		  B.IconName = "About"
		  B.SmallIconName = "About-small"
		  S.Buttons.Append B
		  
		  
		  //Tab 4
		  T = New RibbonTab("เมนูประวัติต่างๆ", "menuHistory")
		  me.Tabs.Append T
		  
		  S = New RibbonSection("Options", "เมนูประวัติ", 0, True, 2)
		  T.Sections.Append S
		  
		  B = New RibbonButton("education", "ประวัติการศึกษา", Ico("settings"), Ico("Settings-small"))
		  B.IconName = "translate"
		  B.SmallIconName = "Translate-small"
		  S.Buttons.Append B
		  B = New RibbonButton("positionHistory", "ประวัติการดำรงตำแหน่ง", Ico("translate"), Ico("Translate-small"))
		  B.IconName = "translate"
		  B.SmallIconName = "Translate-small"
		  S.Buttons.Append B
		  B = New RibbonButton("positionHistory", "ประวัติการรับเงินเดือน", Ico("translate"), Ico("Translate-small"))
		  B.IconName = "translate"
		  B.SmallIconName = "Translate-small"
		  S.Buttons.Append B
		  B = New RibbonButton("positionHistory", "ประวัติการลา", Ico("translate"), Ico("Translate-small"))
		  B.IconName = "translate"
		  B.SmallIconName = "Translate-small"
		  S.Buttons.Append B
		  B = New RibbonButton("positionHistory", "ประวัติการครอบครัว", Ico("translate"), Ico("Translate-small"))
		  B.IconName = "translate"
		  B.SmallIconName = "Translate-small"
		  S.Buttons.Append B
		  B = New RibbonButton("positionHistory", "ประวัติการความประพฤติ", Ico("translate"), Ico("Translate-small"))
		  B.IconName = "translate"
		  B.SmallIconName = "Translate-small"
		  S.Buttons.Append B
		  B = New RibbonButton("positionHistory", "ประวัติการอบรม", Ico("translate"), Ico("Translate-small"))
		  B.IconName = "translate"
		  B.SmallIconName = "Translate-small"
		  S.Buttons.Append B
		  B = New RibbonButton("positionHistory", "ประวัติการกองทุน", Ico("translate"), Ico("Translate-small"))
		  B.IconName = "translate"
		  B.SmallIconName = "Translate-small"
		  S.Buttons.Append B
		  
		  S = New RibbonSection("?", "Help", 0, False, 1)
		  T.Sections.Append S
		  
		  B = New RibbonButton("Help", "User Guide", Ico("LanguageReference"), Ico("LanguageReference-small"), 0, False, False, False)
		  B.IconName = "LanguageReference"
		  B.SmallIconName = "LanguageReference-small"
		  S.Buttons.Append B
		  S.Buttons.Append New RibbonButton() //Separator
		  'B = New RibbonButton("Update", "Check for Update", Nil, Nil, 70)
		  'B.EmbeddedControl = New RibbonEmbeddedControl(testbox, -1, -1)
		  'B.WhiteSpace = True
		  'S.Buttons.Append B
		  B = New RibbonButton("About", "About", Ico("About"), Ico("About-small"))
		  B.IconName = "About"
		  B.SmallIconName = "About-small"
		  S.Buttons.Append B
		  'me.SetColor &cBEBEBE
		  
		  
		  'me.SetStyle(me.StyleOffice2007)
		  me.Height = me.BestHeight
		  
		  me.Tabs(0).BackColor = &cFF0000
		  me.Tabs(0).HasBackColor = True
		  me.Tabs(1).BackColor = &c0000FF
		  me.Tabs(1).HasBackColor = True
		  me.Tabs(2).BackColor = &cF2CB1D
		  me.Tabs(2).HasBackColor = True
		  
		  me.SetStyle(me.StyleOffice2013)
		  
		  
		  me.Fullrefresh = True
		  me.Refresh
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action(SectionName As String, ButtonName As String)
		  'MsgBox "Clicked on : " + SectionName + ", " + ButtonName
		  
		  if ButtonName = "Home" Then
		    'PersonnelWindow.Close
		    'PersonnelWindow.Show
		    PersonnelWindow.PagePanel1.Value = 1
		  elseif ButtonName = "Create" Then
		    PersonnelWindow.ctnContent1.lblTitle.Text = "ฟอร์มกรอกข้อมูลประวัติพนักงาน"
		    PersonnelWindow.cFormPersonnel.NewForm
		    PersonnelWindow.ctnListPersonnel1.lstPersonnel.ListIndex = -1
		    PersonnelWindow.PagePanel1.Value = 0
		  elseif ButtonName = "Edit" then
		    PersonnelWindow.PagePanel1.Value = 0
		    PersonnelWindow.cFormPersonnel.SetID(PersonnelWindow.cViewPersonnel.CurrentID)
		  elseif ButtonName = "Save" Then
		    if PersonnelWindow.PagePanel1.Value = 0 Then
		      PersonnelWindow.cFormPersonnel.BindData
		    else
		      MsgBox("กรุณาไปที่หน้ากรอกข้อมูล")
		    end
		  elseif ButtonName = "Delete" Then
		    'MsgBox PersonnelWindow.cViewPersonnel.CurrentID
		    Dim pInstance as  new Repo.ws.Personnel
		    if PersonnelWindow.cViewPersonnel.CurrentID <> "" then
		      MsgBox PersonnelWindow.cViewPersonnel.CurrentID
		      Dim result as Boolean
		      result = pInstance.Delete(PersonnelWindow.cViewPersonnel.CurrentID)
		      if result then
		        MsgBox "ลบข้อมูลสำเร็จ"
		      else
		        MsgBox "กรุณาเลือกข้อมูล"
		      end
		    end
		    PersonnelWindow.ctnListPersonnel1.LoadLists
		    
		  elseif ButtonName = "SaveForm" Then
		    PersonnelWindow.cFormPersonnel.BindData
		    'MsgBox "สร้างใหม่"
		  elseif ButtonName = "CancelForm" Then
		    MsgBox "ยกเลิก"
		    PersonnelWindow.ctnContent1.lblTitle.Text = "ประวัติพนักงาน"
		    PersonnelWindow.PagePanel1.Value = 1
		  elseif ButtonName = "ImportPerson" then
		    PersonnelWindow.cFormPersonnel.NewForm
		    PersonnelWindow.ctnListPersonnel1.lstPersonnel.ListIndex = -1
		    PersonnelWindow.PagePanel1.Value = 0
		    PersonnelWindow.cFormPersonnel.ImportPerson
		  End
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
