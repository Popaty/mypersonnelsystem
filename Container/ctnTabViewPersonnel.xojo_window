#tag Window
Begin ContainerControl ctnTabViewPersonnel
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   True
   Height          =   611
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   True
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   944
   Begin Rectangle Rectangle1
      AutoDeactivate  =   True
      BorderWidth     =   1
      BottomRightColor=   &c7F7F007F
      Enabled         =   True
      FillColor       =   &cFFFF00FF
      Height          =   611
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   2
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      TopLeftColor    =   &c7F7F007F
      Visible         =   True
      Width           =   944
      Begin TabPanel TabPanel1
         AutoDeactivate  =   True
         Bold            =   False
         Enabled         =   True
         Height          =   571
         HelpTag         =   ""
         Index           =   0
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   20
         LockBottom      =   True
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         Panels          =   ""
         Scope           =   2
         SmallTabs       =   True
         TabDefinition   =   "ข้อมูลทั่วไป\rลดหย่อนภาษี\rเหตุการณ์\rเงินได้ - เงินหัก\rวันลา\rตำแหน่ง\rประวัติการรับเงิน\rกองทุน\rประกันสังคม\rวินัย\rเครื่องราชอิสริยาภรณ์\rเอกสาร"
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   20
         Underline       =   False
         Value           =   5
         Visible         =   True
         Width           =   904
         Begin ctnListHistoryProfile ctnListEvent
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFFFF00FF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   3
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnListHistoryProfile ctnListFund
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFFFF00FF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   8
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnListHistoryProfile ctnListLeave
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFFFF00FF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   5
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnListHistoryProfile ctnListHistoryPayment
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFFFF00FF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   7
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnListHistoryProfile ctnListHistoryPosition
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFFFF00FF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   6
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnListHistoryProfile ctnListInsignia
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFFFF00FF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   11
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnListHistoryProfile ctnListIncome
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFFFF00FF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   275
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   5
            TabPanelIndex   =   4
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnListHistoryProfile ctnListDeduct
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFFFF00FF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   275
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   6
            TabPanelIndex   =   4
            TabStop         =   True
            Top             =   316
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnUploadFile ctnListUploadFile
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFFFF00FF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   12
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         BeginSegmented SegmentedControl SegmentedControl1
            Enabled         =   True
            Height          =   24
            Index           =   -2147483648
            InitialParent   =   "TabPanel1$0"
            Left            =   50
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            MacControlStyle =   3
            Scope           =   0
            Segments        =   "ประวัติ\n\nTrue\rอื่นๆ\n\nFalse\rการศึกษา\n\nFalse\rผู้ที่เกี่ยวข้อง\n\nFalse\rประสบการณ์ทำงาน\n\nFalse\rทักษะ\n\nFalse\rเปลี่ยนชื่อ\n\nFalse"
            SelectionType   =   0
            TabPanelIndex   =   1
            Top             =   58
            Visible         =   True
            Width           =   844
         End
         Begin ctnListHistoryProfile ctnListDiscipline
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFFFF00FF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   10
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnFormTaxReduce ctnTaxReduceForm
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFFFF00FF
            Backdrop        =   0
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   2
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnViewDetails ctnViewPersonnelDetails
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFFFF00FF
            Backdrop        =   0
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   497
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "TabPanel1$0"
            Left            =   1000
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            resignate       =   False
            Scope           =   0
            TabIndex        =   1
            TabPanelIndex   =   1
            TabStop         =   True
            Top             =   20
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
      End
      Begin ctnViewDetailsOther ctnViewPersonnelDetailsOther
         AcceptFocus     =   False
         AcceptTabs      =   True
         AutoDeactivate  =   True
         BackColor       =   &cFFFF00FF
         Backdrop        =   0
         Enabled         =   True
         EraseBackground =   True
         HasBackColor    =   False
         Height          =   497
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Left            =   1000
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   0
         TabStop         =   True
         Top             =   40
         Transparent     =   True
         UseFocusRing    =   False
         Visible         =   True
         Width           =   904
      End
   End
   Begin ctnListHistoryProfile ctnListRelative
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      CategoryName    =   ""
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   497
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   1038
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   43
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   904
   End
   Begin ctnListHistoryProfile ctnListEducation
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      CategoryName    =   ""
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   497
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   1058
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   904
   End
   Begin ctnListHistoryProfile ctnListExperience
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      CategoryName    =   ""
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   497
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   1078
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   20
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   904
   End
   Begin ctnListHistoryProfile ctnListNameChange
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      CategoryName    =   ""
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   497
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   1098
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   40
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   904
   End
   Begin ctnListHistoryProfile ctnListSkill
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFFFF00FF
      Backdrop        =   0
      CategoryName    =   ""
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   497
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   1118
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   60
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   904
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  Self.ctnListEducation.SetEducation
		  Self.ctnListEvent.SetEvent
		  Self.ctnListExperience.SetExperience
		  Self.ctnListFund.SetFund
		  Self.ctnListHistoryPayment.SetHistoryPayment
		  Self.ctnListHistoryPosition.SetHistoryPosition
		  Self.ctnListInsignia.SetInsignia
		  Self.ctnListLeave.SetLeave
		  Self.ctnListNameChange.SetNameChange
		  Self.ctnListRelative.SetRelative
		  Self.ctnListSkill.SetSkill
		  Self.ctnListIncome.SetIncome
		  Self.ctnListDeduct.SetDeduct
		  Self.ctnListDiscipline.SetDiscipline
		  Self.ctnViewPersonnelDetailsOther.ctnListBank.SetBankAccount
		  Self.ctnViewPersonnelDetailsOther.ctnListAddress.SetAddress
		  
		  Self.ctnListIncome.lblTopic.Text = "เงินได้"
		  Self.ctnListDeduct.lblTopic.Text = "เงินหัก"
		  Self.ctnViewPersonnelDetailsOther.ctnListBank.lblTopic.Text = "บัญชีธนาคาร"
		  Self.ctnViewPersonnelDetailsOther.ctnListAddress.lblTopic.Text = "ที่อยู่"
		  
		  Self.ctnListIncome.lstHistoryProfile.Height = 200
		  Self.ctnListDeduct.lstHistoryProfile.Height = 200
		  Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Height = 70
		  Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.Height = 70
		  
		  
		  Self.ctnListEducation.CategoryName = "education"
		  Self.ctnListEvent.CategoryName = "event"
		  Self.ctnListExperience.CategoryName = "experience"
		  Self.ctnListFund.CategoryName = "fund"
		  Self.ctnListHistoryPayment.CategoryName = "history-payment"
		  Self.ctnListHistoryPosition.CategoryName = "history-position"
		  Self.ctnListInsignia.CategoryName = "insignia"
		  Self.ctnListLeave.CategoryName = "leave"
		  Self.ctnListNameChange.CategoryName = "name-change"
		  Self.ctnListRelative.CategoryName = "relative"
		  Self.ctnListSkill.CategoryName = "skill"
		  Self.ctnListIncome.CategoryName = "income"
		  Self.ctnListDeduct.CategoryName = "deduct"
		  Self.ctnListDiscipline.CategoryName = "discipline"
		  Self.ctnViewPersonnelDetailsOther.ctnListAddress.CategoryName = "address"
		  Self.ctnViewPersonnelDetailsOther.ctnListBank.CategoryName = "bank"
		  
		  Self.ctnListHistoryPayment.SetDisableButton
		  Self.ctnListHistoryPosition.SetDisableButton
		  Self.ctnListInsignia.SetDisableButton
		  Self.ctnListEvent.SetDisableButton
		  Self.ctnListIncome.SetDisableButton
		  Self.ctnListDeduct.SetDisableButton
		  Self.ctnListLeave.SetDisableButton
		  Self.ctnListHistoryPosition.SetDisableButton
		  Self.ctnListHistoryPayment.SetDisableButton
		  Self.ctnListFund.SetDisableButton
		  Self.ctnListDiscipline.SetDisableButton
		  Self.ctnListInsignia.SetDisableButton
		  Self.ctnListNameChange.SetDisableButton
		  Self.ctnListEducation.SetDisableButton
		  Self.ctnListRelative.SetDisableButton
		  Self.ctnListExperience.SetDisableButton
		  Self.ctnListSkill.SetDisableButton
		  
		  Self.ctnListEvent.SetInvisibleButton
		  Self.ctnListIncome.SetInvisibleButton
		  Self.ctnListDeduct.SetInvisibleButton
		  Self.ctnListLeave.SetInvisibleButton
		  Self.ctnListHistoryPosition.SetInvisibleButton
		  Self.ctnListFund.SetInvisibleButton
		  Self.ctnListDiscipline.SetInvisibleButton
		  Self.ctnListInsignia.SetInvisibleButton
		  Self.ctnListHistoryPayment.SetInvisibleButton
		  
		  Self.OpenFirstTab
		  
		  //Set Combo Box
		  Self.SetupComponents
		  
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub ActionSegment()
		  // count down so we avoid re-evaluating the ubound all the time
		  For i As Integer = SegmentedControl1.Items.Ubound DownTo 0
		    
		    // get the reference to the segment
		    Dim s As SegmentedControlItem = SegmentedControl1.Items( i )
		    
		    //see if the segment was selected
		    If s.Selected Then
		      select case s.Title 
		      case "ประวัติ"
		        self.HideComponent
		        Self.ctnViewPersonnelDetails.Left = 20
		        Self.ctnViewPersonnelDetails.Top = 94
		        Self.ctnViewPersonnelDetails.Visible = True
		      case "การศึกษา"
		        self.HideComponent
		        Self.ctnListEducation.Left = 20
		        Self.ctnListEducation.Top = 94
		        self.ctnListEducation.Visible = True
		      case "ผู้ที่เกี่ยวข้อง"
		        self.HideComponent
		        Self.ctnListRelative.Left = 20
		        Self.ctnListRelative.Top = 94
		        self.ctnListRelative.Visible = True
		      case "ประสบการณ์ทำงาน"
		        self.HideComponent
		        Self.ctnListExperience.Left = 20
		        Self.ctnListExperience.Top = 94
		        self.ctnListExperience.Visible = True
		      case "เปลี่ยนชื่อ"
		        self.HideComponent
		        Self.ctnListNameChange.Left = 20
		        Self.ctnListNameChange.Top = 94
		        self.ctnListNameChange.Visible = True
		      case "ทักษะ"
		        self.HideComponent
		        Self.ctnListSkill.Left = 20
		        Self.ctnListSkill.Top = 94
		        self.ctnListSkill.Visible = True
		      case "อื่นๆ"
		        self.HideComponent
		        Self.ctnViewPersonnelDetailsOther.Left = 20
		        Self.ctnViewPersonnelDetailsOther.Top = 94
		        self.ctnViewPersonnelDetailsOther.Visible = True
		      end
		    End If
		  Next
		  
		  // make sure the segmented control knows to resizes its drawing boundaries or you can get weird effects
		  SegmentedControl1.SizeToFit
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub BindData()
		  Dim data as new JSONItem
		  data = me.GetValue
		  'MsgBox data.ToString
		  me.SendData(data)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CleanAllRow()
		  Self.ctnListDeduct.lstHistoryProfile.DeleteAllRows
		  Self.ctnListDiscipline.lstHistoryProfile.DeleteAllRows
		  Self.ctnListEducation.lstHistoryProfile.DeleteAllRows
		  Self.ctnListEvent.lstHistoryProfile.DeleteAllRows
		  Self.ctnListExperience.lstHistoryProfile.DeleteAllRows
		  Self.ctnListFund.lstHistoryProfile.DeleteAllRows
		  Self.ctnListHistoryPayment.lstHistoryProfile.DeleteAllRows
		  Self.ctnListHistoryPosition.lstHistoryProfile.DeleteAllRows
		  Self.ctnListIncome.lstHistoryProfile.DeleteAllRows
		  Self.ctnListLeave.lstHistoryProfile.DeleteAllRows
		  Self.ctnListNameChange.lstHistoryProfile.DeleteAllRows
		  Self.ctnListRelative.lstHistoryProfile.DeleteAllRows
		  Self.ctnListSkill.lstHistoryProfile.DeleteAllRows
		  Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.DeleteAllRows
		  Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.DeleteAllRows
		  'Self.ctnListUploadFile.lstHistoryProfile.DeleteAllRows
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetValue() As JSONItem
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub HideComponent()
		  Self.ctnViewPersonnelDetails.Visible = False
		  Self.ctnListEducation.Visible = False
		  Self.ctnListRelative.Visible = False
		  Self.ctnListExperience.Visible = False
		  Self.ctnListNameChange.Visible = False
		  Self.ctnListSkill.Visible = False
		  self.ctnViewPersonnelDetailsOther.Visible = False
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub OpenFirstTab()
		  Self.ctnViewPersonnelDetails.Left = 20
		  Self.ctnViewPersonnelDetails.Top = 94
		  Self.ctnViewPersonnelDetails.Visible = True
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SendData(formData as JSONItem)
		  'MsgBox formData.ToString
		  Dim pInstance As New Repo.WS.Personnel
		  If formData.Value("id") = "" Then
		    If pInstance.Save(formData) Then
		      MsgBox "เพิ่มข้อมูลสำเร็จ"
		    Else
		      MsgBox "เพิ่มข้อมูลไม่สำเร็จ"
		    End
		  Else
		    If pInstance.Update(formData) Then
		      MsgBox "แก้ไขข้อมูลสำเร็จ"
		    Else
		      MsgBox "แก้ไขข้อมูลไม่สำเร็จ"
		    End
		  End
		  
		  PersonnelWindow.ctnListPersonnel1.LoadLists
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetActingPosition()
		  'Dim pos As New Repo.WS.Position
		  'Dim js As String
		  'Dim jsData As New JSONItem
		  'Try
		  'js = pos.ListAll
		  'jsData = New JSONItem(js)
		  'If jsData.Value("status") = True Then
		  'Dim objData As JSONItem = jsData.Value("data")
		  'Me.ctnViewPersonnelDetails.cboActing.AddRow("กรุณาเลือกข้อมูล")
		  'Me.ctnViewPersonnelDetails.cboActing.RowTag(0) = ""
		  'For i As Integer = 0 To objData.Count-1
		  'Dim iItem As JSONItem = objData.Child(i)
		  'Me.ctnViewPersonnelDetails.cboActing.AddRow(iItem.Value("positionNameTh"))
		  'Me.ctnViewPersonnelDetails.cboActing.RowTag(i+1) = iItem.Value("id")
		  'Next
		  'End
		  'Catch err As KeyNotFoundException
		  'End
		  '
		  'Me.ctnViewPersonnelDetails.cboActing.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetBloodGroup()
		  'Dim bg As New Repo.WS.BloodGroups
		  'Dim js As String
		  'Dim jsData As New JSONItem
		  'Try
		  'js = bg.ListAll
		  'jsData = New JSONItem(js)
		  'If jsData.Value("status") = True Then
		  'Dim objData As JSONItem = jsData.Value("data")
		  'Me.ctnViewPersonnelDetailsOther.cboBloodGroup.AddRow("กรุณาเลือกข้อมูล")
		  'Me.ctnViewPersonnelDetailsOther.RowTag(0) = ""
		  'For i As Integer = 0 To objData.Count-1
		  'Dim iItem As JSONItem = objData.Child(i)
		  'Me.ctnViewPersonnelDetailsOther.cboBloodGroup.AddRow(iItem.Value("name"))
		  'Me.ctnViewPersonnelDetailsOther.cboBloodGroup.RowTag(i+1) = iItem.Value("id")
		  'Next
		  'End
		  'Catch err As KeyNotFoundException
		  'End
		  '
		  'Me.ctnViewPersonnelDetailsOther.cboBloodGroup.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetCitizenShip()
		  'Dim cs As New Repo.WS.CitizenShip
		  'Dim js As String
		  'Dim jsData As New JSONItem
		  'Try
		  'js = cs.ListAll
		  'jsData = New JSONItem(js)
		  'If jsData.Value("status") = True Then
		  'Dim objData As JSONItem = jsData.Value("data")
		  'Me.ctnViewPersonnelDetailsOther.cboCitizenShip.AddRow("กรุณาเลือกข้อมูล")
		  'Me.ctnViewPersonnelDetailsOther.cboCitizenShip.RowTag(0) = ""
		  'For i As Integer = 0 To objData.Count-1
		  'Dim iItem As JSONItem = objData.Child(i)
		  'Me.ctnViewPersonnelDetailsOther.cboCitizenShip.AddRow(iItem.Value("name"))
		  'Me.ctnViewPersonnelDetailsOther.cboCitizenShip.RowTag(i+1) = iItem.Value("id")
		  'Next
		  'End
		  'Catch err As KeyNotFoundException
		  'End
		  '
		  'Me.ctnViewPersonnelDetailsOther.cboCitizenShip.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetCurrentPosition()
		  'Dim pos As New Repo.WS.Position
		  'Dim js As String
		  'Dim jsData As New JSONItem
		  'Try
		  'js = pos.ListAll
		  'jsData = New JSONItem(js)
		  'If jsData.Value("status") = True Then
		  'Dim objData As JSONItem = jsData.Value("data")
		  'Me.ctnViewPersonnelDetails.cboCurrentPosition.AddRow("กรุณาเลือกข้อมูล")
		  'Me.ctnViewPersonnelDetails.cboCurrentPosition.RowTag(0) = ""
		  'For i As Integer = 0 To objData.Count-1
		  'Dim iItem As JSONItem = objData.Child(i)
		  'Me.ctnViewPersonnelDetails.cboCurrentPosition.AddRow(iItem.Value("positionNameTh"))
		  'Me.ctnViewPersonnelDetails.cboCurrentPosition.RowTag(i+1) = iItem.Value("id")
		  'Next
		  'End
		  'Catch err As KeyNotFoundException
		  'End
		  '
		  'Me.ctnViewPersonnelDetails.cboCurrentPosition.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetEyeColor()
		  'Dim eye As New Repo.WS.EyeColor
		  'Dim js As String
		  'Dim jsData As New JSONItem
		  'Try
		  'js = eye.ListAll
		  'jsData = New JSONItem(js)
		  'If jsData.Value("status") = True Then
		  'Dim objData As JSONItem = jsData.Value("data")
		  'Me.cboCurrentPosition.AddRow("กรุณาเลือกข้อมูล")
		  'Me.cboCurrentPosition.RowTag(0) = ""
		  'For i As Integer = 0 To objData.Count-1
		  'Dim iItem As JSONItem = objData.Child(i)
		  'Me.cboCurrentPosition.AddRow(iItem.Value("positionNameTh"))
		  'Me.cboCurrentPosition.RowTag(i+1) = iItem.Value("id")
		  'Next
		  'End
		  'Catch err As KeyNotFoundException
		  'End
		  '
		  'Me.cboCurrentPosition.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetID(id as String)
		  'Dim pInstance As New Repo.WS.Personnel
		  Self.CurrentID = id
		  Self.SetValue
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetMaritalStatus()
		  Dim ms As New Repo.WS.MaritalStatus
		  Dim js As String
		  Dim jsData As New JSONItem
		  Try
		    js = ms.ListAll
		    jsData = New JSONItem(js)
		    If jsData.Value("status") = True Then
		      Dim objData As JSONItem = jsData.Value("data")
		      Me.ctnViewPersonnelDetails.cboMarital.AddRow("กรุณาเลือกข้อมูล")
		      Me.ctnViewPersonnelDetails.cboMarital.RowTag(0) = ""
		      For i As Integer = 0 To objData.Count-1
		        Dim iItem As JSONItem = objData.Child(i)
		        Me.ctnViewPersonnelDetails.cboMarital.AddRow(iItem.Value("name"))
		        Me.ctnViewPersonnelDetails.cboMarital.RowTag(i+1) = iItem.Value("id")
		      Next
		    End
		  Catch err As KeyNotFoundException
		  End
		  
		  Me.ctnViewPersonnelDetails.cboMarital.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetMilitary()
		  'Dim pos As New Repo.WS.Positions
		  'Dim js As String
		  'Dim jsData As New JSONItem
		  'Try
		  'js = pos.ListAll
		  'jsData = New JSONItem(js)
		  'If jsData.Value("status") = True Then
		  'Dim objData As JSONItem = jsData.Value("data")
		  'Me.cboCurrentPosition.AddRow("กรุณาเลือกข้อมูล")
		  'Me.cboCurrentPosition.RowTag(0) = ""
		  'For i As Integer = 0 To objData.Count-1
		  'Dim iItem As JSONItem = objData.Child(i)
		  'Me.cboCurrentPosition.AddRow(iItem.Value("positionNameTh"))
		  'Me.cboCurrentPosition.RowTag(i+1) = iItem.Value("id")
		  'Next
		  'End
		  'Catch err As KeyNotFoundException
		  'End
		  '
		  'Me.cboCurrentPosition.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetNameTitle()
		  'Dim nti As New Repo.WS.NameTitle
		  'Dim js As String
		  'Dim jsData As New JSONItem
		  'Try
		  'js = nti.ListAll
		  'jsData = New JSONItem(js)
		  'If jsData.Value("status") = True Then
		  'Dim objData As JSONItem = jsData.Value("data")
		  'Me.ctnViewPersonnelDetails.cboNameTitle.AddRow("กรุณาเลือกข้อมูล")
		  'Me.ctnViewPersonnelDetails.cboNameTitle.RowTag(0) = ""
		  'For i As Integer = 0 To objData.Count-1
		  'Dim iItem As JSONItem = objData.Child(i)
		  'Me.ctnViewPersonnelDetails.cboNameTitle.AddRow(iItem.Value("nameTh"))
		  'Me.ctnViewPersonnelDetails.cboNameTitle.RowTag(i+1) = iItem.Value("id")
		  'Next
		  'End
		  'Catch err As KeyNotFoundException
		  'End
		  '
		  'Me.ctnViewPersonnelDetails.cboNameTitle.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetNationality()
		  'Dim nt As New Repo.WS.Nationality
		  'Dim js As String
		  'Dim jsData As New JSONItem
		  'Try
		  'js = nt.ListAll
		  'jsData = New JSONItem(js)
		  'If jsData.Value("status") = True Then
		  'Dim objData As JSONItem = jsData.Value("data")
		  'Me.ctnViewPersonnelDetails.cboNationality.AddRow("กรุณาเลือกข้อมูล")
		  'Me.ctnViewPersonnelDetails.cboNationality.RowTag(0) = ""
		  'For i As Integer = 0 To objData.Count-1
		  'Dim iItem As JSONItem = objData.Child(i)
		  'Me.ctnViewPersonnelDetails.cboNationality.AddRow(iItem.Value("name"))
		  'Me.ctnViewPersonnelDetails.cboNationality.RowTag(i+1) = iItem.Value("id")
		  'Next
		  'End
		  'Catch err As KeyNotFoundException
		  'End
		  '
		  'Me.ctnViewPersonnelDetails.cboNationality.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetPersonnelType()
		  Dim pt As New Repo.WS.PersonnelType
		  Dim js As String
		  Dim jsData As New JSONItem
		  Try
		    js = pt.ListAll
		    jsData = New JSONItem(js)
		    If jsData.Value("status") = True Then
		      Dim objData As JSONItem = jsData.Value("data")
		      Me.ctnViewPersonnelDetails.cboTypeEmp.AddRow("กรุณาเลือกข้อมูล")
		      Me.ctnViewPersonnelDetails.cboTypeEmp.RowTag(0) = ""
		      For i As Integer = 0 To objData.Count-1
		        Dim iItem As JSONItem = objData.Child(i)
		        Me.ctnViewPersonnelDetails.cboTypeEmp.AddRow(iItem.Value("name"))
		        Me.ctnViewPersonnelDetails.cboTypeEmp.RowTag(i+1) = iItem.Value("id")
		      Next
		    End
		  Catch err As KeyNotFoundException
		  End
		  
		  Me.ctnViewPersonnelDetails.cboTypeEmp.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetReligion()
		  'Dim rg As New Repo.WS.Religion
		  'Dim js As String
		  'Dim jsData As New JSONItem
		  'Try
		  'js = rg.ListAll
		  'jsData = New JSONItem(js)
		  'If jsData.Value("status") = True Then
		  'Dim objData As JSONItem = jsData.Value("data")
		  'Me.ctnViewPersonnelDetailsOther.cboReligion.AddRow("กรุณาเลือกข้อมูล")
		  'Me.ctnViewPersonnelDetailsOther.cboReligion.RowTag(0) = ""
		  'For i As Integer = 0 To objData.Count-1
		  'Dim iItem As JSONItem = objData.Child(i)
		  'Me.ctnViewPersonnelDetailsOther.cboReligion.AddRow(iItem.Value("name"))
		  'Me.ctnViewPersonnelDetailsOther.cboReligion.RowTag(i+1) = iItem.Value("id")
		  'Next
		  'End
		  'Catch err As KeyNotFoundException
		  'End
		  '
		  'Me.ctnViewPersonnelDetailsOther.cboReligion.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetSalaryType()
		  'Dim st As New Repo.WS.SalaryType
		  'Dim js As String
		  'Dim jsData As New JSONItem
		  'Try
		  'js = st.ListAll
		  'jsData = New JSONItem(js)
		  'If jsData.Value("status") = True Then
		  'Dim objData As JSONItem = jsData.Value("data")
		  'Me.ctnViewPersonnelDetails.cboSalaryType.AddRow("กรุณาเลือกข้อมูล")
		  'Me.ctnViewPersonnelDetails.cboSalaryType.RowTag(0) = ""
		  'For i As Integer = 0 To objData.Count-1
		  'Dim iItem As JSONItem = objData.Child(i)
		  'Me.ctnViewPersonnelDetails.cboSalaryType.AddRow(iItem.Value("name"))
		  'Me.ctnViewPersonnelDetails.cboSalaryType.RowTag(i+1) = iItem.Value("id")
		  'Next
		  'End
		  'Catch err As KeyNotFoundException
		  'End
		  '
		  'Me.ctnViewPersonnelDetails.cboSalaryType.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetTaxType()
		  'Dim tt As New Repo.WS.TaxType
		  'Dim js As String
		  'Dim jsData As New JSONItem
		  'Try
		  'js = tt.ListAll
		  'jsData = New JSONItem(js)
		  'If jsData.Value("status") = True Then
		  'Dim objData As JSONItem = jsData.Value("data")
		  'Me.ctnViewPersonnelDetailsOther.cboTaxType.AddRow("กรุณาเลือกข้อมูล")
		  'Me.ctnViewPersonnelDetailsOther.cboTaxType.RowTag(0) = ""
		  'For i As Integer = 0 To objData.Count-1
		  'Dim iItem As JSONItem = objData.Child(i)
		  'Me.ctnViewPersonnelDetailsOther.cboTaxType.AddRow(iItem.Value("name"))
		  'Me.ctnViewPersonnelDetailsOther.cboTaxType.RowTag(i+1) = iItem.Value("id")
		  'Next
		  'End
		  'Catch err As KeyNotFoundException
		  'End
		  '
		  'Me.ctnViewPersonnelDetailsOther.cboTaxType.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetupComponents()
		  self.SetCurrentPosition
		  'self.SetActingPosition
		  self.SetBloodGroup
		  self.SetCitizenShip
		  self.SetMilitary
		  self.SetNationality
		  self.SetReligion
		  self.SetMaritalStatus
		  self.SetNameTitle
		  self.SetSalaryType
		  self.SetTaxType
		  self.SetPersonnelType
		  'self.SetEyeColor
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetValue()
		  Dim pInstance As New Repo.WS.Personnel
		  Dim jsPerson As New JSONItem
		  
		  jsPerson = pInstance.GetByID(Self.CurrentID)
		  Self.CleanAllRow
		  
		  If jsPerson.Value("nameTitleName") <> "" Then
		    PersonnelWindow.ctnContent1.lblTitle.Text = "ประวัติพนักงาน : "+jsPerson.Value("nameTitleName")+" "+jsPerson.Value("firstNameTh")+" "+jsPerson.Value("lastNameTh")
		  Else
		    PersonnelWindow.ctnContent1.lblTitle.Text = "ประวัติพนักงาน : คุณ "+jsPerson.Value("firstNameTh")+" "+jsPerson.Value("lastNameTh")
		  End
		  
		  //CurrentPosition
		  If jsPerson.HasName("currentPosition") Then
		    If jsPerson.Value("currentPosition") <> Nil Then 
		      Dim jsPositionItem As New JSONItem
		      jsPositionItem = jsPerson.Value("currentPosition")
		      If jsPositionItem.Count > 1 Then
		        MsgBox "ตำแหน่งมากกว่า 1"
		      Else
		        For i2 As Integer = 0 To jsPositionItem.Count-1
		          Me.ctnViewPersonnelDetails.dataMainPosition.Text = jsPositionItem.Child(i2).Value("positionName")
		        Next
		      End
		    Else
		    End
		  End
		  
		  //ActingPosition
		  'if jsPerson.HasName("actingPosition") then
		  'if jsPerson.Value("actingPosition") <> Nil then 
		  'Dim jsPositionItem as new JSONItem
		  'jsPositionItem = jsPerson.Value("actingPosition")
		  'if jsPositionItem.Count > 1 then
		  'MsgBox "ตำแหน่งมากกว่า 1"
		  'else
		  'for i2 as integer = 0 to jsPositionItem.Count-1
		  'Me.ctnViewPersonnelDetails.dataMainPosition.text = jsPositionItem.Child(i2).Value("positionName")
		  'Next
		  'end
		  'else
		  'end
		  'end
		  
		  
		  'Me.ctnViewPersonnelDetails.dataMainPosition.Text 
		  'Me.ctnViewPersonnelDetails.dataActingPosition.Text
		  Me.ctnViewPersonnelDetails.dataEmployeeCode.Text = jsPerson.Value("code")
		  Me.ctnViewPersonnelDetails.dataFirstNameTH.Text = jsPerson.Value("firstNameTh")
		  Me.ctnViewPersonnelDetails.dataLastNameTH.Text = jsPerson.Value("lastNameTh")
		  Me.ctnViewPersonnelDetails.dataFirstNameENG.Text = jsPerson.Value("firstNameEng")
		  Me.ctnViewPersonnelDetails.dataLastNameENG.Text = jsPerson.Value("lastNameEng")
		  Me.ctnViewPersonnelDetails.dataNickNameTH.Text = jsPerson.Value("nickNameTh")
		  Me.ctnViewPersonnelDetails.dataNickNamENG.Text = jsPerson.Value("nickNameEng")
		  Me.ctnViewPersonnelDetails.dataStartWorkingDate.Text = jsPerson.Value("startWorkingDate")
		  Me.ctnViewPersonnelDetails.dataStartDate.Text = jsPerson.Value("assignWorkingDate")
		  Me.ctnViewPersonnelDetails.dataIDCard.Text = jsPerson.Value("identifyNo")
		  Me.ctnViewPersonnelDetails.dataIssueDate.Text = jsPerson.Value("dateIssue")
		  Me.ctnViewPersonnelDetails.dataExpireDate.Text = jsPerson.Value("dateExpire")
		  Me.ctnViewPersonnelDetails.dataResignateDate.Text = jsPerson.Value("resignateDate")
		  Me.ctnViewPersonnelDetails.dataResignateReason.Text = jsPerson.Value("resignateReason")
		  Me.ctnViewPersonnelDetails.dataNumberTraining.Text = jsPerson.Value("numberTrainingDay")
		  Me.ctnViewPersonnelDetails.dataDuedateProbation.Text = jsPerson.Value("trainingDueDate")
		  Me.ctnViewPersonnelDetails.dataSalaryType.Text = jsPerson.Value("salaryTypeName")
		  'Me.ctnViewPersonnelDetails.dataSalary.Text = jsPerson.Value("id")
		  Me.ctnViewPersonnelDetails.dataTitleTh.Text = jsPerson.Value("nameTitleName")
		  'Me.ctnViewPersonnelDetails.dataTitleEng.Text = jsPerson.Value("nameTitleNameEng")
		  
		  Me.ctnViewPersonnelDetails.chkProbationStatus.Value = jsPerson.Value("id")
		  Me.ctnViewPersonnelDetails.chkResignateStatus.Value = jsPerson.Value("resignate")
		  if jsPerson.HasName("imageProfile") then
		    if jsPerson.Value("imageProfile").StringValue <> "" then
		      Me.ctnViewPersonnelDetails.ImageProfile.Image = Utils.ImgTools.StringToPicture(jsPerson.Value("imageProfile").StringValue)
		    end
		  end
		  Me.ctnViewPersonnelDetails.dataTypeEmp.Text = jsPerson.Value("personnelTypeName")
		  Me.ctnViewPersonnelDetails.dataGender.Text = jsPerson.Value("gender")
		  Me.ctnViewPersonnelDetails.dataMarital.Text = jsPerson.Value("maritalName")
		  
		  //////////////////////OtherFormDetails//////////////////////
		  'Me.ctnViewPersonnelDetailsOther.cboNationality.ListIndex
		  'Me.ctnViewPersonnelDetailsOther.cboCitizenShip.ListIndex
		  'Me.ctnViewPersonnelDetailsOther.cboEyeColor.ListIndex
		  'Me.ctnViewPersonnelDetailsOther.cboBloodGroup.ListIndex
		  'Me.ctnViewPersonnelDetailsOther.cboReligion.ListIndex
		  'Me.ctnViewPersonnelDetailsOther.cboMilitary.ListIndex
		  Me.ctnViewPersonnelDetailsOther.dataWeight.Text = Str(jsPerson.Value("weight").IntegerValue)
		  Me.ctnViewPersonnelDetailsOther.dataHeight.Text = Str(jsPerson.Value("height").IntegerValue)
		  Me.ctnViewPersonnelDetailsOther.dataScar.Text = jsPerson.Value("scar")
		  Me.ctnViewPersonnelDetailsOther.dataNationality.Text = jsPerson.Value("nationalityName")
		  Me.ctnViewPersonnelDetailsOther.dataCitizen.Text = jsPerson.Value("citizenshipName")
		  
		  If jsPerson.HasName("eyesColor") Then
		    Dim jsEyeTmp As New JSONItem
		    jsEyeTmp = jsPerson.Value("eyesColor")
		    If jsEyeTmp.ToString <> "" Then
		      Me.ctnViewPersonnelDetailsOther.dataEyeColor.Text = jsEyeTmp.Value("name")
		    End
		  End
		  
		  Me.ctnViewPersonnelDetailsOther.dataReligion.Text = jsPerson.Value("religionName") 
		  Me.ctnViewPersonnelDetailsOther.dataBloodGroup.Text = jsPerson.Value("bloodGroupName")
		  
		  Dim c As Control
		  'Dim result As String
		  For i As Integer = 0 To Self.ctnViewPersonnelDetails.ControlCount-1
		    If Self.ctnViewPersonnelDetails.Control(i) IsA Label Then
		      c = Self.ctnViewPersonnelDetails.Control(i)
		      If Label(c).Text = "" Then
		        Label(c).Text = "-"
		      End
		    End If
		  Next
		  
		  If jsPerson.HasName("addresses") Then
		    Dim jsAddress As New JSONItem
		    jsAddress = jsPerson.Value("addresses")
		    If jsAddress.ToString <> "" Then
		      Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.DeleteAllRows
		      For i As Integer = 0 To jsAddress.Count - 1
		        Dim jsAddItem As New JSONItem
		        Dim js As String
		        If jsAddress.Child(i).HasName("currentAddress") And jsAddress.Child(i).Value("currentAddress") <> Nil Then
		          js = jsAddress.Child(i).Value("currentAddress")
		          jsAddItem = New JSONItem(js)
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.AddRow
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,0) = Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.ListCount.ToText
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.CellType(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,1) = jsAddItem.Value("addressTypeName")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,2) = jsAddItem.Value("houseNo")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,3) = jsAddItem.Value("provinceName")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,4) = jsAddItem.Value("districtName")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,5) = jsAddItem.Value("subdistrictName")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,6) = jsAddItem.Value("zipcodeName")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,7) = jsAddItem.Value("homePhone")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,8) = jsAddItem.Value("mobilePhone")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,9) = jsAddItem.Value("email")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,10) = jsAddItem.Value("addressDetailsName")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,11) = jsAddItem.Value("addressDetails1Name")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,12) = jsAddItem.Value("remark")
		        End
		        If jsAddress.Child(i).HasName("registrationAddress") And jsAddress.Child(i).Value("registrationAddress") <> Nil  Then
		          js = jsAddress.Child(i).Value("registrationAddress")
		          jsAddItem = New JSONItem(js)
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.AddRow
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,0) = Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.ListCount.ToText
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.CellType(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,1) = jsAddItem.Value("addressTypeName")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,2) = jsAddItem.Value("houseNo")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,3) = jsAddItem.Value("provinceName")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,4) = jsAddItem.Value("districtName")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,5) = jsAddItem.Value("subdistrictName")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,6) = jsAddItem.Value("zipcodeName")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,7) = jsAddItem.Value("homePhone")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,8) = jsAddItem.Value("mobilePhone")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,9) = jsAddItem.Value("email")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,10) = jsAddItem.Value("addressDetailsName")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,11) = jsAddItem.Value("addressDetails1Name")
		          Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListAddress.lstHistoryProfile.LastIndex,12) = jsAddItem.Value("remark")
		        End
		      Next
		    End
		  End
		  
		  If jsPerson.HasName("banks") Then
		    Dim jsBank As New JSONItem
		    jsBank = jsPerson.Value("banks")
		    If jsBank.ToString <> "" Then
		      Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.DeleteAllRows
		      for i as integer = 0 to jsBank.Count-1
		        Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.AddRow
		        Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.LastIndex,0) = Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.ListCount.ToText
		        Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.CellType(Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		        Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.LastIndex,1) = jsBank.Child(i).Value("bankName")
		        Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.LastIndex,2) = jsBank.Child(i).Value("bankBranchNo")
		        Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.LastIndex,3) = jsBank.Child(i).Value("accountNo")
		        Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.LastIndex,4) = jsBank.Child(i).Value("accountName")
		        'Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.Cell(Self.ctnViewPersonnelDetailsOther.ctnListBank.lstHistoryProfile.LastIndex,5) = jsBank.Child(i).Value("remark")
		        
		      next
		    End
		  End
		  
		  If jsPerson.HasName("educations") Then
		    Dim jsEducation As New JSONItem
		    jsEducation = jsPerson.Value("educations")
		    If jsEducation.ToString <> "" Then
		      Me.ctnListEducation.lstHistoryProfile.DeleteAllRows
		      For i As Integer = 0 To jsEducation.Count-1
		        Me.ctnListEducation.lstHistoryProfile.AddRow
		        Me.ctnListEducation.lstHistoryProfile.Cell(Me.ctnListEducation.lstHistoryProfile.LastIndex,0) = Me.ctnListEducation.lstHistoryProfile.ListCount.ToText
		        Me.ctnListEducation.lstHistoryProfile.CellType(Me.ctnListEducation.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		        Me.ctnListEducation.lstHistoryProfile.CellTag(Me.ctnListEducation.lstHistoryProfile.LastIndex,0) = jsEducation.Child(i).Value("id")
		        
		        Me.ctnListEducation.lstHistoryProfile.CellType(Me.ctnListEducation.lstHistoryProfile.LastIndex,1) = Listbox.TypeCheckbox
		        If jsEducation.Child(i).HasName("useDefault") Then
		          If jsEducation.Child(i).Value("useDefault").BooleanValue Then
		            Me.ctnListEducation.lstHistoryProfile.CellState(Me.ctnListEducation.lstHistoryProfile.LastIndex,1) = CheckBox.CheckedStates.Checked
		          End
		        End
		        
		        Me.ctnListEducation.lstHistoryProfile.CellType(Me.ctnListEducation.lstHistoryProfile.LastIndex,2) = Listbox.TypeCheckbox
		        If jsEducation.Child(i).HasName("high") Then
		          If jsEducation.Child(i).Value("high").BooleanValue Then
		            
		            
		            Me.ctnListEducation.lstHistoryProfile.CellState(Me.ctnListEducation.lstHistoryProfile.LastIndex,2) = CheckBox.CheckedStates.Checked
		          End
		        End
		        
		        Me.ctnListEducation.lstHistoryProfile.Cell(Me.ctnListEducation.lstHistoryProfile.LastIndex,3) = jsEducation.Child(i).Value("certificateName")
		        Me.ctnListEducation.lstHistoryProfile.Cell(Me.ctnListEducation.lstHistoryProfile.LastIndex,4) = jsEducation.Child(i).Value("institutionName")
		        Me.ctnListEducation.lstHistoryProfile.Cell(Me.ctnListEducation.lstHistoryProfile.LastIndex,5) = jsEducation.Child(i).Value("facultyName")
		        Me.ctnListEducation.lstHistoryProfile.Cell(Me.ctnListEducation.lstHistoryProfile.LastIndex,6) = jsEducation.Child(i).Value("majorName")
		        Me.ctnListEducation.lstHistoryProfile.Cell(Me.ctnListEducation.lstHistoryProfile.LastIndex,7) = jsEducation.Child(i).Value("gpa")
		        Me.ctnListEducation.lstHistoryProfile.Cell(Me.ctnListEducation.lstHistoryProfile.LastIndex,8) = jsEducation.Child(i).Value("startYear")
		        Me.ctnListEducation.lstHistoryProfile.Cell(Me.ctnListEducation.lstHistoryProfile.LastIndex,9) = jsEducation.Child(i).Value("endYear")
		        Me.ctnListEducation.lstHistoryProfile.Cell(Me.ctnListEducation.lstHistoryProfile.LastIndex,10) = jsEducation.Child(i).Value("country")
		        Me.ctnListEducation.lstHistoryProfile.Cell(Me.ctnListEducation.lstHistoryProfile.LastIndex,11) = jsEducation.Child(i).Value("remark")
		        Me.ctnListEducation.lstHistoryProfile.RowTag(Me.ctnListEducation.lstHistoryProfile.LastIndex) = Me.ctnListEducation.lstHistoryProfile.LastIndex
		      Next
		    End
		    
		  End
		  
		  If jsPerson.HasName("relative") Then
		    Dim s As String = jsPerson.Value("relative")
		    Dim jsRelative As New JSONItem(s)
		    If jsRelative.ToString <> "" Then
		      Me.ctnListRelative.lstHistoryProfile.DeleteAllRows
		      For i As Integer = 0 To jsRelative.Count-1
		        Me.ctnListRelative.lstHistoryProfile.AddRow
		        Me.ctnListRelative.lstHistoryProfile.Cell(Me.ctnListRelative.lstHistoryProfile.LastIndex,0) = Me.ctnListRelative.lstHistoryProfile.ListCount.ToText
		        Me.ctnListRelative.lstHistoryProfile.Cell(Me.ctnListRelative.lstHistoryProfile.LastIndex,1) = jsRelative.Child(i).Value("nameTitleName")
		        Me.ctnListRelative.lstHistoryProfile.CellTag(Me.ctnListRelative.lstHistoryProfile.LastIndex,1) = jsRelative.Child(i).Value("nameTitle")
		        Me.ctnListRelative.lstHistoryProfile.Cell(Me.ctnListRelative.lstHistoryProfile.LastIndex,2) = jsRelative.Child(i).Value("firstName")
		        Me.ctnListRelative.lstHistoryProfile.Cell(Me.ctnListRelative.lstHistoryProfile.LastIndex,3) = jsRelative.Child(i).Value("lastName")
		        Me.ctnListRelative.lstHistoryProfile.Cell(Me.ctnListRelative.lstHistoryProfile.LastIndex,4) = jsRelative.Child(i).Value("gender")
		        Me.ctnListRelative.lstHistoryProfile.Cell(Me.ctnListRelative.lstHistoryProfile.LastIndex,5) = jsRelative.Child(i).Value("relationship")
		        Me.ctnListRelative.lstHistoryProfile.Cell(Me.ctnListRelative.lstHistoryProfile.LastIndex,6) = jsRelative.Child(i).Value("identificationNo")
		        Me.ctnListRelative.lstHistoryProfile.Cell(Me.ctnListRelative.lstHistoryProfile.LastIndex,7) = jsRelative.Child(i).Value("job")
		        Me.ctnListRelative.lstHistoryProfile.Cell(Me.ctnListRelative.lstHistoryProfile.LastIndex,8) = jsRelative.Child(i).Value("DOB")
		        Me.ctnListRelative.lstHistoryProfile.Cell(Me.ctnListRelative.lstHistoryProfile.LastIndex,9) = jsRelative.Child(i).Value("status")
		        Me.ctnListRelative.lstHistoryProfile.Cell(Me.ctnListRelative.lstHistoryProfile.LastIndex,10) = jsRelative.Child(i).Value("deathDate")
		        Me.ctnListRelative.lstHistoryProfile.Cell(Me.ctnListRelative.lstHistoryProfile.LastIndex,11) = jsRelative.Child(i).Value("phoneNo")
		        Me.ctnListRelative.lstHistoryProfile.Cell(Me.ctnListRelative.lstHistoryProfile.LastIndex,12) = jsRelative.Child(i).Value("remark")
		      Next
		      
		    End
		  End
		  
		  If jsPerson.HasName("workExperience") Then
		    Dim s As String = jsPerson.Value("workExperience")
		    Dim jsWorkExperience As New JSONItem(s)
		    If jsWorkExperience.ToString <> "" Then
		      Me.ctnListExperience.lstHistoryProfile.DeleteAllRows
		      For i As Integer = 0 To jsWorkExperience.Count-1
		        Me.ctnListExperience.lstHistoryProfile.AddRow
		        Me.ctnListExperience.lstHistoryProfile.Cell(Me.ctnListExperience.lstHistoryProfile.LastIndex,0) = Me.ctnListExperience.lstHistoryProfile.ListCount.ToText
		        Me.ctnListExperience.lstHistoryProfile.Cell(Me.ctnListExperience.lstHistoryProfile.LastIndex,1) = jsWorkExperience.Child(i).Value("company")
		        Me.ctnListExperience.lstHistoryProfile.Cell(Me.ctnListExperience.lstHistoryProfile.LastIndex,2) = jsWorkExperience.Child(i).Value("position")
		        Me.ctnListExperience.lstHistoryProfile.Cell(Me.ctnListExperience.lstHistoryProfile.LastIndex,3) = jsWorkExperience.Child(i).Value("salary")
		        Me.ctnListExperience.lstHistoryProfile.Cell(Me.ctnListExperience.lstHistoryProfile.LastIndex,4) = jsWorkExperience.Child(i).Value("effectiveDate")
		        Me.ctnListExperience.lstHistoryProfile.Cell(Me.ctnListExperience.lstHistoryProfile.LastIndex,5) = jsWorkExperience.Child(i).Value("endDate")
		        Me.ctnListExperience.lstHistoryProfile.Cell(Me.ctnListExperience.lstHistoryProfile.LastIndex,6) = jsWorkExperience.Child(i).Value("jobType")
		        Me.ctnListExperience.lstHistoryProfile.Cell(Me.ctnListExperience.lstHistoryProfile.LastIndex,7) = jsWorkExperience.Child(i).Value("jobLevel")
		        Me.ctnListExperience.lstHistoryProfile.Cell(Me.ctnListExperience.lstHistoryProfile.LastIndex,8) = jsWorkExperience.Child(i).Value("jobDuration")
		        Me.ctnListExperience.lstHistoryProfile.Cell(Me.ctnListExperience.lstHistoryProfile.LastIndex,9) = jsWorkExperience.Child(i).Value("leaveReason")
		        Me.ctnListExperience.lstHistoryProfile.Cell(Me.ctnListExperience.lstHistoryProfile.LastIndex,10) = jsWorkExperience.Child(i).Value("remark")
		      Next
		    End
		  End
		  
		  If jsPerson.HasName("skill") Then
		    Dim s As String = jsPerson.Value("skill")
		    Dim jsSkill As New JSONItem(s)
		    If jsSkill.ToString <> "" Then
		      Me.ctnListSkill.lstHistoryProfile.DeleteAllRows
		      For i As Integer = 0 To jsSkill.Count-1
		        Me.ctnListSkill.lstHistoryProfile.AddRow
		        Me.ctnListSkill.lstHistoryProfile.Cell(Me.ctnListSkill.lstHistoryProfile.LastIndex,0) = Me.ctnListSkill.lstHistoryProfile.ListCount.ToText
		        Me.ctnListSkill.lstHistoryProfile.Cell(Me.ctnListSkill.lstHistoryProfile.LastIndex,1) = jsSkill.Child(i).Value("skillName")
		        Me.ctnListSkill.lstHistoryProfile.Cell(Me.ctnListSkill.lstHistoryProfile.LastIndex,2) = jsSkill.Child(i).Value("skillDetailsName")
		        Me.ctnListSkill.lstHistoryProfile.Cell(Me.ctnListSkill.lstHistoryProfile.LastIndex,3) = jsSkill.Child(i).Value("skillLevelName")
		        Me.ctnListSkill.lstHistoryProfile.Cell(Me.ctnListSkill.lstHistoryProfile.LastIndex,4) = jsSkill.Child(i).Value("remark")
		      Next
		    End
		  End
		  
		  If jsPerson.HasName("changeName") Then
		  End
		  
		  If jsPerson.HasName("personnelHistory") Then
		    Dim jsHist As New JSONItem
		    jsHist = jsPerson.Value("personnelHistory") 
		    If jsHist.ToString <> "" Then
		      For i As Integer = 0 To jsHist.Count-1
		        Me.ctnListEvent.lstHistoryProfile.AddRow
		        Me.ctnListEvent.lstHistoryProfile.Cell(Me.ctnListEvent.lstHistoryProfile.LastIndex,0) = Me.ctnListEvent.lstHistoryProfile.ListCount.ToText
		        'Me.ctnListEvent.lstHistoryProfile.Cell(Me.ctnListEvent.lstHistoryProfile.LastIndex,1) = jsHist.Child(i).Value("title")
		        Me.ctnListEvent.lstHistoryProfile.Cell(Me.ctnListEvent.lstHistoryProfile.LastIndex,2) = jsHist.Child(i).Value("eventDate")
		        Me.ctnListEvent.lstHistoryProfile.Cell(Me.ctnListEvent.lstHistoryProfile.LastIndex,3) = jsHist.Child(i).Value("title")
		        Me.ctnListEvent.lstHistoryProfile.Cell(Me.ctnListEvent.lstHistoryProfile.LastIndex,4) = jsHist.Child(i).Value("detail")
		      Next
		    End
		  End
		  
		  If jsPerson.HasName("incomeDeductConfig") Then
		    Dim jsINDC As New JSONItem
		    jsINDC = jsPerson.Value("incomeDeductConfig") 
		    If jsINDC.ToString <> "" Then
		      For i As Integer = 0 To jsINDC.Count-1 
		        Dim icase As Integer = jsINDC.Child(i).Value("incomeDeductType").IntegerValue
		        Select Case icase
		        Case 1
		          Me.ctnListIncome.lstHistoryProfile.AddRow
		          Me.ctnListIncome.lstHistoryProfile.Cell(Me.ctnListIncome.lstHistoryProfile.LastIndex,0) = Me.ctnListIncome.lstHistoryProfile.ListCount.ToText
		          Me.ctnListIncome.lstHistoryProfile.Cell(Me.ctnListIncome.lstHistoryProfile.LastIndex,1) = jsINDC.Child(i).Value("incomeDeductCode")
		          Me.ctnListIncome.lstHistoryProfile.Cell(Me.ctnListIncome.lstHistoryProfile.LastIndex,2) = jsINDC.Child(i).Value("incomeDeductName")
		          Me.ctnListIncome.lstHistoryProfile.Cell(Me.ctnListIncome.lstHistoryProfile.LastIndex,3) = Str(jsINDC.Child(i).Value("amount").IntegerValue)
		          Me.ctnListIncome.lstHistoryProfile.Cell(Me.ctnListIncome.lstHistoryProfile.LastIndex,7) = jsINDC.Child(i).Value("remark")
		        Case 2
		          Me.ctnListDeduct.lstHistoryProfile.AddRow
		          Me.ctnListDeduct.lstHistoryProfile.Cell(Me.ctnListDeduct.lstHistoryProfile.LastIndex,0) = Me.ctnListDeduct.lstHistoryProfile.ListCount.ToText
		          Me.ctnListDeduct.lstHistoryProfile.Cell(Me.ctnListDeduct.lstHistoryProfile.LastIndex,1) = jsINDC.Child(i).Value("incomeDeductCode")
		          Me.ctnListDeduct.lstHistoryProfile.Cell(Me.ctnListDeduct.lstHistoryProfile.LastIndex,2) = jsINDC.Child(i).Value("incomeDeductName")
		          Me.ctnListDeduct.lstHistoryProfile.Cell(Me.ctnListDeduct.lstHistoryProfile.LastIndex,3) = Str(jsINDC.Child(i).Value("amount").IntegerValue)
		          Me.ctnListDeduct.lstHistoryProfile.Cell(Me.ctnListDeduct.lstHistoryProfile.LastIndex,7) = jsINDC.Child(i).Value("remark")
		        End Select
		      Next
		    End
		  End
		  
		  If jsPerson.HasName("leaveDocument") Then
		    Dim jsLeave As New JSONItem
		    jsLeave = jsPerson.Value("leaveDocument")
		    If jsLeave.ToString <> "" Then
		      For i As Integer = 0 To jsLeave.Count -1
		      Next
		    End
		  End
		  
		  If jsPerson.HasName("positionHistory") Then
		    Dim jsPosHist As New JSONItem
		    jsPosHist = jsPerson.Value("positionHistory")
		    If jsPosHist.ToString <> "" Then
		      For i As Integer = 0 To jsPosHist.Count -1
		      Next
		    End
		  End
		  
		  If jsPerson.HasName("payrollHistory") Then
		    Dim jsPayRoll As New JSONItem
		    jsPayRoll = jsPerson.Value("payrollHistory")
		    If jsPayRoll.ToString <> "" Then
		      For i As Integer = 0 To jsPayRoll.Count -1
		      Next
		    End
		  End
		  
		  If jsPerson.HasName("fundHistory") Then
		    Dim jsFund As New JSONItem
		    jsFund = jsPerson.Value("fundHistory")
		    If jsFund.ToString <> "" Then
		      For i As Integer = 0 To jsFund.Count -1
		      Next
		    End
		  End
		  
		  If jsPerson.HasName("SSIHistory") Then
		    Dim jsSSI As New JSONItem
		    jsSSI = jsPerson.Value("SSIHistory")
		    If jsSSI.ToString <> "" Then
		      For i As Integer = 0 To jsSSI.Count -1
		      Next
		    End
		  End
		  
		  If jsPerson.HasName("disciplineHistory") Then
		    Dim jsDiscipline As New JSONItem
		    jsDiscipline = jsPerson.Value("disciplineHistory")
		    If jsDiscipline.ToString <> "" Then
		      For i As Integer = 0 To jsDiscipline.Count -1
		      Next
		    End
		  End
		  
		  If jsPerson.HasName("insigniaHistory") Then
		    Dim jsInsignia As New JSONItem
		    jsInsignia = jsPerson.Value("insigniaHistory")
		    If jsInsignia.ToString <> "" Then
		      For i As Integer = 0 To jsInsignia.Count -1
		      Next
		    End
		  End
		  
		  Exception err As JSONException
		    MsgBox "ไม่สามารถดึงข้อมูล JSON ได้ กรุณาลองใหม่อีกครั้ง"
		  Exception err As KeyNotFoundException
		    MsgBox "หา key ไม่เจอ"
		  Exception err As IllegalCastException
		    MsgBox "ผิดชนิด"
		    
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		CurrentID As String
	#tag EndProperty


#tag EndWindowCode

#tag Events TabPanel1
	#tag Event
		Sub Change()
		  'MsgBox me.Value.ToText
		  if me.Value = 0 then
		    'self.ctnGeneralForm.Visible = True
		    self.ActionSegment
		  else
		    Self.HideComponent
		  end
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events SegmentedControl1
	#tag Event
		Sub Action(itemIndex as integer)
		  Self.ActionSegment
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CurrentID"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
