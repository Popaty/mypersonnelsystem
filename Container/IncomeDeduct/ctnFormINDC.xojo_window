#tag Window
Begin ContainerControl ctnFormINDC
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   611
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   944
   Begin Rectangle Rectangle1
      AutoDeactivate  =   True
      BorderWidth     =   1
      BottomRightColor=   &c00000000
      Enabled         =   True
      FillColor       =   &cFFFFFF00
      Height          =   611
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   48
      TabPanelIndex   =   0
      Top             =   0
      TopLeftColor    =   &c00000000
      Visible         =   True
      Width           =   944
      Begin ctnListHistoryProfile ctnListDeduct
         AcceptFocus     =   False
         AcceptTabs      =   True
         AutoDeactivate  =   True
         BackColor       =   &cFFFFFF00
         Backdrop        =   0
         CategoryName    =   ""
         Enabled         =   True
         EraseBackground =   True
         HasBackColor    =   False
         Height          =   200
         HelpTag         =   ""
         InitialParent   =   "Rectangle1"
         Left            =   20
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         Top             =   391
         Transparent     =   True
         UseFocusRing    =   False
         Visible         =   True
         Width           =   904
      End
      Begin ctnListHistoryProfile ctnListIncome
         AcceptFocus     =   False
         AcceptTabs      =   True
         AutoDeactivate  =   True
         BackColor       =   &cFFFFFF00
         Backdrop        =   0
         CategoryName    =   ""
         Enabled         =   True
         EraseBackground =   True
         HasBackColor    =   False
         Height          =   200
         HelpTag         =   ""
         InitialParent   =   "Rectangle1"
         Left            =   20
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   0
         TabStop         =   True
         Top             =   179
         Transparent     =   True
         UseFocusRing    =   False
         Visible         =   True
         Width           =   904
      End
      Begin Label Label4
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   456
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   2
         TabPanelIndex   =   0
         Text            =   "รายละเอียด"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   20
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin TextArea taDetails
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   True
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   55
         HelpTag         =   ""
         HideSelection   =   True
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   570
         LimitText       =   0
         LineHeight      =   0.0
         LineSpacing     =   1.0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Multiline       =   True
         ReadOnly        =   False
         Scope           =   0
         ScrollbarHorizontal=   False
         ScrollbarVertical=   True
         Styled          =   True
         TabIndex        =   4
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   20
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   356
      End
      Begin Label Label3
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   458
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   5
         TabPanelIndex   =   0
         Text            =   "หมายเหตุ"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   85
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label Label5
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   20
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   6
         TabPanelIndex   =   0
         Text            =   "พนักงาน"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   85
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin TextArea taRemark
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   True
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   55
         HelpTag         =   ""
         HideSelection   =   True
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   570
         LimitText       =   0
         LineHeight      =   0.0
         LineSpacing     =   1.0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Multiline       =   True
         ReadOnly        =   False
         Scope           =   0
         ScrollbarHorizontal=   False
         ScrollbarVertical=   True
         Styled          =   True
         TabIndex        =   7
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   84
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   356
      End
      Begin BevelButton BevelButton1
         AcceptFocus     =   True
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "ค้นหาพนักงานจากรายชื่อ"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   ""
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   132
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   8
         TabPanelIndex   =   0
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   199
      End
      Begin TextField fldPersonnelInform
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   132
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   9
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   119
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   311
      End
   End
   Begin Label Label1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   30
      TabPanelIndex   =   0
      Text            =   "รหัสเอกสาร"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   20
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin TextField fldDocumentCode
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   132
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   29
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   19
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   199
   End
   Begin TextField fldTitle
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      Italic          =   False
      Left            =   132
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   39
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   51
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   199
   End
   Begin Label Label2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   28
      TabPanelIndex   =   0
      Text            =   "เรื่อง"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   52
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  Self.ctnListIncome.lblTopic.Text = "เงินได้"
		  Self.ctnListDeduct.lblTopic.Text = "เงินหัก"
		  Self.ctnListIncome.CategoryName = "income"
		  Self.ctnListDeduct.CategoryName = "deduct"
		  Self.ctnListIncome.SetIncome
		  Self.ctnListDeduct.SetDeduct
		  Self.ctnListIncome.lstHistoryProfile.Height = 150
		  Self.ctnListDeduct.lstHistoryProfile.Height = 150
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub BindData()
		  Dim data as new JSONItem
		  data = me.GetValue
		  MsgBox data.ToString
		  me.SendData(data)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetValue() As JSONItem
		  Dim jsData As New JSONItem
		  'jsData.Value("id") = Self.CurrentID
		  jsData.Value("id") = ""
		  jsData.Value("code") = Self.fldDocumentCode.Text
		  jsData.Value("title") = Self.fldTitle.Text
		  jsData.Value("detail") = Self.taDetails.Text
		  jsData.Value("remark") = Self.taRemark.Text
		  jsData.Value("organization") = IncomeDeductWindow.ctnListINDC1.SelectedOrganization
		  jsData.Value("personnel") = Self.PersonSelected.Value("personnelId")
		  
		  Dim incomeDeductJson As New JSONItem
		  Dim incomeDeductNew As New JSONItem
		  Dim incomeDeductUpdate As New JSONItem
		  Dim incomeDeductDelete As New JSONItem
		  Dim incomeDeductTemp As New JSONItem
		  Dim incomeDeductItem As New JSONItem
		  
		  incomeDeductTemp = Self.ctnListIncome.GetValue("income")
		  
		  incomeDeductItem = incomeDeductTemp
		  If incomeDeductItem.HasName("new") Then
		    incomeDeductItem = incomeDeductTemp.Value("new")
		    If incomeDeductItem.Count > 0 Then
		      For i As Integer =0 To incomeDeductItem.Count-1
		        incomeDeductNew.Append(incomeDeductItem.Child(i))
		      Next
		    End
		  End
		  
		  incomeDeductItem = incomeDeductTemp
		  If incomeDeductItem.HasName("update") Then
		    incomeDeductItem = incomeDeductTemp.Value("update")
		    If incomeDeductItem.Count > 0 Then
		      For i As Integer =0 To incomeDeductItem.Count-1
		        incomeDeductUpdate.Append(incomeDeductItem.Child(i))
		      Next
		    End
		  End
		  
		  incomeDeductItem = incomeDeductTemp
		  If incomeDeductItem.HasName("delete") Then
		    incomeDeductItem = incomeDeductTemp.Value("delete")
		    If incomeDeductItem.Count > 0 Then
		      For i As Integer =0 To incomeDeductItem.Count-1
		        incomeDeductDelete.Append(incomeDeductItem.Child(i))
		      Next
		    End
		  End
		  
		  incomeDeductTemp = Self.ctnListDeduct.GetValue("deduct")
		  
		  incomeDeductItem = incomeDeductTemp
		  If incomeDeductItem.HasName("new") Then
		    incomeDeductItem = incomeDeductTemp.Value("new")
		    If incomeDeductItem.Count > 0 Then
		      For i As Integer =0 To incomeDeductItem.Count-1
		        incomeDeductNew.Append(incomeDeductItem.Child(i))
		      Next
		    End
		  End
		  
		  incomeDeductItem = incomeDeductTemp
		  If incomeDeductItem.HasName("update") Then
		    incomeDeductItem = incomeDeductTemp.Value("update")
		    If incomeDeductItem.Count > 0 Then
		      For i As Integer =0 To incomeDeductItem.Count-1
		        incomeDeductUpdate.Append(incomeDeductItem.Child(i))
		      Next
		    End
		  End
		  
		  incomeDeductItem = incomeDeductTemp
		  If incomeDeductItem.HasName("delete") Then
		    incomeDeductItem = incomeDeductTemp.Value("delete")
		    If incomeDeductItem.Count > 0 Then
		      For i As Integer =0 To incomeDeductItem.Count-1
		        incomeDeductDelete.Append(incomeDeductItem.Child(i))
		      Next
		    End
		  End
		  
		  If incomeDeductNew.Count > 0 Then
		    incomeDeductJson.Value("new") = incomeDeductNew
		  End
		  If incomeDeductUpdate.Count > 0 Then
		    incomeDeductJson.Value("update") = incomeDeductUpdate
		  End
		  If incomeDeductDelete.Count > 0 Then
		    incomeDeductJson.Value("delete") = incomeDeductDelete
		  End
		  
		  jsData.Value("incomeDeductItem") = incomeDeductJson.ToString
		  
		  Return jsData
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SendData(formData as JSONItem)
		  'MsgBox formData.ToString
		  Dim pincInstance As New Repo.WS.PersonnelIncomeDeductDocument
		  If formData.Value("id") = "" Then
		    If pincInstance.Save(formData) Then
		      MsgBox "เพิ่มข้อมูลสำเร็จ"
		    Else
		      MsgBox "เพิ่มข้อมูลไม่สำเร็จ"
		    End
		  Else
		    If pincInstance.Update(formData) Then
		      MsgBox "แก้ไขข้อมูลสำเร็จ"
		    Else
		      MsgBox "แก้ไขข้อมูลไม่สำเร็จ"
		    End
		  End
		  IncomeDeductWindow.ctnListINDC1.LoadLists
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetID(id as string)
		  self.ctnListIncome.lstHistoryProfile.DeleteAllRows
		  self.ctnListDeduct.lstHistoryProfile.DeleteAllRows
		  self.CurrentID = id
		  self.SetValue
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetIncomeDeductToList(data as JSONItem)
		  Dim sType as String
		  MsgBox data.ToString
		  sType = data.Value("type")
		  
		  Select case sType
		  case "1"
		    self.ctnListIncome.lstHistoryProfile.AddRow
		    self.ctnListIncome.lstHistoryProfile.Cell(self.ctnListIncome.lstHistoryProfile.LastIndex,0) = self.ctnListIncome.lstHistoryProfile.ListCount.ToText
		    self.ctnListIncome.lstHistoryProfile.CellType(self.ctnListIncome.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		    self.ctnListIncome.lstHistoryProfile.CellTag(self.ctnListIncome.lstHistoryProfile.LastIndex,0) = data.Value("id")
		    self.ctnListIncome.lstHistoryProfile.Cell(self.ctnListIncome.lstHistoryProfile.LastIndex,1) = data.Value("incomeDeductTypeCode")
		    self.ctnListIncome.lstHistoryProfile.CellTag(self.ctnListIncome.lstHistoryProfile.LastIndex,1) = data.Value("incomeDeductTypeId")
		    self.ctnListIncome.lstHistoryProfile.Cell(self.ctnListIncome.lstHistoryProfile.LastIndex,2) = data.Value("incomeDeductTypeName")
		    self.ctnListIncome.lstHistoryProfile.Cell(self.ctnListIncome.lstHistoryProfile.LastIndex,3) = Str(Val(data.Value("amount")))
		    self.ctnListIncome.lstHistoryProfile.Cell(self.ctnListIncome.lstHistoryProfile.LastIndex,4) = data.Value("annuityName")
		    self.ctnListIncome.lstHistoryProfile.CellTag(self.ctnListIncome.lstHistoryProfile.LastIndex,4) = data.Value("annuityId")
		    self.ctnListIncome.lstHistoryProfile.Cell(self.ctnListIncome.lstHistoryProfile.LastIndex,5) = data.Value("startDate")
		    self.ctnListIncome.lstHistoryProfile.Cell(self.ctnListIncome.lstHistoryProfile.LastIndex,6) = data.Value("endDate")
		    self.ctnListIncome.lstHistoryProfile.Cell(self.ctnListIncome.lstHistoryProfile.LastIndex,7) = data.Value("remark")
		    self.ctnListIncome.lstHistoryProfile.RowTag(self.ctnListIncome.lstHistoryProfile.LastIndex) = self.ctnListIncome.lstHistoryProfile.LastIndex
		    
		  case "2"
		    self.ctnListDeduct.lstHistoryProfile.AddRow
		    self.ctnListDeduct.lstHistoryProfile.Cell(self.ctnListDeduct.lstHistoryProfile.LastIndex,0) = self.ctnListIncome.lstHistoryProfile.ListCount.ToText
		    self.ctnListDeduct.lstHistoryProfile.CellType(self.ctnListIncome.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		    self.ctnListDeduct.lstHistoryProfile.CellTag(self.ctnListIncome.lstHistoryProfile.LastIndex,0) = data.Value("id")
		    self.ctnListDeduct.lstHistoryProfile.Cell(self.ctnListIncome.lstHistoryProfile.LastIndex,1) = data.Value("incomeDeductTypeCode")
		    self.ctnListDeduct.lstHistoryProfile.CellTag(self.ctnListIncome.lstHistoryProfile.LastIndex,1) = data.Value("incomeDeductTypeId")
		    self.ctnListDeduct.lstHistoryProfile.Cell(self.ctnListIncome.lstHistoryProfile.LastIndex,2) = data.Value("incomeDeductTypeName")
		    self.ctnListDeduct.lstHistoryProfile.Cell(self.ctnListIncome.lstHistoryProfile.LastIndex,3) = Str(Val(data.Value("amount")))
		    self.ctnListDeduct.lstHistoryProfile.Cell(self.ctnListIncome.lstHistoryProfile.LastIndex,4) = data.Value("annuityName")
		    self.ctnListDeduct.lstHistoryProfile.CellTag(self.ctnListIncome.lstHistoryProfile.LastIndex,4) = data.Value("annuityId")
		    self.ctnListDeduct.lstHistoryProfile.Cell(self.ctnListIncome.lstHistoryProfile.LastIndex,5) = data.Value("startDate")
		    self.ctnListDeduct.lstHistoryProfile.Cell(self.ctnListIncome.lstHistoryProfile.LastIndex,6) = data.Value("endDate")
		    self.ctnListDeduct.lstHistoryProfile.Cell(self.ctnListIncome.lstHistoryProfile.LastIndex,7) = data.Value("remark")
		    self.ctnListDeduct.lstHistoryProfile.RowTag(self.ctnListIncome.lstHistoryProfile.LastIndex) = self.ctnListIncome.lstHistoryProfile.LastIndex
		    
		  end
		  
		  'Exception err as KeyNotFoundException
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetValue()
		  'MsgBox self.CurrentID
		  self.fldPersonnelInform.Text = self.PersonSelected.Value("name")
		  Dim psIncInstance as new Repo.WS.PersonnelIncomeDeductDocument
		  Dim jsInc as new JSONItem
		  Dim jsItem as new JSONItem
		  
		  jsInc = psIncInstance.GetIncomeDeductByID(self.CurrentID)
		  
		  'MsgBox jsInc.ToString
		  for i as integer = 0 to jsInc.Count-1
		    jsItem = jsInc.Child(i)
		    self.SetIncomeDeductToList(jsItem)
		  next
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		CurrentID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		divSelected As String
	#tag EndProperty

	#tag Property, Flags = &h0
		orgID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		ParentPositionSelected As String
	#tag EndProperty

	#tag Property, Flags = &h0
		PersonSelected As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		PositionSelected As String
	#tag EndProperty

	#tag Property, Flags = &h0
		typeSelection As String
	#tag EndProperty

	#tag Property, Flags = &h0
		workSelected As String
	#tag EndProperty


#tag EndWindowCode

#tag Events BevelButton1
	#tag Event
		Sub Action()
		  Dim psModel as new INDCSearchPersonWindow
		  psModel.Show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CurrentID"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="divSelected"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="orgID"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ParentPositionSelected"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="PositionSelected"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="typeSelection"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="workSelected"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
#tag EndViewBehavior
