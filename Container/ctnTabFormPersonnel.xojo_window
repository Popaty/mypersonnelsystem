#tag Window
Begin ContainerControl ctnTabFormPersonnel
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   True
   Height          =   611
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   True
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   944
   Begin Rectangle Rectangle1
      AutoDeactivate  =   True
      BorderWidth     =   1
      BottomRightColor=   &c7F007F7F
      Enabled         =   True
      FillColor       =   &cFF00FFFF
      Height          =   611
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   2
      TabIndex        =   0
      TabPanelIndex   =   0
      Top             =   0
      TopLeftColor    =   &c7F007F7F
      Visible         =   True
      Width           =   944
      Begin TabPanel TabPanel1
         AutoDeactivate  =   True
         Bold            =   False
         Enabled         =   True
         Height          =   571
         HelpTag         =   ""
         Index           =   0
         InitialParent   =   "Rectangle1"
         Italic          =   False
         Left            =   20
         LockBottom      =   True
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         Panels          =   ""
         Scope           =   2
         SmallTabs       =   True
         TabDefinition   =   "ข้อมูลทั่วไป\rลดหย่อนภาษี\rเหตุการณ์\rเงินได้ - เงินหัก\rวันลา\rตำแหน่ง\rประวัติการรับเงิน\rกองทุน\rวินัย\rเครื่องราชอิสริยาภรณ์\rเอกสาร"
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   20
         Underline       =   False
         Value           =   0
         Visible         =   True
         Width           =   904
         Begin ctnListHistoryProfile ctnListEvent
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFF00FFFF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   3
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnListHistoryProfile ctnListFund
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFF00FFFF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   8
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnListHistoryProfile ctnListLeave
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFF00FFFF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   5
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnListHistoryProfile ctnListHistoryPayment
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFF00FFFF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   7
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnListHistoryProfile ctnListHistoryPosition
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFF00FFFF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   6
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnListHistoryProfile ctnListInsignia
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFF00FFFF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   10
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnListHistoryProfile ctnListIncome
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFF00FFFF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   275
            HelpTag         =   ""
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   5
            TabPanelIndex   =   4
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnListHistoryProfile ctnListDeduct
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFF00FFFF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   275
            HelpTag         =   ""
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   6
            TabPanelIndex   =   4
            TabStop         =   True
            Top             =   316
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnUploadFile ctnListUploadFile
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFF00FFFF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   11
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         BeginSegmented SegmentedControl SegmentedControl1
            Enabled         =   True
            Height          =   24
            Index           =   -2147483648
            InitialParent   =   "TabPanel1$0"
            Left            =   50
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            MacControlStyle =   3
            Scope           =   0
            Segments        =   "ประวัติ\n\nTrue\rอื่นๆ\n\nFalse\rการศึกษา\n\nFalse\rผู้ที่เกี่ยวข้อง\n\nFalse\rประสบการณ์ทำงาน\n\nFalse\rทักษะ\n\nFalse\rเปลี่ยนชื่อ\n\nFalse"
            SelectionType   =   0
            TabPanelIndex   =   1
            Top             =   58
            Visible         =   True
            Width           =   844
         End
         Begin ctnFormDetails ctnGeneralForm
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFF00FFFF
            Backdrop        =   0
            Enabled         =   True
            EraseBackground =   True
            gender          =   ""
            HasBackColor    =   False
            Height          =   497
            HelpTag         =   ""
            InitialParent   =   "TabPanel1$0"
            Left            =   1000
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            resignate       =   False
            Scope           =   0
            TabIndex        =   1
            TabPanelIndex   =   1
            TabStop         =   True
            Top             =   94
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   False
            Width           =   904
         End
         Begin ctnFormDetailsOther ctnOtherForm
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFF00FFFF
            Backdrop        =   0
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   497
            HelpTag         =   ""
            InitialParent   =   "TabPanel1$0"
            Left            =   1000
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   2
            TabPanelIndex   =   1
            TabStop         =   True
            Top             =   20
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnListHistoryProfile ctnListDiscipline
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFF00FFFF
            Backdrop        =   0
            CategoryName    =   ""
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   True
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   True
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   9
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
         Begin ctnFormTaxReduce ctnTaxReduceForm
            AcceptFocus     =   False
            AcceptTabs      =   True
            AutoDeactivate  =   True
            BackColor       =   &cFF00FFFF
            Backdrop        =   0
            Enabled         =   True
            EraseBackground =   True
            HasBackColor    =   False
            Height          =   548
            HelpTag         =   ""
            InitialParent   =   "TabPanel1$0"
            Left            =   20
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   2
            TabStop         =   True
            Top             =   43
            Transparent     =   True
            UseFocusRing    =   False
            Visible         =   True
            Width           =   904
         End
      End
   End
   Begin ctnListHistoryProfile ctnListRelative
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFF00FFFF
      Backdrop        =   0
      CategoryName    =   ""
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   497
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   1038
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   43
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   904
   End
   Begin ctnListHistoryProfile ctnListEducation
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFF00FFFF
      Backdrop        =   0
      CategoryName    =   ""
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   497
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   1058
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   904
   End
   Begin ctnListHistoryProfile ctnListExperience
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFF00FFFF
      Backdrop        =   0
      CategoryName    =   ""
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   497
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   1078
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   20
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   904
   End
   Begin ctnListHistoryProfile ctnListNameChange
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFF00FFFF
      Backdrop        =   0
      CategoryName    =   ""
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   497
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   1098
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   40
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   904
   End
   Begin ctnListHistoryProfile ctnListSkill
      AcceptFocus     =   False
      AcceptTabs      =   True
      AutoDeactivate  =   True
      BackColor       =   &cFF00FFFF
      Backdrop        =   0
      CategoryName    =   ""
      Enabled         =   True
      EraseBackground =   True
      HasBackColor    =   False
      Height          =   497
      HelpTag         =   ""
      InitialParent   =   ""
      Left            =   1118
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   60
      Transparent     =   True
      UseFocusRing    =   False
      Visible         =   True
      Width           =   904
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  Self.ctnListEducation.SetEducation
		  Self.ctnListEvent.SetEvent
		  Self.ctnListExperience.SetExperience
		  Self.ctnListFund.SetFund
		  Self.ctnListHistoryPayment.SetHistoryPayment
		  Self.ctnListHistoryPosition.SetHistoryPosition
		  Self.ctnListInsignia.SetInsignia
		  Self.ctnListLeave.SetLeave
		  Self.ctnListNameChange.SetNameChange
		  Self.ctnListRelative.SetRelative
		  Self.ctnListSkill.SetSkill
		  Self.ctnListIncome.SetIncome
		  Self.ctnListDeduct.SetDeduct
		  Self.ctnListDiscipline.SetDiscipline
		  Self.ctnOtherForm.ctnListBank.SetBankAccount
		  Self.ctnOtherForm.ctnListAddress.SetAddress
		  
		  Self.ctnListIncome.lblTopic.Text = "เงินได้"
		  Self.ctnListDeduct.lblTopic.Text = "เงินหัก"
		  Self.ctnOtherForm.ctnListBank.lblTopic.Text = "บัญชีธนาคาร"
		  Self.ctnOtherForm.ctnListAddress.lblTopic.Text = "ที่อยู่"
		  
		  Self.ctnListIncome.lstHistoryProfile.Height = 200
		  Self.ctnListDeduct.lstHistoryProfile.Height = 200
		  Self.ctnOtherForm.ctnListAddress.lstHistoryProfile.Height = 70
		  Self.ctnOtherForm.ctnListBank.lstHistoryProfile.Height = 70
		  
		  
		  Self.ctnListEducation.CategoryName = "education"
		  Self.ctnListEvent.CategoryName = "event"
		  Self.ctnListExperience.CategoryName = "experience"
		  Self.ctnListFund.CategoryName = "fund"
		  Self.ctnListHistoryPayment.CategoryName = "history-payment"
		  Self.ctnListHistoryPosition.CategoryName = "history-position"
		  Self.ctnListInsignia.CategoryName = "insignia"
		  Self.ctnListLeave.CategoryName = "leave"
		  Self.ctnListNameChange.CategoryName = "name-change"
		  Self.ctnListRelative.CategoryName = "relative"
		  Self.ctnListSkill.CategoryName = "skill"
		  Self.ctnListIncome.CategoryName = "income"
		  Self.ctnListDeduct.CategoryName = "deduct"
		  Self.ctnListDiscipline.CategoryName = "discipline"
		  Self.ctnOtherForm.ctnListAddress.CategoryName = "address"
		  Self.ctnOtherForm.ctnListBank.CategoryName = "bank"
		  
		  Self.ctnListHistoryPayment.SetDisableButton
		  Self.ctnListHistoryPosition.SetDisableButton
		  Self.ctnListInsignia.SetDisableButton
		  Self.ctnListEvent.SetDisableButton
		  Self.ctnListIncome.SetDisableButton
		  Self.ctnListDeduct.SetDisableButton
		  Self.ctnListLeave.SetDisableButton
		  Self.ctnListHistoryPosition.SetDisableButton
		  Self.ctnListHistoryPayment.SetDisableButton
		  Self.ctnListFund.SetDisableButton
		  Self.ctnListDiscipline.SetDisableButton
		  Self.ctnListInsignia.SetDisableButton
		  Self.ctnListNameChange.SetDisableButton
		  
		  Self.ctnListEvent.SetInvisibleButton
		  Self.ctnListIncome.SetInvisibleButton
		  Self.ctnListDeduct.SetInvisibleButton
		  Self.ctnListLeave.SetInvisibleButton
		  Self.ctnListHistoryPosition.SetInvisibleButton
		  Self.ctnListFund.SetInvisibleButton
		  Self.ctnListDiscipline.SetInvisibleButton
		  Self.ctnListInsignia.SetInvisibleButton
		  Self.ctnListHistoryPayment.SetInvisibleButton
		  
		  Self.OpenFirstTab
		  
		  //Set Combo Box
		  Self.SetupComponents
		  
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub ActionSegment()
		  // count down so we avoid re-evaluating the ubound all the time
		  For i As Integer = SegmentedControl1.Items.Ubound DownTo 0
		    
		    // get the reference to the segment
		    Dim s As SegmentedControlItem = SegmentedControl1.Items( i )
		    
		    //see if the segment was selected
		    If s.Selected Then
		      select case s.Title 
		      case "ประวัติ"
		        self.HideComponent
		        Self.ctnGeneralForm.Left = 20
		        Self.ctnGeneralForm.Top = 94
		        Self.ctnGeneralForm.Visible = True
		      case "การศึกษา"
		        self.HideComponent
		        Self.ctnListEducation.Left = 20
		        Self.ctnListEducation.Top = 94
		        self.ctnListEducation.Visible = True
		      case "ผู้ที่เกี่ยวข้อง"
		        self.HideComponent
		        Self.ctnListRelative.Left = 20
		        Self.ctnListRelative.Top = 94
		        self.ctnListRelative.Visible = True
		      case "ประสบการณ์ทำงาน"
		        self.HideComponent
		        Self.ctnListExperience.Left = 20
		        Self.ctnListExperience.Top = 94
		        self.ctnListExperience.Visible = True
		      case "เปลี่ยนชื่อ"
		        self.HideComponent
		        Self.ctnListNameChange.Left = 20
		        Self.ctnListNameChange.Top = 94
		        self.ctnListNameChange.Visible = True
		      case "ทักษะ"
		        self.HideComponent
		        Self.ctnListSkill.Left = 20
		        Self.ctnListSkill.Top = 94
		        self.ctnListSkill.Visible = True
		      case "อื่นๆ"
		        self.HideComponent
		        Self.ctnOtherForm.Left = 20
		        Self.ctnOtherForm.Top = 94
		        self.ctnOtherForm.Visible = True
		      end
		    End If
		  Next
		  
		  // make sure the segmented control knows to resizes its drawing boundaries or you can get weird effects
		  SegmentedControl1.SizeToFit
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub BindData()
		  Dim data As New JSONItem
		  data = Me.GetValue
		  'MsgBox data.Value("militaryService").StringValue
		  'MsgBox self.CurrentFileUpload.ToString
		  'MsgBox PersonnelWindow.cFormPersonnel.CurrentFileUpload.ToString
		  'for i as integer = 0 to PersonnelWindow.cFormPersonnel.CurrentFileUpload.Count -1
		  'MsgBox PersonnelWindow.cFormPersonnel.CurrentFileUpload.Child(i).Value(PersonnelWindow.cFormPersonnel.CurrentFileUpload.Child(i).Name(0))
		  ''Dim f as new FolderItem
		  ''Dim fileName as String
		  ''fileName = PersonnelWindow.cFormPersonnel.CurrentFileUpload.Value("")
		  ''f = f.Child("upload").Child()
		  ''Utils.WebService.UploadFile()
		  'next
		  Me.SendData(data)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ClearData()
		  Self.ctnGeneralForm.fldAssignDate.Text = ""
		  Self.ctnGeneralForm.fldDOB.Text = ""
		  Self.ctnGeneralForm.fldDueDateProbation.Text = ""
		  Self.ctnGeneralForm.fldEmpCode.Text = ""
		  Self.ctnGeneralForm.fldExpireDate.Text = ""
		  Self.ctnGeneralForm.fldFirstNameENG.Text = ""
		  Self.ctnGeneralForm.fldFirstNameTH.Text = ""
		  Self.ctnGeneralForm.fldIDCard.Text = ""
		  Self.ctnGeneralForm.fldIssueDate.Text = ""
		  Self.ctnGeneralForm.fldLastNameENG.Text = ""
		  Self.ctnGeneralForm.fldLastNameTH.Text = ""
		  Self.ctnGeneralForm.fldNickNameENG.Text = ""
		  Self.ctnGeneralForm.fldNickNameTH.Text = ""
		  Self.ctnGeneralForm.fldNumberTrainingDay.Text = ""
		  Self.ctnGeneralForm.fldResignationDate.Text = ""
		  Self.ctnGeneralForm.fldSalary.Text = ""
		  Self.ctnGeneralForm.fldStartWorkingDate.Text = ""
		  Self.ctnGeneralForm.fldStartTrainingDate.Text = ""
		  
		  Self.ctnOtherForm.fldHeight.Text = ""
		  Self.ctnOtherForm.fldScar.Text = ""
		  Self.ctnOtherForm.fldTaxRate.Text = ""
		  Self.ctnOtherForm.fldWeight.Text = ""
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetValue() As JSONItem
		  Dim data As New JSONItem
		  //filed บังคับ
		  data.value("id") = Self.CurrentID
		  data.value("organization") = PersonnelWindow.ctnListPersonnel1.SelectedOrganization
		  data.value("firstNameTh") = Self.ctnGeneralForm.fldFirstNameTH.Text
		  data.Value("lastNameTh") = Self.ctnGeneralForm.fldLastNameTH.Text
		  data.Value("nickNameTh") = Self.ctnGeneralForm.fldNickNameTH.Text
		  data.value("firstNameEng") = Self.ctnGeneralForm.fldFirstNameENG.Text
		  data.Value("lastNameEng") = Self.ctnGeneralForm.fldLastNameENG.Text
		  data.Value("nickNameEng") = Self.ctnGeneralForm.fldNickNameENG.Text
		  data.Value("identifyNo") = Self.ctnGeneralForm.fldIDCard.Text
		  data.Value("numberTrainingDay")  = Self.ctnGeneralForm.fldNumberTrainingDay.Text
		  data.Value("code") = Self.ctnGeneralForm.fldEmpCode.Text
		  data.Value("gender") = Self.ctnGeneralForm.gender
		  data.Value("resignate") = Self.ctnGeneralForm.resignate
		  if Self.ctnGeneralForm.resignate then
		    data.Value("resignateDate") = Utils.DateTools.gDataToStringForJSON(Utils.DateTools.gStringToDate(Self.ctnGeneralForm.fldResignationDate.Text))
		  else
		    data.Value("resignateDate") = ""
		  end
		  data.Value("socialSecurityInsurance") = Self.ctnGeneralForm.chkSocialSecurity.Value
		  data.Value("birthDate") = Utils.DateTools.gDataToStringForJSON(Utils.DateTools.gStringToDate(Self.ctnGeneralForm.fldDOB.Text))
		  
		  data.Value("dateIssue") = Utils.DateTools.gDataToStringForJSON(Utils.DateTools.gStringToDate(Self.ctnGeneralForm.fldIssueDate.Text))
		  data.Value("dateExpire") = Utils.DateTools.gDataToStringForJSON(Utils.DateTools.gStringToDate(Self.ctnGeneralForm.fldExpireDate.Text))
		  data.Value("startWorkingDate") = Utils.DateTools.gDataToStringForJSON(Utils.DateTools.gStringToDate(Self.ctnGeneralForm.fldStartWorkingDate.Text))
		  data.Value("personnelTypeId") = Self.ctnGeneralForm.cboTypeEmp.RowTag(Self.ctnGeneralForm.cboTypeEmp.ListIndex)
		  'data.Value("annuity") = "LANN_00001"
		  
		  
		  // filed เพิ่มเติม
		  data.Value("taxType") = Self.ctnOtherForm.cboTaxType.RowTag(Self.ctnOtherForm.cboTaxType.ListIndex)
		  data.Value("taxRate") = Self.ctnOtherForm.fldTaxRate.Text
		  data.Value("salaryType") = Self.ctnGeneralForm.cboSalaryType.RowTag(Self.ctnGeneralForm.cboSalaryType.ListIndex)
		  data.Value("nameTitle") = Self.ctnGeneralForm.cboNameTitle.RowTag(Self.ctnGeneralForm.cboNameTitle.ListIndex)
		  data.Value("marital") = Self.ctnGeneralForm.cboMarital.RowTag(Self.ctnGeneralForm.cboMarital.ListIndex)
		  data.Value("scar") = Self.ctnOtherForm.fldScar.Text
		  data.Value("weight") = Self.ctnOtherForm.fldWeight.Text
		  data.value("nationality") = Self.ctnOtherForm.cboNationality.RowTag(Self.ctnOtherForm.cboNationality.ListIndex)
		  data.value("citizenship") = Self.ctnOtherForm.cboCitizenShip.RowTag(Self.ctnOtherForm.cboCitizenShip.ListIndex)
		  data.value("height") = Self.ctnOtherForm.fldHeight.Text
		  data.Value("trainingDueDate") = Utils.DateTools.gDataToStringForJSON(Utils.DateTools.gStringToDate(Self.ctnGeneralForm.fldDueDateProbation.Text))
		  data.Value("assignWorkingDate") = Utils.DateTools.gDataToStringForJSON(Utils.DateTools.gStringToDate(Self.ctnGeneralForm.fldAssignDate.Text))
		  data.Value("religion") = Self.ctnOtherForm.cboReligion.RowTag(Self.ctnOtherForm.cboReligion.ListIndex)
		  data.Value("shiftWork") = "0"
		  data.Value("eyesColor") = Self.ctnOtherForm.cboEyeColor.RowTag(Self.ctnOtherForm.cboEyeColor.ListIndex)
		  data.Value("bloodGroup") = Self.ctnOtherForm.cboBloodGroup.RowTag(Self.ctnOtherForm.cboBloodGroup.ListIndex)
		  data.Value("militaryExempted") = Self.ctnOtherForm.cboMilitary.RowTag(Self.ctnOtherForm.cboMilitary.ListIndex)
		  data.Value("militaryService") = Self.ctnOtherForm.MilitaryCurrentStatus
		  
		  'self.fldAssignDate.Text
		  'self.fldDueDateProbation.Text
		  'self.chkProbationStatus
		  'self.fldResignationDate.Text
		  'self.cboLeaveReason.RowTag(self.cboLeaveReason.ListIndex)
		  'self.fldDOB.Text
		  'self.cboTypeEmp.RowTag(self.cboTypeEmp.ListIndex)
		  'self.cboGender.RowTag(self.cboGender.ListIndex)
		  'self.cboMarital.RowTag(self.cboMarital.ListIndex)
		  
		  'ReduceTax
		  data.Value("breakTax") = Self.ctnTaxReduceForm.getValue.ToString
		  
		  
		  '"address":"{"new":[{"registrationAddress":"reg add","currentAddress":"current add"}]}",
		  Dim addressJson As New JSONItem
		  Dim addressItem As New JSONItem
		  Dim addressArray As New JSONItem
		  Dim jsAdrressTotal As New JSONItem
		  jsAdrressTotal = Self.ctnOtherForm.ctnListAddress.GetValue("address")
		  For i As Integer = 0 To jsAdrressTotal.Count-1
		    If jsAdrressTotal.Child(i).Value("addressTypeName") = "ที่อยู่ปัจจุบัน" Then
		      addressItem.Value("currentAddress") = jsAdrressTotal.Child(i).ToString
		    Elseif  jsAdrressTotal.Child(i).Value("addressTypeName") = "ที่อยู่ตามทะเบียนบ้าน" Then
		      addressItem.Value("registrationAddress") = jsAdrressTotal.Child(i).ToString
		    End
		  Next
		  addressArray.Append(addressItem)
		  addressJson.Value("new") = addressArray
		  data.Value("address") = addressJson.ToString
		  'MsgBox addressJson.ToString
		  
		  //GetData From tab panels
		  data.value("education") = Self.ctnListEducation.GetValue("education").ToString
		  data.value("relative") = Self.ctnListRelative.GetValue("relative").ToString
		  data.value("skill") = Self.ctnListSkill.GetValue("skill").ToString
		  data.value("workExperience") = Self.ctnListExperience.GetValue("experience").ToString
		  data.Value("bank") = Self.ctnOtherForm.ctnListBank.GetValue("bank").ToString
		  
		  
		  //income Deduct
		  'Dim incomeDeductJson As New JSONItem
		  'Dim incomeDeductNew As New JSONItem
		  'Dim incomeDeductUpdate As New JSONItem
		  'Dim incomeDeductDelete As New JSONItem
		  'Dim incomeDeductTemp As New JSONItem
		  'Dim incomeDeductItem As New JSONItem
		  'incomeDeductTemp = Self.ctnListIncome.GetValue("income")
		  '
		  'incomeDeductItem = incomeDeductTemp
		  'If incomeDeductItem.HasName("new") Then
		  'incomeDeductItem = incomeDeductTemp.Value("new")
		  'If incomeDeductItem.Count > 0 Then
		  'For i As Integer =0 To incomeDeductItem.Count-1
		  'incomeDeductNew.Append(incomeDeductItem.Child(i))
		  'Next
		  'End
		  'End
		  '
		  'incomeDeductItem = incomeDeductTemp
		  'If incomeDeductItem.HasName("update") Then
		  'incomeDeductItem = incomeDeductTemp.Value("update")
		  'If incomeDeductItem.Count > 0 Then
		  'For i As Integer =0 To incomeDeductItem.Count-1
		  'incomeDeductUpdate.Append(incomeDeductItem.Child(i))
		  'Next
		  'End
		  'End
		  '
		  'incomeDeductItem = incomeDeductTemp
		  'If incomeDeductItem.HasName("delete") Then
		  'incomeDeductItem = incomeDeductTemp.Value("delete")
		  'If incomeDeductItem.Count > 0 Then
		  'For i As Integer =0 To incomeDeductItem.Count-1
		  'incomeDeductDelete.Append(incomeDeductItem.Child(i))
		  'Next
		  'End
		  'End
		  '
		  'incomeDeductTemp = Self.ctnListIncome.GetValue("deduct")
		  '
		  'incomeDeductItem = incomeDeductTemp
		  'If incomeDeductItem.HasName("new") Then
		  'incomeDeductItem = incomeDeductTemp.Value("new")
		  'If incomeDeductItem.Count > 0 Then
		  'For i As Integer =0 To incomeDeductItem.Count-1
		  'incomeDeductNew.Append(incomeDeductItem.Child(i))
		  'Next
		  'End
		  'End
		  '
		  'incomeDeductItem = incomeDeductTemp
		  'If incomeDeductItem.HasName("update") Then
		  'incomeDeductItem = incomeDeductTemp.Value("update")
		  'If incomeDeductItem.Count > 0 Then
		  'For i As Integer =0 To incomeDeductItem.Count-1
		  'incomeDeductUpdate.Append(incomeDeductItem.Child(i))
		  'Next
		  'End
		  'End
		  '
		  'incomeDeductItem = incomeDeductTemp
		  'If incomeDeductItem.HasName("delete") Then
		  'incomeDeductItem = incomeDeductTemp.Value("delete")
		  'If incomeDeductItem.Count > 0 Then
		  'For i As Integer =0 To incomeDeductItem.Count-1
		  'incomeDeductDelete.Append(incomeDeductItem.Child(i))
		  'Next
		  'End
		  'End
		  '
		  'If incomeDeductNew.Count > 0 Then
		  'incomeDeductJson.Value("new") = incomeDeductNew
		  'End
		  'If incomeDeductUpdate.Count > 0 Then
		  'incomeDeductJson.Value("update") = incomeDeductUpdate
		  'End
		  'If incomeDeductDelete.Count > 0 Then
		  'incomeDeductJson.Value("delete") = incomeDeductDelete
		  'End
		  
		  'MsgBox incomeDeductJson.ToString
		  'MsgBox Self.ctnListExperience.GetValue("experience").ToString
		  'Self.ctnListFund.GetValue("fund").ToString
		  'self.ctnListLeave.GetValue("leave").ToString
		  'self.ctnListNameChange.GetValue("name-change").ToString
		  'MsgBox self.ctnListSkill.GetValue("skill").ToString
		  
		  //สร้าง JsonItem แล้ว append เข้า JsonItem อีกทีเพื่อทำเป็น array จากนั้นถึงจะ assign ให้เท่ากับ Key หลักที่ต้องการ
		  //"position":{"new":"[{\"positionId\":\"LPOS_00007\",\"salary\":\"35000\"}]"
		  Dim posJson As New JSONItem
		  Dim posItem As New JSONItem
		  Dim posArray As New JSONItem
		  posItem.Value("positionId") = Self.ctnGeneralForm.cboCurrentPosition.RowTag(Self.ctnGeneralForm.cboCurrentPosition.ListIndex)
		  posItem.Value("mainPosition") = True
		  posItem.Value("salary") = Self.ctnGeneralForm.fldSalary.Text
		  posArray.Append(posItem)
		  posJson.Value("new") = posArray
		  data.Value("position") =  posJson.ToString
		  
		  
		  
		  '''"bank":"{"new":[{"bankId":"LBANK_00001","bankBranchNo":"","accountNo":"11111111","accountName":""}]}",
		  'Dim bankJson As New JSONItem
		  'Dim bankItem As New JSONItem
		  'Dim bankArray As New JSONItem
		  'bankItem.Value("bankId") = "LBANK_00001"
		  'bankItem.Value("bankBranchNo") = "001"
		  'bankItem.Value("accountNo") = "989898989898"
		  'bankItem.Value("accountName") = "Tongdee"
		  'bankArray.Append(bankItem)
		  'bankJson.value("new") = bankArray
		  'data.value("bank") = bankJson.ToString
		  '
		  ''"incomeDeduct":"{"new":[{"incomeDeductTypeId":"LINDET_00002","amount":"3500","remark":""}]}"
		  'Dim incomeJson As New JSONItem
		  'Dim incomeItem As New JSONItem
		  'Dim incomeArray As New JSONItem
		  'incomeItem.value("incomeDeductTypeId") = "LINDET_00002"
		  'incomeItem.value("amount") = "35000"
		  'incomeItem.value("remark") = ""
		  'incomeItem.value("personelId") = ""
		  'incomeArray.Append(incomeItem)
		  'incomeJson.Value("new") = incomeArray
		  'data.Value("incomeDeduct") = incomeJson.ToString
		  '
		  ''Education
		  'Dim educationJson As New JSONItem
		  'Dim educationItem As New JSONItem
		  'Dim educationArray As New JSONItem
		  'educationItem.value("major") = "LMAJ_00011"
		  ''educationItem.value("institution") = "Science"
		  ''educationItem.value("faculty") = "Science"
		  'educationItem.value("certificate") = "LCER_00011"
		  'educationItem.value("gpa") = "3.5"
		  'educationItem.value("country") = "ประเทศไทย"
		  'educationItem.value("startYear") = "09/09/1989"
		  'educationItem.value("endYear") = "09/09/1992"
		  'educationArray.Append(educationItem)
		  'educationJson.Value("new") = educationArray
		  'data.Value("education") = educationJson.ToString
		  
		  'Relative
		  'Dim relativeJson As New JSONItem
		  'Dim relativeItem As New JSONItem
		  'Dim relativeArray As New JSONItem
		  'relativeItem.value("major") = "LINDET_00002"
		  'relativeItem.value("institution") = "35000"
		  'relativeArray.Append(relativeItem)
		  'relativeJson.Value("new") = relativeArray
		  'data.Value("relative") = relativeJson.ToString
		  
		  'Eventเหตุการณ์
		  'Dim eventJson As New JSONItem
		  'Dim eventItem As New JSONItem
		  'Dim eventArray As New JSONItem
		  'eventItem.value("major") = "LINDET_00002"
		  'eventItem.value("institution") = "35000"
		  'eventArray.Append(eventItem)
		  'eventJson.Value("new") = eventArray
		  'data.Value("event") = eventJson.ToString
		  
		  'work Experience
		  'Dim expJson As New JSONItem
		  'Dim expItem As New JSONItem
		  'Dim expArray As New JSONItem
		  'expItem.value("major") = "LINDET_00002"
		  'expItem.value("institution") = "35000"
		  'expArray.Append(expItem)
		  'expJson.Value("new") = expArray
		  'data.Value("experience") = expJson.ToString
		  
		  'fund
		  'Dim fundJson As New JSONItem
		  'Dim fundItem As New JSONItem
		  'Dim fundArray As New JSONItem
		  'fundItem.value("major") = "LINDET_00002"
		  'fundItem.value("institution") = "35000"
		  'fundArray.Append(fundItem)
		  'fundJson.Value("new") = fundArray
		  'data.Value("fund") = fundJson.ToString
		  
		  'leaving
		  'Dim leaveJson As New JSONItem
		  'Dim leaveItem As New JSONItem
		  'Dim leaveArray As New JSONItem
		  'leaveItem.value("major") = "LINDET_00002"
		  'leaveItem.value("institution") = "35000"
		  'leaveArray.Append(leaveItem)
		  'leaveJson.Value("new") = leaveArray
		  'data.Value("leave") = leaveJson.ToString
		  
		  'naming-Change
		  'Dim ncJson As New JSONItem
		  'Dim ncItem As New JSONItem
		  'Dim ncArray As New JSONItem
		  'ncItem.value("major") = "LINDET_00002"
		  'ncItem.value("institution") = "35000"
		  'ncArray.Append(ncItem)
		  'ncJson.Value("new") = ncArray
		  'data.Value("nc") = ncJson.ToString
		  
		  'ประวัติการรับเงิน
		  'Dim hmJson As New JSONItem
		  'Dim hmItem As New JSONItem
		  'Dim hmArray As New JSONItem
		  'hmItem.value("major") = "LINDET_00002"
		  'hmItem.value("institution") = "35000"
		  'hmArray.Append(hmItem)
		  'hmJson.Value("new") = hmArray
		  'data.Value("hm") = hmJson.ToString
		  
		  'history position
		  'Dim hpJson As New JSONItem
		  'Dim hpItem As New JSONItem
		  'Dim hpArray As New JSONItem
		  'hpItem.value("major") = "LINDET_00002"
		  'hpItem.value("institution") = "35000"
		  'hpArray.Append(hpItem)
		  'hpJson.Value("new") = hpArray
		  'data.Value("hp") = hpJson.ToString
		  
		  'skill
		  'Dim skillJson As New JSONItem
		  'Dim skillItem As New JSONItem
		  'Dim skillArray As New JSONItem
		  'skillItem.value("major") = "LINDET_00002"
		  'skillItem.value("institution") = "35000"
		  'skillArray.Append(skillItem)
		  'skillJson.Value("new") = skillArray
		  'data.Value("skill") = skillJson.ToString
		  
		  'insignia
		  'Dim insigniaJson As New JSONItem
		  'Dim insigniaItem As New JSONItem
		  'Dim insigniaArray As New JSONItem
		  'insigniaItem.value("major") = "LINDET_00002"
		  'insigniaItem.value("institution") = "35000"
		  'insigniaArray.Append(insigniaItem)
		  'insigniaJson.Value("new") = insigniaArray
		  'data.Value("insignia") = insigniaJson.ToString
		  
		  Return data
		  
		  Exception err As KeyNotFoundException
		    'Exception err as OutOfBoundsException
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub HideComponent()
		  Self.ctnGeneralForm.Visible = False
		  Self.ctnListEducation.Visible = False
		  Self.ctnListRelative.Visible = False
		  Self.ctnListExperience.Visible = False
		  Self.ctnListNameChange.Visible = False
		  Self.ctnListSkill.Visible = False
		  self.ctnOtherForm.Visible = False
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ImportPerson()
		  // open zip archive
		  Dim f As FolderItem=GetOpenFolderItem("zip")
		  Dim z As New UnZipMBS(f)
		  
		  if f <> nil then
		    
		    // let's start
		    z.GoToFirstFile
		    Do
		      // get details on this file:
		      Dim info As UnZipFileInfoMBS = z.FileInfo
		      Dim name As String = z.FileName
		      
		      If Left(name,8) <> "__MACOSX" Then // ignore Mac special files for metadata
		        z.OpenCurrentFile
		        If z.Lasterror=0 Then
		          // create output file (if you want to support folders, this needs to be changed. See other examples)
		          Dim outfile As folderitem = GetFolderItem(Name)
		          Dim b As BinaryStream = BinaryStream.Create(outfile, True)
		          Dim s As String
		          Dim jsTemp As New JSONItem
		          // now read 100 KB chunks and write them to new file
		          Do
		            s=z.ReadCurrentFile(100000)
		            If Right(name,4) = "json" Then
		              jsTemp = New JSONItem(s)
		              Self.SetProfileWithJson(jsTemp)
		            End
		          Loop Until LenB(s)>0
		          // cleanup
		          b.Close
		          z.CloseCurrentFile
		        End If
		        
		      End If
		      // move to next file until we reach the end
		      z.GoToNextFile
		    Loop Until z.Lasterror<>0
		  else
		    PersonnelWindow.ctnListPersonnel1.lstPersonnel.ListIndex = 0
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub NewForm()
		  Me.CurrentID = ""
		  Me.ctnGeneralForm.Visible = True
		  Me.ctnGeneralForm.cboCurrentPosition.ListIndex = 0
		  Me.ClearData
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub OpenFirstTab()
		  Self.ctnGeneralForm.Left = 20
		  Self.ctnGeneralForm.Top = 94
		  Self.ctnGeneralForm.Visible = True
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SendData(formData as JSONItem)
		  'MsgBox formData.ToString
		  Dim pInstance As New Repo.WS.Personnel
		  If formData.Value("id") = "" Then
		    If pInstance.Save(formData) Then
		      MsgBox "เพิ่มข้อมูลสำเร็จ"
		    Else
		      MsgBox "เพิ่มข้อมูลไม่สำเร็จ"
		    End
		  Else
		    If pInstance.Update(formData) Then
		      MsgBox "แก้ไขข้อมูลสำเร็จ"
		    Else
		      MsgBox "แก้ไขข้อมูลไม่สำเร็จ"
		    End
		  End
		  
		  PersonnelWindow.ctnListPersonnel1.LoadLists
		  PersonnelWindow.PagePanel1.Value = 1
		  PersonnelWindow.ctnListPersonnel1.lstPersonnel.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetActingPosition()
		  'Dim pos As New Repo.WS.Position
		  'Dim js As String
		  'Dim jsData As New JSONItem
		  'Try
		  'js = pos.ListAll
		  'jsData = New JSONItem(js)
		  'If jsData.Value("status") = True Then
		  'Dim objData As JSONItem = jsData.Value("data")
		  'Me.ctnGeneralForm.cboActing.AddRow("กรุณาเลือกข้อมูล")
		  'Me.ctnGeneralForm.cboActing.RowTag(0) = ""
		  'For i As Integer = 0 To objData.Count-1
		  'Dim iItem As JSONItem = objData.Child(i)
		  'Me.ctnGeneralForm.cboActing.AddRow(iItem.Value("positionNameTh"))
		  'Me.ctnGeneralForm.cboActing.RowTag(i+1) = iItem.Value("id")
		  'Next
		  'End
		  'Catch err As KeyNotFoundException
		  'End
		  '
		  'Me.ctnGeneralForm.cboActing.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetBloodGroup()
		  Dim bg As New Repo.WS.BloodGroups
		  Dim js As String
		  Dim jsData As New JSONItem
		  Try
		    js = bg.ListAll
		    jsData = New JSONItem(js)
		    If jsData.Value("status") = True Then
		      Dim objData As JSONItem = jsData.Value("data")
		      Me.ctnOtherForm.cboBloodGroup.AddRow("กรุณาเลือกข้อมูล")
		      Me.ctnOtherForm.cboBloodGroup.RowTag(0) = ""
		      For i As Integer = 0 To objData.Count-1
		        Dim iItem As JSONItem = objData.Child(i)
		        Me.ctnOtherForm.cboBloodGroup.AddRow(iItem.Value("name"))
		        Me.ctnOtherForm.cboBloodGroup.RowTag(i+1) = iItem.Value("id")
		      Next
		    End
		  Catch err As KeyNotFoundException
		  End
		  
		  Me.ctnOtherForm.cboBloodGroup.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetCitizenShip()
		  Dim cs As New Repo.WS.CitizenShip
		  Dim js As String
		  Dim jsData As New JSONItem
		  Try
		    js = cs.ListAll
		    jsData = New JSONItem(js)
		    If jsData.Value("status") = True Then
		      Dim objData As JSONItem = jsData.Value("data")
		      Me.ctnOtherForm.cboCitizenShip.AddRow("กรุณาเลือกข้อมูล")
		      Me.ctnOtherForm.cboCitizenShip.RowTag(0) = ""
		      For i As Integer = 0 To objData.Count-1
		        Dim iItem As JSONItem = objData.Child(i)
		        Me.ctnOtherForm.cboCitizenShip.AddRow(iItem.Value("name"))
		        Me.ctnOtherForm.cboCitizenShip.RowTag(i+1) = iItem.Value("id")
		      Next
		    End
		  Catch err As KeyNotFoundException
		  End
		  
		  Me.ctnOtherForm.cboCitizenShip.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetCurrentPosition()
		  Dim pos As New Repo.WS.Position
		  Dim js As String
		  Dim jsData As New JSONItem
		  Try
		    js = pos.ListAll
		    jsData = New JSONItem(js)
		    If jsData.Value("status") = True Then
		      Dim objData As JSONItem = jsData.Value("data")
		      Me.ctnGeneralForm.cboCurrentPosition.AddRow("กรุณาเลือกข้อมูล")
		      Me.ctnGeneralForm.cboCurrentPosition.RowTag(0) = ""
		      For i As Integer = 0 To objData.Count-1
		        Dim iItem As JSONItem = objData.Child(i)
		        Me.ctnGeneralForm.cboCurrentPosition.AddRow(iItem.Value("positionNameTh"))
		        Me.ctnGeneralForm.cboCurrentPosition.RowTag(i+1) = iItem.Value("id")
		      Next
		    End
		  Catch err As KeyNotFoundException
		  End
		  
		  Me.ctnGeneralForm.cboCurrentPosition.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetEyeColor()
		  Dim eye As New Repo.WS.EyeColor
		  Dim js As String
		  Dim jsData As New JSONItem
		  Try
		    js = eye.ListAll
		    jsData = New JSONItem(js)
		    If jsData.Value("status") = True Then
		      Dim objData As JSONItem = jsData.Value("data")
		      Me.ctnOtherForm.cboEyeColor.AddRow("กรุณาเลือกข้อมูล")
		      Me.ctnOtherForm.cboEyeColor.RowTag(0) = ""
		      For i As Integer = 0 To objData.Count-1
		        Dim iItem As JSONItem = objData.Child(i)
		        Me.ctnOtherForm.cboEyeColor.AddRow(iItem.Value("name"))
		        Me.ctnOtherForm.cboEyeColor.RowTag(i+1) = iItem.Value("id")
		      Next
		    End
		  Catch err As KeyNotFoundException
		  End
		  
		  Me.ctnOtherForm.cboEyeColor.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetGender()
		  Self.ctnGeneralForm.cboGender.AddRow("ชาย")
		  Self.ctnGeneralForm.cboGender.RowTag(0) = "male"
		  Self.ctnGeneralForm.cboGender.AddRow("หญิง")
		  Self.ctnGeneralForm.cboGender.RowTag(1) = "female"
		  
		  Self.ctnGeneralForm.cboGender.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetID(id as String)
		  'Dim pInstance As New Repo.WS.Personnel
		  Self.CurrentID = id
		  Self.SetValue
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetMaritalStatus()
		  Dim ms As New Repo.WS.MaritalStatus
		  Dim js As String
		  Dim jsData As New JSONItem
		  Try
		    js = ms.ListAll
		    jsData = New JSONItem(js)
		    If jsData.Value("status") = True Then
		      Dim objData As JSONItem = jsData.Value("data")
		      Me.ctnGeneralForm.cboMarital.AddRow("กรุณาเลือกข้อมูล")
		      Me.ctnGeneralForm.cboMarital.RowTag(0) = ""
		      For i As Integer = 0 To objData.Count-1
		        Dim iItem As JSONItem = objData.Child(i)
		        Me.ctnGeneralForm.cboMarital.AddRow(iItem.Value("name"))
		        Me.ctnGeneralForm.cboMarital.RowTag(i+1) = iItem.Value("id")
		      Next
		    End
		  Catch err As KeyNotFoundException
		  End
		  
		  Me.ctnGeneralForm.cboMarital.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetMilitary()
		  Self.ctnOtherForm.cboMilitary.AddRow("กรุณเลือกข้อมูล")
		  Self.ctnOtherForm.cboMilitary.AddRow("ปลดเป็นกองหนุน")
		  Self.ctnOtherForm.cboMilitary.RowTag(0) = "0"
		  Self.ctnOtherForm.cboMilitary.AddRow("จับได้ใบดำ")
		  Self.ctnOtherForm.cboMilitary.RowTag(1) = "1"
		  Self.ctnOtherForm.cboMilitary.AddRow("ร่างกายไม่ได้คุณลักษณะ")
		  Self.ctnOtherForm.cboMilitary.RowTag(2) = "2"
		  
		  Self.ctnOtherForm.cboMilitary.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetNameTitle()
		  Dim nti As New Repo.WS.NameTitle
		  Dim js As String
		  Dim jsData As New JSONItem
		  Try
		    js = nti.ListAll
		    jsData = New JSONItem(js)
		    If jsData.Value("status") = True Then
		      Dim objData As JSONItem = jsData.Value("data")
		      Me.ctnGeneralForm.cboNameTitle.AddRow("กรุณาเลือกข้อมูล")
		      Me.ctnGeneralForm.cboNameTitle.RowTag(0) = ""
		      For i As Integer = 0 To objData.Count-1
		        Dim iItem As JSONItem = objData.Child(i)
		        Me.ctnGeneralForm.cboNameTitle.AddRow(iItem.Value("nameTh"))
		        Me.ctnGeneralForm.cboNameTitle.RowTag(i+1) = iItem.Value("id")
		      Next
		    End
		  Catch err As KeyNotFoundException
		  End
		  
		  Me.ctnGeneralForm.cboNameTitle.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetNationality()
		  Dim nt As New Repo.WS.Nationality
		  Dim js As String
		  Dim jsData As New JSONItem
		  Try
		    js = nt.ListAll
		    jsData = New JSONItem(js)
		    If jsData.Value("status") = True Then
		      Dim objData As JSONItem = jsData.Value("data")
		      Me.ctnOtherForm.cboNationality.AddRow("กรุณาเลือกข้อมูล")
		      Me.ctnOtherForm.cboNationality.RowTag(0) = ""
		      For i As Integer = 0 To objData.Count-1
		        Dim iItem As JSONItem = objData.Child(i)
		        Me.ctnOtherForm.cboNationality.AddRow(iItem.Value("name"))
		        Me.ctnOtherForm.cboNationality.RowTag(i+1) = iItem.Value("id")
		      Next
		    End
		  Catch err As KeyNotFoundException
		  End
		  
		  Me.ctnOtherForm.cboNationality.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetPersonnelType()
		  Dim pt As New Repo.WS.PersonnelType
		  Dim js As String
		  Dim jsData As New JSONItem
		  Try
		    js = pt.ListAll
		    jsData = New JSONItem(js)
		    If jsData.Value("status") = True Then
		      Dim objData As JSONItem = jsData.Value("data")
		      Me.ctnGeneralForm.cboTypeEmp.AddRow("กรุณาเลือกข้อมูล")
		      Me.ctnGeneralForm.cboTypeEmp.RowTag(0) = ""
		      For i As Integer = 0 To objData.Count-1
		        Dim iItem As JSONItem = objData.Child(i)
		        Me.ctnGeneralForm.cboTypeEmp.AddRow(iItem.Value("name"))
		        Me.ctnGeneralForm.cboTypeEmp.RowTag(i+1) = iItem.Value("id")
		      Next
		    End
		  Catch err As KeyNotFoundException
		  End
		  
		  Me.ctnGeneralForm.cboTypeEmp.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetProfileWithJson(data As JSONItem)
		  'MsgBox data.ToString
		  self.ctnGeneralForm.fldFirstNameTH.text = data.Value("firstname_th")
		  self.ctnGeneralForm.fldLastNameTH.text = data.Value("lastname_th")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetReligion()
		  Dim rg As New Repo.WS.Religion
		  Dim js As String
		  Dim jsData As New JSONItem
		  Try
		    js = rg.ListAll
		    jsData = New JSONItem(js)
		    If jsData.Value("status") = True Then
		      Dim objData As JSONItem = jsData.Value("data")
		      Me.ctnOtherForm.cboReligion.AddRow("กรุณาเลือกข้อมูล")
		      Me.ctnOtherForm.cboReligion.RowTag(0) = ""
		      For i As Integer = 0 To objData.Count-1
		        Dim iItem As JSONItem = objData.Child(i)
		        Me.ctnOtherForm.cboReligion.AddRow(iItem.Value("name"))
		        Me.ctnOtherForm.cboReligion.RowTag(i+1) = iItem.Value("id")
		      Next
		    End
		  Catch err As KeyNotFoundException
		  End
		  
		  Me.ctnOtherForm.cboReligion.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetSalaryType()
		  Dim st As New Repo.WS.SalaryType
		  Dim js As String
		  Dim jsData As New JSONItem
		  Try
		    js = st.ListAll
		    jsData = New JSONItem(js)
		    If jsData.Value("status") = True Then
		      Dim objData As JSONItem = jsData.Value("data")
		      Me.ctnGeneralForm.cboSalaryType.AddRow("กรุณาเลือกข้อมูล")
		      Me.ctnGeneralForm.cboSalaryType.RowTag(0) = ""
		      For i As Integer = 0 To objData.Count-1
		        Dim iItem As JSONItem = objData.Child(i)
		        Me.ctnGeneralForm.cboSalaryType.AddRow(iItem.Value("name"))
		        Me.ctnGeneralForm.cboSalaryType.RowTag(i+1) = iItem.Value("id")
		      Next
		    End
		  Catch err As KeyNotFoundException
		  End
		  
		  Me.ctnGeneralForm.cboSalaryType.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetTaxType()
		  Dim tt As New Repo.WS.TaxType
		  Dim js As String
		  Dim jsData As New JSONItem
		  Try
		    js = tt.ListAll
		    jsData = New JSONItem(js)
		    If jsData.Value("status") = True Then
		      Dim objData As JSONItem = jsData.Value("data")
		      Me.ctnOtherForm.cboTaxType.AddRow("กรุณาเลือกข้อมูล")
		      Me.ctnOtherForm.cboTaxType.RowTag(0) = ""
		      For i As Integer = 0 To objData.Count-1
		        Dim iItem As JSONItem = objData.Child(i)
		        Me.ctnOtherForm.cboTaxType.AddRow(iItem.Value("name"))
		        Me.ctnOtherForm.cboTaxType.RowTag(i+1) = iItem.Value("id")
		      Next
		    End
		  Catch err As KeyNotFoundException
		  End
		  
		  Me.ctnOtherForm.cboTaxType.ListIndex = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetupComponents()
		  Self.SetCurrentPosition
		  Self.SetActingPosition
		  Self.SetBloodGroup
		  Self.SetCitizenShip
		  Self.SetMilitary
		  Self.SetNationality
		  Self.SetReligion
		  Self.SetMaritalStatus
		  Self.SetNameTitle
		  Self.SetSalaryType
		  Self.SetTaxType
		  Self.SetPersonnelType
		  Self.SetGender
		  Self.SetEyeColor
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetValue()
		  Dim pInstance as new Repo.WS.Personnel
		  Dim jsPerson as new JSONItem
		  jsPerson = pInstance.GetByID(self.CurrentID)
		  
		  PersonnelWindow.ctnContent1.lblTitle.Text = "แก้ไขประวัติพนักงาน : คุณ  "+jsPerson.Value("firstNameTh")+" "+jsPerson.Value("lastNameTh")
		  
		  me.ctnGeneralForm.fldFirstNameTH.Text = jsPerson.Value("firstNameTh")
		  me.ctnGeneralForm.fldLastNameTH.Text = jsPerson.Value("lastNameTh")
		  me.ctnGeneralForm.fldFirstNameENG.Text = jsPerson.Value("firstNameEng")
		  me.ctnGeneralForm.fldLastNameENG.Text = jsPerson.Value("lastNameEng")
		  'me.dataEmployeeID.Text = jsPerson.Value("id")
		  'me.dataFirstNameTH.Text = jsPerson.Value("firstNameTh")
		  'me.dataLastNameTH.Text = jsPerson.Value("lastNameTh")
		  'me.dataNickNameTH.Text = jsPerson.Value("nickNameTh")
		  'me.dataFirstNameENG.Text = jsPerson.Value("firstNameEng")
		  'me.dataLastNameENG.Text = jsPerson.Value("lastNameEng")
		  'me.dataNickNamENG.text = jsPerson.Value("nickNameEng")
		  'me.dataDOB.text = jsPerson.Value("birthDate")
		  'me.dataGender.text = jsPerson.Value("gender")
		  'me.dataIDCard.text = jsPerson.Value("identifyNo")
		  'me.dataIssueDate.text = jsPerson.Value("dateIssue")
		  'me.dataExpireDate.text = jsPerson.Value("dateExpire")
		  ''me.dataTypeOfEmployee.text
		  ''me.dataWorkingStatus.text 
		  ''me.dataLeaveDate.Text 
		  ''me.dataLeaveReason.Text
		  'me.dataStartWorkingDate.Text = jsPerson.Value("startWorkingDate")
		  'me.dataStartDate.text = jsPerson.Value("assignWorkingDate")
		  
		  'MsgBox jsPerson.ToString
		  
		  'Dim jsBreakTax as new JSONItem
		  'jsBreakTax = jsPerson.Value("breakTax")
		  'if jsBreakTax <> Nil then
		  'self.ctnTaxReduceForm.setValue(jsBreakTax)
		  'end
		  
		  
		  
		  Dim jsEducation AS new JSONItem
		  jsEducation = jsPerson.Value("educations")
		  me.ctnListEducation.lstHistoryProfile.DeleteAllRows
		  if jsEducation.ToString <> "" then
		    for i as integer = 0 to jsEducation.Count-1
		      me.ctnListEducation.lstHistoryProfile.AddRow
		      me.ctnListEducation.lstHistoryProfile.Cell(me.ctnListEducation.lstHistoryProfile.LastIndex,0) = me.ctnListEducation.lstHistoryProfile.ListCount.ToText
		      me.ctnListEducation.lstHistoryProfile.CellType(me.ctnListEducation.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      me.ctnListEducation.lstHistoryProfile.Cell(me.ctnListEducation.lstHistoryProfile.LastIndex,1) = jsEducation.Child(i).Value("certificateName")
		      me.ctnListEducation.lstHistoryProfile.CellTag(me.ctnListEducation.lstHistoryProfile.LastIndex,1) = jsEducation.Child(i).Value("certificateId")
		      me.ctnListEducation.lstHistoryProfile.Cell(me.ctnListEducation.lstHistoryProfile.LastIndex,2) = jsEducation.Child(i).Value("institutionName")
		      me.ctnListEducation.lstHistoryProfile.CellTag(me.ctnListEducation.lstHistoryProfile.LastIndex,2) = jsEducation.Child(i).Value("institutionId")
		      me.ctnListEducation.lstHistoryProfile.Cell(me.ctnListEducation.lstHistoryProfile.LastIndex,3) = jsEducation.Child(i).Value("facultyName")
		      me.ctnListEducation.lstHistoryProfile.CellTag(me.ctnListEducation.lstHistoryProfile.LastIndex,3) = jsEducation.Child(i).Value("facultyId")
		      me.ctnListEducation.lstHistoryProfile.Cell(me.ctnListEducation.lstHistoryProfile.LastIndex,4) = jsEducation.Child(i).Value("majorName")
		      me.ctnListEducation.lstHistoryProfile.CellTag(me.ctnListEducation.lstHistoryProfile.LastIndex,4) = jsEducation.Child(i).Value("majorId")
		      me.ctnListEducation.lstHistoryProfile.Cell(me.ctnListEducation.lstHistoryProfile.LastIndex,5) = jsEducation.Child(i).Value("gpa")
		      me.ctnListEducation.lstHistoryProfile.Cell(me.ctnListEducation.lstHistoryProfile.LastIndex,6) = jsEducation.Child(i).Value("startYear")
		      me.ctnListEducation.lstHistoryProfile.Cell(me.ctnListEducation.lstHistoryProfile.LastIndex,7) = jsEducation.Child(i).Value("endYear")
		      me.ctnListEducation.lstHistoryProfile.Cell(me.ctnListEducation.lstHistoryProfile.LastIndex,8) = jsEducation.Child(i).Value("country")
		      me.ctnListEducation.lstHistoryProfile.Cell(me.ctnListEducation.lstHistoryProfile.LastIndex,9) = jsEducation.Child(i).Value("remark")
		      me.ctnListEducation.lstHistoryProfile.RowTag(me.ctnListEducation.lstHistoryProfile.LastIndex) = me.ctnListEducation.lstHistoryProfile.LastIndex
		      me.ctnListEducation.lstHistoryProfile.CellTag(me.ctnListEducation.lstHistoryProfile.LastIndex,0) = jsEducation.Child(i).Value("id")
		    next
		  end
		  
		  Dim sRelative as String = jsPerson.Value("relative")
		  Dim jsRelative as new JSONItem(sRelative)
		  if jsRelative.ToString <> "" then
		    me.ctnListRelative.lstHistoryProfile.DeleteAllRows
		    for i as integer = 0 to jsRelative.Count-1
		      me.ctnListRelative.lstHistoryProfile.AddRow
		      me.ctnListRelative.lstHistoryProfile.Cell(me.ctnListRelative.lstHistoryProfile.LastIndex,0) = me.ctnListRelative.lstHistoryProfile.ListCount.ToText
		      me.ctnListRelative.lstHistoryProfile.CellType(me.ctnListRelative.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      me.ctnListRelative.lstHistoryProfile.Cell(me.ctnListRelative.lstHistoryProfile.LastIndex,1) = jsRelative.Child(i).Value("nameTitleName")
		      me.ctnListRelative.lstHistoryProfile.CellTag(me.ctnListRelative.lstHistoryProfile.LastIndex,1) = jsRelative.Child(i).Value("nameTitle")
		      me.ctnListRelative.lstHistoryProfile.Cell(me.ctnListRelative.lstHistoryProfile.LastIndex,2) = jsRelative.Child(i).Value("firstName")
		      me.ctnListRelative.lstHistoryProfile.Cell(me.ctnListRelative.lstHistoryProfile.LastIndex,3) = jsRelative.Child(i).Value("lastName")
		      me.ctnListRelative.lstHistoryProfile.Cell(me.ctnListRelative.lstHistoryProfile.LastIndex,4) = jsRelative.Child(i).Value("gender")
		      me.ctnListRelative.lstHistoryProfile.Cell(me.ctnListRelative.lstHistoryProfile.LastIndex,5) = jsRelative.Child(i).Value("relationship")
		      me.ctnListRelative.lstHistoryProfile.Cell(me.ctnListRelative.lstHistoryProfile.LastIndex,6) = jsRelative.Child(i).Value("identificationNo")
		      me.ctnListRelative.lstHistoryProfile.Cell(me.ctnListRelative.lstHistoryProfile.LastIndex,7) = jsRelative.Child(i).Value("job")
		      me.ctnListRelative.lstHistoryProfile.Cell(me.ctnListRelative.lstHistoryProfile.LastIndex,8) = jsRelative.Child(i).Value("DOB")
		      me.ctnListRelative.lstHistoryProfile.Cell(me.ctnListRelative.lstHistoryProfile.LastIndex,9) = jsRelative.Child(i).Value("status")
		      me.ctnListRelative.lstHistoryProfile.Cell(me.ctnListRelative.lstHistoryProfile.LastIndex,10) = jsRelative.Child(i).Value("deathDate")
		      me.ctnListRelative.lstHistoryProfile.Cell(me.ctnListRelative.lstHistoryProfile.LastIndex,11) = jsRelative.Child(i).Value("phoneNo")
		      me.ctnListRelative.lstHistoryProfile.Cell(me.ctnListRelative.lstHistoryProfile.LastIndex,12) = jsRelative.Child(i).Value("remark")
		    next
		  end
		  
		  Dim sAddress as String = jsPerson.Value("address")
		  Dim jsAddress as new JSONItem(sAddress)
		  if jsAddress.ToString <> "" then
		    me.ctnOtherForm.ctnListAddress.lstHistoryProfile.DeleteAllRows
		    for i as integer = 0 to jsAddress.Count-1
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.AddRow()
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.Cell(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,0) = me.ctnOtherForm.ctnListAddress.lstHistoryProfile.ListCount.ToText
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.CellType(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.Cell(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,1) = jsAddress.child(i).Value("addressTypeName")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.CellTag(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,1) = jsAddress.child(i).Value("addressType")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.Cell(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,2) = jsAddress.child(i).Value("houseNo")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.Cell(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,3) = jsAddress.child(i).Value("provinceName")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.CellTag(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,3) = jsAddress.child(i).Value("province")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.Cell(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,4) = jsAddress.child(i).Value("districtName")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.CellTag(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,4) = jsAddress.child(i).Value("district")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.Cell(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,5) = jsAddress.child(i).Value("subdistrictName")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.CellTag(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,5) = jsAddress.child(i).Value("subdistrict")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.Cell(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,6) = jsAddress.child(i).Value("zipcodeName")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.CellTag(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,6) = jsAddress.child(i).Value("zipcode")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.Cell(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,7) = jsAddress.child(i).Value("homePhone")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.Cell(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,8) = jsAddress.child(i).Value("mobilePhone")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.Cell(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,9) = jsAddress.child(i).Value("email")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.Cell(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,10) = jsAddress.child(i).Value("addressDetailsName")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.CellTag(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,10) = jsAddress.child(i).Value("addressDetails")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.Cell(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,11) = jsAddress.child(i).Value("addressDetails1Name")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.CellTag(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,11) = jsAddress.child(i).Value("addressDetails1")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.Cell(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex,12) = jsAddress.child(i).Value("remark")
		      me.ctnOtherForm.ctnListAddress.lstHistoryProfile.RowTag(me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex) = me.ctnOtherForm.ctnListAddress.lstHistoryProfile.LastIndex
		    next
		  end
		  
		  Dim sExperience as String = jsPerson.Value("experience")
		  Dim jsExperience as new JSONItem(sExperience)
		  if jsExperience.ToString <> "" then
		    me.ctnListExperience.lstHistoryProfile.DeleteAllRows
		    for i as integer = 0 to jsExperience.Count-1
		      me.ctnListExperience.lstHistoryProfile.AddRow()
		      me.ctnListExperience.lstHistoryProfile.Cell(me.ctnListExperience.lstHistoryProfile.LastIndex,0) = me.ctnListExperience.lstHistoryProfile.ListCount.ToText
		      me.ctnListExperience.lstHistoryProfile.CellType(me.ctnListExperience.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      me.ctnListExperience.lstHistoryProfile.Cell(me.ctnListExperience.lstHistoryProfile.LastIndex,1) = jsExperience.child(i).Value("company")
		      me.ctnListExperience.lstHistoryProfile.Cell(me.ctnListExperience.lstHistoryProfile.LastIndex,2) = jsExperience.child(i).Value("position")
		      me.ctnListExperience.lstHistoryProfile.Cell(me.ctnListExperience.lstHistoryProfile.LastIndex,3) = jsExperience.child(i).Value("salary")
		      me.ctnListExperience.lstHistoryProfile.Cell(me.ctnListExperience.lstHistoryProfile.LastIndex,4) = jsExperience.child(i).Value("effectiveDate")
		      me.ctnListExperience.lstHistoryProfile.Cell(me.ctnListExperience.lstHistoryProfile.LastIndex,5) = jsExperience.child(i).Value("endDate")
		      me.ctnListExperience.lstHistoryProfile.Cell(me.ctnListExperience.lstHistoryProfile.LastIndex,6) = jsExperience.child(i).Value("jobType")
		      me.ctnListExperience.lstHistoryProfile.Cell(me.ctnListExperience.lstHistoryProfile.LastIndex,7) = jsExperience.child(i).Value("jobLevel")
		      me.ctnListExperience.lstHistoryProfile.Cell(me.ctnListExperience.lstHistoryProfile.LastIndex,8) = jsExperience.child(i).Value("jobDuration")
		      me.ctnListExperience.lstHistoryProfile.Cell(me.ctnListExperience.lstHistoryProfile.LastIndex,9) = jsExperience.child(i).Value("leaveReason")
		      me.ctnListExperience.lstHistoryProfile.Cell(me.ctnListExperience.lstHistoryProfile.LastIndex,10) = jsExperience.child(i).Value("remark")
		      me.ctnListExperience.lstHistoryProfile.RowTag(me.ctnListExperience.lstHistoryProfile.LastIndex) = me.ctnListExperience.lstHistoryProfile.LastIndex
		    next
		  end
		  
		  Dim sSkill as String = jsPerson.Value("skill")
		  Dim jsSkill as new JSONItem(sSkill)
		  if jsSkill.ToString <> "" then
		    me.ctnListExperience.lstHistoryProfile.DeleteAllRows
		    for i as integer = 0 to jsExperience.Count-1
		      me.ctnListSkill.lstHistoryProfile.AddRow()
		      me.ctnListSkill.lstHistoryProfile.Cell(me.ctnListSkill.lstHistoryProfile.LastIndex,0) = me.ctnListSkill.lstHistoryProfile.ListCount.ToText
		      me.ctnListSkill.lstHistoryProfile.CellType(me.ctnListSkill.lstHistoryProfile.LastIndex,0) = Listbox.TypeCheckbox
		      me.ctnListSkill.lstHistoryProfile.Cell(me.ctnListSkill.lstHistoryProfile.LastIndex,1) = jsExperience.Child(i).Value("skillName")
		      me.ctnListSkill.lstHistoryProfile.CellTag(me.ctnListSkill.lstHistoryProfile.LastIndex,1) = jsExperience.Child(i).Value("skill")
		      me.ctnListSkill.lstHistoryProfile.Cell(me.ctnListSkill.lstHistoryProfile.LastIndex,2) = jsExperience.Child(i).Value("skillDetailsName")
		      me.ctnListSkill.lstHistoryProfile.CellTag(me.ctnListSkill.lstHistoryProfile.LastIndex,2) = jsExperience.Child(i).Value("skillDetails")
		      me.ctnListSkill.lstHistoryProfile.Cell(me.ctnListSkill.lstHistoryProfile.LastIndex,3) = jsExperience.Child(i).Value("skillLevelName")
		      me.ctnListSkill.lstHistoryProfile.CellTag(me.ctnListSkill.lstHistoryProfile.LastIndex,3) = jsExperience.Child(i).Value("skillLevel")
		      me.ctnListSkill.lstHistoryProfile.Cell(me.ctnListSkill.lstHistoryProfile.LastIndex,4) = jsExperience.Child(i).Value("remark")
		      me.ctnListSkill.lstHistoryProfile.RowTag(me.ctnListSkill.lstHistoryProfile.LastIndex) = me.ctnListSkill.lstHistoryProfile.LastIndex
		    next
		  end
		  
		  
		  Exception err as JSONException
		  Exception err as KeyNotFoundException
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		CurrentFileUpload As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		CurrentID As String
	#tag EndProperty


#tag EndWindowCode

#tag Events TabPanel1
	#tag Event
		Sub Change(index as Integer)
		  'MsgBox me.Value.ToText
		  if me.Value = 0 then
		    'self.ctnGeneralForm.Visible = True
		    self.ActionSegment
		  else
		    Self.HideComponent
		  end
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events SegmentedControl1
	#tag Event
		Sub Action(itemIndex as integer)
		  Self.ActionSegment
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CurrentID"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
