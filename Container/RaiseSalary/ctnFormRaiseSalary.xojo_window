#tag Window
Begin ContainerControl ctnFormRaiseSalary
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   611
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   944
   Begin PagePanel PagePanel1
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   611
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      PanelCount      =   2
      Panels          =   ""
      Scope           =   0
      TabIndex        =   25
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Value           =   0
      Visible         =   True
      Width           =   944
      Begin Rectangle Rectangle1
         AutoDeactivate  =   True
         BorderWidth     =   1
         BottomRightColor=   &c00000000
         Enabled         =   True
         FillColor       =   &cFF00FFFF
         Height          =   611
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Left            =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   26
         TabPanelIndex   =   1
         TabStop         =   True
         Top             =   0
         TopLeftColor    =   &c00000000
         Visible         =   True
         Width           =   944
         Begin TextField fldDocumentDate
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFF00FFFF
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "Rectangle1"
            Italic          =   False
            Left            =   132
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   53
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   199
         End
         Begin Label Label5
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "Rectangle1"
            Italic          =   False
            Left            =   20
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   1
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "รายชื่อพนักงาน"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   151
            Transparent     =   True
            Underline       =   False
            Visible         =   True
            Width           =   100
         End
         Begin BevelButton BevelButton1
            AcceptFocus     =   True
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "ค้นหารายชื่อพนักงาน"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   ""
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "Rectangle1"
            Italic          =   False
            Left            =   132
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   2
            TabPanelIndex   =   1
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   117
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   199
         End
         Begin Label Label11
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "Rectangle1"
            Italic          =   False
            Left            =   20
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   3
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "สถานะอนุมัติ"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   539
            Transparent     =   True
            Underline       =   False
            Visible         =   True
            Width           =   100
         End
         Begin RadioButton rdoApproveStatus
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "อนุมัติ"
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "Rectangle1"
            Italic          =   False
            Left            =   132
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   4
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   539
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   194
         End
      End
      Begin RadioButton RadioButton3
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "ตามตำแหน่ง"
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Italic          =   False
         Left            =   241
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   86
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   90
      End
      Begin RadioButton RadioButton2
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "บุคคล"
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Italic          =   False
         Left            =   132
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label Label12
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Italic          =   False
         Left            =   20
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   2
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   "ประเภทการปรับ"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   85
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin TextField fldDocumentCode
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFF00FFFF
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Italic          =   False
         Left            =   132
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   9
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   21
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   199
      End
      Begin Label Label4
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Italic          =   False
         Left            =   375
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   16
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   "วันที่มีผล"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   54
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label Label2
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Italic          =   False
         Left            =   20
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   18
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   "วันที่เอกสาร"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   53
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label Label1
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Italic          =   False
         Left            =   20
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   19
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   "รหัสเอกสาร"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   21
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin TextField fldEffectiveDate
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFF00FFFF
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         Italic          =   False
         Left            =   487
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   21
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   52
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   199
      End
      Begin Listbox lstPersonForRS
         AutoDeactivate  =   True
         AutoHideScrollbars=   True
         Bold            =   False
         Border          =   True
         ColumnCount     =   0
         ColumnsResizable=   False
         ColumnWidths    =   ""
         DataField       =   ""
         DataSource      =   ""
         DefaultRowHeight=   -1
         Enabled         =   True
         EnableDrag      =   False
         EnableDragReorder=   False
         GridLinesHorizontal=   2
         GridLinesVertical=   2
         HasHeading      =   False
         HeadingIndex    =   -1
         Height          =   367
         HelpTag         =   ""
         Hierarchical    =   False
         Index           =   -2147483648
         InitialParent   =   "PagePanel1"
         InitialValue    =   ""
         Italic          =   False
         Left            =   132
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         RequiresSelection=   False
         Scope           =   0
         ScrollbarHorizontal=   False
         ScrollBarVertical=   True
         SelectionType   =   0
         TabIndex        =   24
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   151
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   792
         _ScrollOffset   =   0
         _ScrollWidth    =   -1
      End
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  Self.PagePanel1.Value = 0 
		  Self.SetRaiseSalaryItem
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub BindData()
		  Dim data as new JSONItem
		  data = me.GetValue
		  MsgBox data.ToString
		  me.SendData(data)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetValue() As JSONItem
		  Dim data as new JSONItem
		  data.Value("id") = self.CurrentId
		  data.Value("organization") = RaiseSalaryWindow.ctnListRaiseSalary1.SelectedOrganization
		  data.Value("documentCode")  = self.fldDocumentCode.text
		  data.Value("documentDate") = self.fldDocumentDate.text
		  data.Value("approveStatus") = self.rdoApproveStatus.Value
		  
		  Dim rsArray as new JSONItem
		  Dim rsItem as new JSONItem
		  Dim rsNew as new JSONItem
		  Dim rsUpdate as new JSONItem
		  Dim rsDelete as new JSONItem
		  
		  for i as integer = 0 to self.lstPersonForRS.ListCount-1
		    rsItem.Value("id") = self.lstPersonForRS.CellTag(self.lstPersonForRS.ListIndex,0)
		    rsItem.Value("personnelId") = self.lstPersonForRS.RowTag(self.lstPersonForRS.ListIndex)
		    rsItem.Value("currentSalary") = self.lstPersonForRS.Cell(self.lstPersonForRS.ListIndex,2)
		    rsItem.Value("newSalary") = self.lstPersonForRS.Cell(self.lstPersonForRS.ListIndex,3)
		    
		    if rsItem.Value("id") <> "" then
		      rsUpdate.Append(rsItem)
		    else
		      rsNew.Append(rsItem)
		    end
		  next
		  
		  if rsNew.Count > 0 then
		    rsArray.Value("new") = rsNew
		  end
		  if rsUpdate.Count > 0 then
		    rsArray.Value("update") = rsUpdate
		  end
		  if rsDelete.Count > 0 then
		    rsArray.Value("delete") = rsDelete
		  end
		  
		  data.Value("raiseSalaryItem") = rsArray.ToString
		  
		  return data
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadListRS()
		  if Utils.WebService.IsConnect then
		    Dim personInstance As New Repo.WS.Personnel
		    Dim js As String
		    Dim jsData As new JSONItem
		    Dim sData As new JSONItem
		    
		    self.lstPersonForRS.DeleteAllRows
		    
		    sData.Value("organizationId") = RaiseSalaryWindow.ctnListRaiseSalary1.SelectedOrganization
		    sData.Value("positionId") = self.PositionSelected
		    sData.Value("search") = self.PersonnelSelected
		    
		    Try
		      js = personInstance.ListAllSalary(sData)
		      jsData = New JSONItem(js)
		      If jsData.Value("status") = True Then
		        Dim objData As JSONItem = jsData.Value("data")
		        For i As Integer = 0 To objData.Count - 1
		          Dim iItem As JSONItem = objData.Child(i)
		          self.lstPersonForRS.AddRow
		          self.lstPersonForRS.RowTag(self.lstPersonForRS.LastIndex) = iItem.Value("personnelId")
		          self.lstPersonForRS.CellTag(self.lstPersonForRS.LastIndex,0) = ""
		          self.lstPersonForRS.Cell(self.lstPersonForRS.LastIndex,0) = str(self.lstPersonForRS.ListCount)
		          self.lstPersonForRS.Cell(self.lstPersonForRS.LastIndex,1) = iItem.Value("firstNameTh") +"   "+iItem.Value("lastNameTh")
		          self.lstPersonForRS.Cell(self.lstPersonForRS.LastIndex,2) = Str(iItem.Value("salary").IntegerValue)
		        Next
		      End
		    Catch err As KeyNotFoundException
		    End
		  else
		    MsgBox "ไม่สามารถเชื่อมต่อ webservice ได้"
		  end
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SendData(formData as JSONItem)
		  'MsgBox formData.ToString
		  Dim rsInstance As New Repo.WS.RaiseSalary
		  If formData.Value("id") = "" Then
		    If rsInstance.Save(formData) Then
		      MsgBox "เพิ่มข้อมูลสำเร็จ"
		    Else
		      MsgBox "เพิ่มข้อมูลไม่สำเร็จ"
		    End
		  Else
		    If rsInstance.Update(formData) Then
		      MsgBox "แก้ไขข้อมูลสำเร็จ"
		    Else
		      MsgBox "แก้ไขข้อมูลไม่สำเร็จ"
		    End
		  End
		  
		  RaiseSalaryWindow.ctnListRaiseSalary1.LoadLists
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetID(id as string)
		  self.CurrentID = id
		  self.SetValue
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetNewSalary(row as integer, newSalary as String)
		  if newSalary <> "" then
		    self.lstPersonForRS.Cell(row,3) = newSalary
		  end
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetPersonRS(data as JSONItem)
		  self.lstPersonForRS.DeleteAllRows
		  Try
		    self.lstPersonForRS.AddRow
		    self.lstPersonForRS.RowTag(self.lstPersonForRS.LastIndex) = data.Value("personnelId")
		    self.lstPersonForRS.Cell(self.lstPersonForRS.LastIndex,0) = str(self.lstPersonForRS.ListCount)
		    self.lstPersonForRS.CellTag(self.lstPersonForRS.LastIndex,0) = ""
		    self.lstPersonForRS.Cell(self.lstPersonForRS.LastIndex,1) = data.Value("name")
		    self.lstPersonForRS.Cell(self.lstPersonForRS.LastIndex,2) = data.Value("salary")
		  Catch err As KeyNotFoundException
		  end
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetRaiseSalaryItem()
		  Dim rsItemHeading() as String = Array("ลำดับ", "ชื่อ-นามสกุล","เงินเดือนปัจจุบัน","เงินเดือนใหม่")
		  
		  me.lstPersonForRS.ColumnCount = rsItemHeading.Ubound+1
		  me.lstPersonForRS.HasHeading = True
		  
		  for i as Integer = 0 to rsItemHeading.Ubound
		    me.lstPersonForRS.Heading(i) = rsItemHeading(i)
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetValue()
		  Dim rsInstance as new Repo.WS.RaiseSalary
		  Dim jsRs as new JSONItem
		  jsRs = rsInstance.GetByID(self.CurrentID)
		  
		  self.fldDocumentCode.text = jsRs.Value("documentCode")
		  self.fldDocumentDate.text = jsRs.Value("documentDate")
		  'self.fldEffectiveDate.text = jsRs.Value("")
		  
		  if jsRs.Value("approveStatusName") = "อนุมัติ" then
		    self.rdoApproveStatus.Value = True
		  else
		    self.rdoApproveStatus.Value = False
		  end
		  
		  
		  self.lstPersonForRS.DeleteAllRows
		  Dim rsArray as new JSONItem
		  rsArray = jsRs.Value("raiseSalaryItem")
		  for i as integer = 0 to rsArray.Count-1
		    Dim rsItem as new JSONItem
		    rsItem = rsArray.Child(i)
		    self.lstPersonForRS.AddRow
		    self.lstPersonForRS.RowTag(self.lstPersonForRS.LastIndex) = rsItem.Value("personnelId")
		    self.lstPersonForRS.Cell(self.lstPersonForRS.LastIndex,0) = str(self.lstPersonForRS.ListCount)
		    self.lstPersonForRS.CellTag(self.lstPersonForRS.LastIndex,0) = rsItem.Value("id")
		    self.lstPersonForRS.Cell(self.lstPersonForRS.LastIndex,1) = rsItem.Value("firstNameTh") +"   "+rsItem.Value("lastNameTh")
		    self.lstPersonForRS.Cell(self.lstPersonForRS.LastIndex,2) = Str(rsItem.Value("currentSalary").IntegerValue)
		    self.lstPersonForRS.Cell(self.lstPersonForRS.LastIndex,3) = Str(rsItem.Value("newSalary").IntegerValue)
		    
		  next
		  
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		CurrentID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		PersonnelSelected As String
	#tag EndProperty

	#tag Property, Flags = &h0
		PositionSelected As String
	#tag EndProperty

	#tag Property, Flags = &h0
		typeSelection As String
	#tag EndProperty


#tag EndWindowCode

#tag Events BevelButton1
	#tag Event
		Sub Action()
		  select case self.typeSelection
		  case "บุคคล"
		    Dim inv as new IndividualPersonWindow
		    inv.Show
		  case "ตำแหน่งงาน"
		    Dim gp as new GroupPositionWindow
		    gp.Show
		  else
		    MsgBox "กรุณาเลือกประเภท"
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events RadioButton3
	#tag Event
		Sub Action()
		  self.typeSelection = "ตำแหน่งงาน"
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events RadioButton2
	#tag Event
		Sub Action()
		  self.typeSelection = "บุคคล"
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events lstPersonForRS
	#tag Event
		Sub DoubleClick()
		  Dim rsAction as new RSActionWindow
		  Dim jsData as new JSONItem
		  jsData.Value("id") = me.CellTag(me.ListIndex,0)
		  jsData.Value("name") = me.Cell(me.ListIndex,1)
		  jsData.Value("currentSalary") = me.Cell(me.ListIndex,2) 
		  jsData.Value("newSalary") = me.Cell(me.ListIndex,3) 
		  jsData.Value("row") = me.ListIndex
		  MsgBox jsData.ToString
		  rsAction.SetValue(jsData)
		  rsAction.Show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CurrentID"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="PersonnelSelected"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="PositionSelected"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="typeSelection"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
