#tag Class
Protected Class ActingPosition
	#tag Method, Flags = &h0
		Function Delete(id as String) As Boolean
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim rawData As JSONItem
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionHeader = False
		  c.CollectDebugData = True
		  c.CollectOutputData = True
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  c.SetOptionHTTPHeader(httpHeader)
		  c.OptionCustomRequest = "DELETE"
		  
		  c.OptionURL = App.kURLService + Self.NameService + "/"+Id
		  response = c.Perform
		  If response = 0 Then
		    rawData = New JSONItem(c.OutputData)
		    If rawData.Value("status") Then
		      Return True
		    End
		  End
		  Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetByID(id as String) As JSONItem
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionURL = App.kURLService + NameService + "/" + id
		  c.OptionHeader = False
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  httpHeader.Append("Content-Type: application/json; charset=UTF-8")
		  c.SetOptionHTTPHeader(httpHeader)
		  c.CollectOutputData = True
		  response = c.Perform
		  Dim dataR As New JSONItem
		  
		  If response = 0 Then
		    Dim reqData As New JSONItem(c.OutputData)
		    if reqData.Value("status") = true Then
		      Dim objData As JSONItem = reqData.Value("data")
		      'dataR.Value("id") = objData.Value("id")
		      dataR = objData
		    End
		  End
		  
		  Return dataR
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ListAll(sData As JSONItem = Nil) As String
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim data() As String
		  Dim URL As String
		  
		  If sData.ToString =  "{}" Then
		    URL = App.kURLService + Self.NameService + "/list?&sortValue=desc"
		    
		  Else
		    URL = App.kURLService + Self.NameService + "/list?&sortValue=desc&searchText="+sData.Value("searchText").StringValue
		  End If
		  
		  'URL = App.kURLService + Self.NameService + "/list?fieldSort=id&sortValue=desc"
		  
		  c.OptionVerbose=True
		  c.OptionURL = URL
		  c.OptionHeader = False
		  data.Append("Accept-Version: " + Self.ApiVersion)
		  data.Append("Content-Type: application/json; charset=UTF-8")
		  c.SetOptionHTTPHeader(data)
		  c.CollectDebugData = True
		  c.CollectOutputData = True
		  response = c.Perform
		  
		  If response = 0 Then
		    Return c.OutputData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Save(formData as JSONItem) As Boolean
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim rawData As JSONItem
		  Dim httpHeader() As String
		  
		  'MsgBox formData.ToString
		  
		  c.OptionVerbose=True
		  c.OptionHeader = False
		  c.OptionPost = True
		  c.CollectOutputData = True
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  httpHeader.Append("Content-Type: application/json")
		  httpHeader.Append("Content-Length: " + Str(LenB(formData.ToString)))
		  c.SetOptionHTTPHeader(httpHeader)
		  
		  c.OptionPostFields = formData.ToString
		  c.OptionURL = App.kURLService + NameService
		  
		  response = c.Perform
		  If response = 0 Then
		    rawData = New JSONItem(c.OutputData)
		    If rawData.Value("status") Then
		      Dim jstemp as new JSONItem
		      jstemp = rawData.Value("data")
		      'MsgBox jstemp.Value("id").StringValue
		      Utils.WebService.UploadImageFile(PersonnelWindow.cFormPersonnel.ctnGeneralForm.picFile,jstemp.Value("id").StringValue)
		      Return True
		    End
		  End
		  'MsgBox STR(rawData.Value("message"))
		  Return False
		  
		  Exception err as JSONException
		    
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Update(formData as JSONItem) As Boolean
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim rawData As JSONItem
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionHeader = False
		  c.CollectOutputData = True
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  httpHeader.Append("Content-Type: application/json")
		  httpHeader.Append("Content-Length: " + Str(LenB(formData.ToString)))
		  c.SetOptionHTTPHeader(httpHeader)
		  c.OptionCustomRequest = "PUT"
		  c.OptionPostFields = formData.ToString
		  c.OptionURL = App.kURLService + Self.NameService + "/" + formData.Value("id")
		  response = c.Perform
		  If response = 0 Then
		    rawData = New JSONItem(c.OutputData)
		    If rawData.Value("status") Then
		      Return True
		    End
		  End
		  Return False
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private ApiVersion As String = "1.0"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private NameService As String = "actingPositions"
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
