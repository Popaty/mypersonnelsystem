#tag Class
Protected Class Department
Implements IDepartment
	#tag Method, Flags = &h0
		Function Delete(Id As String) As Boolean
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim rawData As JSONItem
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionHeader = False
		  c.CollectDebugData = True
		  c.CollectOutputData = True
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  c.SetOptionHTTPHeader(httpHeader)
		  c.OptionCustomRequest = "DELETE"
		  
		  c.OptionURL = App.kURLService + Self.NameService + "/"+Id
		  response = c.Perform
		  If response = 0 Then
		    rawData = New JSONItem(c.OutputData)
		    If rawData.Value("status") Then
		      Return True
		    End
		  End
		  Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetByDepartmentType(orgID As String,type As String,divID As String = "") As String
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim data() As String
		  
		  c.OptionVerbose=True
		  c.OptionURL = App.kURLService + Self.NameService + "/list?max=0&departmentType=" + type + "&organization=" + orgID + "&divisionId=" + divID
		  c.OptionHeader = False
		  data.Append("Accept-Version: " + Self.ApiVersion)
		  data.Append("Content-Type: application/json; charset=UTF-8")
		  c.SetOptionHTTPHeader(data)
		  c.CollectDebugData = True
		  c.CollectOutputData = True
		  response = c.Perform
		  
		  If response = 0 Then
		    Return c.OutputData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetByID(id As String) As JSONItem
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionURL = App.kURLService + NameService + "/" + id
		  c.OptionHeader = False
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  httpHeader.Append("Content-Type: application/json; charset=UTF-8")
		  c.SetOptionHTTPHeader(httpHeader)
		  c.CollectOutputData = True
		  response = c.Perform
		  Dim dataR As New JSONItem
		  
		  If response = 0 Then
		    Dim reqData As New JSONItem(c.OutputData)
		    if reqData.Value("status") = true Then
		      Dim objData As JSONItem = reqData.Value("data")
		      dataR.Value("id") = objData.Value("id")
		      dataR.Value("code") = objData.Value("code")
		      dataR.Value("departmentNameTh") = objData.Value("departmentNameTh")
		      dataR.Value("departmentNameEng") = objData.Value("departmentNameEng")
		      dataR.Value("description") = objData.Value("description")
		      dataR.Value("organizationId") = objData.Value("organizationId")
		      dataR.Value("organizationName") = objData.Value("organizationName")
		      dataR.Value("departmentType") = objData.Value("departmentType")
		      dataR.Value("departmentTypeName") = objData.Value("departmentTypeName")
		      dataR.Value("divisionId") = objData.Value("divisionId")
		      dataR.Value("divisionName") = objData.Value("divisionName")
		      dataR.Value("workId") = objData.Value("workId")
		      dataR.Value("workName") = objData.Value("workName")
		      dataR.Value("unitId") = objData.Value("unitId")
		      dataR.Value("unitName") = objData.Value("unitName")
		    End
		  End
		  
		  Return dataR
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetForSelectPosition(type As String,orgID As String,division As String = "",work As String = "") As String
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim data() As String
		  
		  c.OptionVerbose=True
		  c.OptionURL = App.kURLService + Self.NameService + "/listSelect?departmentType=" + type + "&organization=" + orgID + "&divisionId=" + division + "&workId=" + work 
		  c.OptionHeader = False
		  data.Append("Accept-Version: " + Self.ApiVersion)
		  data.Append("Content-Type: application/json; charset=UTF-8")
		  c.SetOptionHTTPHeader(data)
		  c.CollectDebugData = True
		  c.CollectOutputData = True
		  response = c.Perform
		  
		  If response = 0 Then
		    Return c.OutputData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ListAll(sData As JSONItem = Nil) As String
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim data() As String
		  
		  Dim URL As String
		  if sData <> Nil Then
		    URL = App.kURLService + Self.NameService + "/list?max=0&currentPage=" + sData.Value("currentPage") + "&organization=" + sData.Value("organizationId") + "&departmentType=" + sData.Value("departmentType") + "&searchText=" + sData.Value("textSearch")
		  else
		    URL = App.kURLService + Self.NameService + "/list"
		  End if
		  
		  c.OptionVerbose=True
		  c.OptionURL = URL
		  c.OptionHeader = False
		  data.Append("Accept-Version: " + Self.ApiVersion)
		  data.Append("Content-Type: application/json; charset=UTF-8")
		  c.SetOptionHTTPHeader(data)
		  c.CollectDebugData = True
		  c.CollectOutputData = True
		  response = c.Perform
		  
		  If response = 0 Then
		    Return c.OutputData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Save(formData As JSONItem) As Boolean
		  System.DebugLog(formData.ToString)
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim rawData As JSONItem
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionHeader = False
		  c.OptionPost = True
		  c.CollectOutputData = True
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  httpHeader.Append("Content-Type: application/json")
		  httpHeader.Append("Content-Length: " + Str(LenB(formData.ToString)))
		  c.SetOptionHTTPHeader(httpHeader)
		  
		  c.OptionPostFields = formData.ToString
		  c.OptionURL = App.kURLService + NameService
		  response = c.Perform
		  If response = 0 Then
		    rawData = New JSONItem(c.OutputData)
		    If rawData.Value("status") Then
		      Return True
		    else
		      System.DebugLog(rawData.Value("message"))
		    End
		  End
		  Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Update(formData As JSONItem) As Boolean
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim rawData As JSONItem
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionHeader = False
		  c.CollectOutputData = True
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  httpHeader.Append("Content-Type: application/json")
		  httpHeader.Append("Content-Length: " + Str(LenB(formData.ToString)))
		  c.SetOptionHTTPHeader(httpHeader)
		  c.OptionCustomRequest = "PUT"
		  c.OptionPostFields = formData.ToString
		  c.OptionURL = App.kURLService + Self.NameService + "/" + formData.Value("id")
		  response = c.Perform
		  If response = 0 Then
		    rawData = New JSONItem(c.OutputData)
		    If rawData.Value("status") Then
		      Return True
		    End
		  End
		  Return False
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private ApiVersion As String = "1.0"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private NameService As String = "departments"
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
