#tag Module
Protected Module ValidateTools
	#tag Method, Flags = &h1
		Protected Function gcheckIDCard(idNumber As String) As Boolean
		  Dim arrayID(13) as String
		  Dim i as Integer
		  Dim temp As Integer
		  Dim total As Integer
		  Dim lenArray As integer
		  Dim checkDigit As integer
		  Dim result As Boolean
		  lenArray = idNumber.Len
		  temp = 0
		  arrayID(13) = idNumber.Mid(13,1)
		  
		  'MsgBox arrayId(13)
		  for i=0 to idNumber.Len-2
		    arrayID(i) = idNumber.Mid(i+1,1)
		    temp = temp + (arrayID(i).Val * (idNumber.Len-i))
		  Next
		  'MsgBox temp.ToText
		  checkDigit = 11 - (temp mod 11)
		  
		  if checkDigit.ToText.Length > 1 then
		    checkDigit = Str(checkDigit.ToText.Mid(1,1)).Val
		  end if
		  
		  if arrayId(13).Val = checkDigit Then
		    result = True
		  Else
		    result = false
		  end if
		  
		  Return result
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function gFormatIDCard(idNumber As String) As String
		  Dim result as string
		  if idNumber.Len <= 13 then
		    for i as integer = 1 to idNumber.Len
		      if i = 2 or i = 6 or i = 11 or i = 13 then
		        result = result+"-"+idNumber.Mid(i,1)
		      else
		        result = result+idNumber.Mid(i,1)
		      end
		    next
		  else
		    for i as integer = 1 to idNumber.Len
		      if idNumber.Mid(i,1) = "-" then
		      else
		        result = result+idNumber.Mid(i,1)
		      end
		    next
		  end
		  Return result
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function gValidateEmail(emailTxt As String) As Boolean
		  Dim result As Boolean
		  
		  Dim re As New RegEx
		  Dim rm As New RegExMatch
		  
		  If emailTxt = "" then
		    MsgBox "Input your Email"
		  else
		    re.SearchPattern = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
		    rm =re.Search(emailTxt)
		    
		    if rm = Nil Then
		      MsgBox "Email worng pattern."
		      result = False
		    Else
		      result = True
		    End
		  End
		  
		  return result
		  
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
