#tag Module
Protected Module WebService
	#tag Method, Flags = &h1
		Protected Function IsConnect() As Boolean
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionURL = App.kURLService 
		  c.OptionHeader = False
		  httpHeader.Append("Content-Type: application/json; charset=UTF-8")
		  c.SetOptionHTTPHeader(httpHeader)
		  c.CollectOutputData = True
		  response = c.Perform
		  If response = 0 Then
		    Return True
		  Else
		    Return False
		  End
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub SetFormData(sock as HTTPSocket, FormData as Dictionary, Boundary as String)
		  If Boundary.Trim = "" Then  
		    Boundary = "--" + Right(EncodeHex(MD5(Str(Microseconds))), 24) + "-bOuNdArY"  
		  End If  
		  
		  Static CRLF As String = EndOfLine.Windows  
		  Dim data As New MemoryBlock(0)  
		  Dim out As New BinaryStream(data)  
		  
		  For Each key As String In FormData.Keys  
		    out.Write("--" + Boundary + CRLF)  
		    If VarType(FormData.Value(Key)) = Variant.TypeString Then  
		      out.Write("Content-Disposition: form-data; name=""" + key + """" + CRLF + CRLF)  
		      out.Write(FormData.Value(key) + CRLF)  
		    ElseIf FormData.Value(Key) IsA FolderItem Then  
		      Dim file As FolderItem = FormData.Value(key)  
		      out.Write("Content-Disposition: form-data; name=""" + key + """; filename=""" + File.Name + """" + CRLF)  
		      out.Write("Content-Type: application/octet-stream" + CRLF + CRLF) ' replace with actual MIME Type  
		      Dim bs As BinaryStream = BinaryStream.Open(File)  
		      out.Write(bs.Read(bs.Length) + CRLF)  
		      bs.Close  
		    End If  
		  Next  
		  out.Write("--" + Boundary + "--" + CRLF)  
		  out.Close  
		  #If RBVersion > 2012 Then  
		    sock.SetRequestContent(data, "multipart/form-data; boundary=" + Boundary)  
		  #else  
		    sock.SetPostContent(data, "multipart/form-data; boundary=" + Boundary)  
		  #endif  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub UploadFile(file as FolderItem, id as String, key as String)
		  If file<> Nil Then
		    Dim MyHTTPSocket as new HTTPSocket
		    Dim HTMLForm As New Dictionary  
		    HTMLForm.Value(key) = file
		    'HTMLForm.Value("imageProfile") = "delete"
		    Utils.WebService.SetFormData(MyHTTPSocket, HTMLForm, "")
		    Dim urlUpload as String
		    urlUpload = App.kURLService+"personnels/"+id+"/upload"
		    MyHTTPSocket.Post(urlUpload)
		    'MsgBox MyHTTPSocket.HTTPStatusCode.ToText
		  Else
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub UploadImageFile(file as FolderItem, id as String)
		  If file<> Nil Then
		    Dim MyHTTPSocket as new HTTPSocket
		    Dim HTMLForm As New Dictionary  
		    HTMLForm.Value("imageProfile") = file
		    'HTMLForm.Value("imageProfile") = "delete"
		    Utils.WebService.SetFormData(MyHTTPSocket, HTMLForm, "")
		    Dim urlUpload as String
		    urlUpload = App.kURLService+"personnels/"+id+"/upload"
		    MyHTTPSocket.Post(urlUpload)
		    'MsgBox MyHTTPSocket.HTTPStatusCode.ToText
		  Else
		  End If
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
